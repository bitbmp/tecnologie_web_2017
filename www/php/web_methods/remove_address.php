<?php
require_once "../address_utils.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        if(isset($_POST["address_id"])) {
            $user_id = $_SESSION["user"]->id;            
            $address_id = $_POST["address_id"];
            if(remove_address_from_user($user_id, $address_id)) {
                echo "Indirizzo rimosso.";
            } else {
                echo "Impossibile rimuovere l'indirizzo.";
            }        
        } else {
            echo "Parametri mancanti.";
        }
    } else {
        echo "Devi accedere per effettuare queste operazioni.";
    }
} else {
    echo "Questa non è una richiesta post.";
}

?>