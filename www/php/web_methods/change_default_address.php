<?php
require_once "../address_utils.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        if(isset($_POST["address_id"])) {
            $user_id = $_SESSION["user"]->id;            
            $address_id = $_POST["address_id"];
            if(change_user_default_address($user_id, $address_id)) {
                reload_user();
                echo "Indirizzo promosso a default.";
            } else {
                echo "Impossibile promuovere l'indirizzo a default.";
            }     
        } else {
            echo "Parametri mancanti.";
        }
    } else {
        echo "Devi accedere per effettuare queste operazioni.";
    }
} else {
    echo "Questa non è una richiesta post.";
}

?>