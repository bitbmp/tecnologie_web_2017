var addresses_request_number = 0;

$(document).ready(function() {

    getAllAddresses(addresses_request_number);

    //Remove card handler
    $(".btn-remove_address").click(removeAddress);
    //Change default card handler
    $(".btn-set_default_address").click(changeDefaultAddress);
    //Remove default card handler 
    $(".btn-remove_default_address").click(removeDefaultAddress);

    //Add form helper handler
    $("#txbRoad").on("keyup", function() {
        InputInvalidator($(this), !checkStringWithoutNumbers($(this).val()));
    });

    $("#txbNumber").on("keyup", function() {
        InputInvalidator($(this), !checkCivicNumber($(this).val()));
    });

    $("#txbCAP").on("keyup", function() {
        InputInvalidator($(this), !checkCAP($(this).val()));
    });

    $("#txbCity").on("keyup", function() {
        InputInvalidator($(this), !checkStringWithoutNumbers($(this).val()));
    });

    $("#txbProvince").on("keyup", function() {
        InputInvalidator($(this), !checkProvince($(this).val()));
    });

    $("#txbAlias").on("keyup", function() {
        InputInvalidator($(this), !checkString($(this).val()));
    });

    //Toggle button handler
    $(".btn-toggle-newaddress").click( function() {
        FormToggler($("#add-address-form"), $(this), "Aggiungi nuovo indirizzo&nbsp&nbsp&nbsp&nbsp<div class=\"far fa-address-card\"></div>");
    });
    $(".btn-add_newaddress").click(addAddress);

});

function getAllAddresses(request) {
    addresses_request_number++;
    console.log("get all addresses triggered");
    
    var list_section = $('#addresses-list-area');
    list_section.html("");


    $.ajax({
        type: "POST",
        url: "php/web_methods/get_all_addresses.php",
        success: function(html){
            if(html != "Devi essere loggato per accedere." && html != "Questa non è una richiesta post." && addresses_request_number===request+1) {
                list_section.append(html);
                //Need to re-attach event to remove card buttons
                $(".btn-set_default_address").click(changeDefaultAddress);
                $(".btn-remove_default_address").click(removeDefaultAddress);
                $(".btn-remove_address").click(removeAddress);              
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("get all addresses failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function changeDefaultAddress(event) {
    console.log("change default address triggered");
    var form = $(this).parents('form:first');
    var address_id = form.find('.input-hidden-address-id');

    var obj_data = {address_id: address_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/change_default_address.php",
        data: obj_data,
        success: function(msg){
            console.log(msg);
            if (msg == "Indirizzo promosso a default.") {
                getAllAddresses(addresses_request_number);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("change_default_address failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function removeDefaultAddress(event) {
    console.log("remove default address triggered");
    var form = $(this).parents('form:first');
    var address_id = form.find('.input-hidden-address-id');

    var obj_data = {address_id: address_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/remove_default_address.php",
        data: obj_data,
        success: function(msg){
            console.log(msg);
            if (msg == "Indirizzo rimosso da default.") {
                getAllAddresses(addresses_request_number);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("remove_default_address failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function removeAddress(event) {
    console.log("remove address triggered");
    var form = $(this).parents('form:first');
    var address_id = form.find('.input-hidden-address-id');

    var obj_data = {address_id: address_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/remove_address.php",
        data: obj_data,
        success: function(msg){
            console.log(msg);
            if (msg == "Indirizzo rimosso.") {
                getAllAddresses(addresses_request_number);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("remove_address failed");
            console.log(textStatus, errorThrown);
        },
    });
} 

function addAddress(event) {
    console.log("add address triggered");
    
    var form = $(this).parents('form:first');
    var road = form.find('#txbRoad');
    var civic_number = form.find('#txbNumber');
    var cap = form.find('#txbCAP');
    var city = form.find('#txbCity');
    var province = form.find('#txbProvince');
    var alias = form.find('#txbAlias');

    if(checkInput(road, civic_number, cap, city, province, alias)) {
        var obj_data = {road: road.val(), civic_number: civic_number.val(), cap: cap.val(), city: city.val(), province: province.val(), alias: alias.val()};
        $.ajax({
            type: "POST",
            url: "php/web_methods/add_address.php",
            data: obj_data,
            success: function(msg){
                console.log(msg);
                if(msg == "Indirizzo aggiunto.") {
                    getAllAddresses(addresses_request_number);
                    $(".btn-toggle-newaddress").delay(3000).trigger("click");   
                    form.trigger("reset");
                }   
                clean_components_validation({road, civic_number, cap, city, province, alias});                                              
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("add address failed");
                console.log(textStatus, errorThrown);
            },
        });
    }
}

function checkString(string) {
    return string != null && jQuery.trim(string).length > 0;    
}
function checkStringWithoutNumbers(string) {
    return string != null && jQuery.trim(string).length > 0 && !/\d/.test(string);
}

function checkCivicNumber(civic_number) {
    return (civic_number != null && $.isNumeric(civic_number) && civic_number > 0);    
}

function checkCAP(cap) {
    return (cap != null && $.isNumeric(cap) && cap > 0 && cap.toString().length == 5);    
}

function checkProvince(province) {
    return (province != null && province.toString().length == 2 && /^[a-zA-Z]+$/.test(province));    
}

function checkInput(road, civic_number, cap, city, province, alias) {
    if(!checkStringWithoutNumbers(road.val())) {
        console.log("road not valid");
        invalidate_components({road});
        return false;
    } else if (!checkCivicNumber(civic_number.val())) {
        console.log("Civic number not valid");
        invalidate_components({civic_number});
        validate_components({road});
        return false;        
    } else if (!checkCAP(cap.val())) {
        console.log("CAP not valid");
        invalidate_components({cap});
        validate_components({road, civic_number});
        return false;        
    } else if (!checkStringWithoutNumbers(city.val())) { 
        console.log("City not valid");
        invalidate_components({city});
        validate_components({road, civic_number, cap});
        return false;        
    } else if (!checkProvince(province.val())) {
        console.log("Province not valid");
        invalidate_components({province});
        validate_components({road, civic_number, cap, city});
        return false;        
    } else if (!checkString(alias.val())) {
        console.log("Alias not valid");
        invalidate_components({alias});
        validate_components({road, civic_number, cap, city, province});
        return false;        
    } else {
        return true;
    }
}
