var win;
var page_number = 0;
var request_id = 0;
var end_reached = false;
var page_loading = false;
var current_filter;
var show_dynamic_area;
var default_show_dynamic_area;
var searchText = "";

$(document).ready(function() {
    if(default_show_dynamic_area) {
        show_dynamic_area = true;
        loadNewPage(request_id);
    } else {
        show_dynamic_area = false;
    }
    // Each time the user scrolls
    console.log(current_filter);
	$(window).scroll(scrollTrigger);
    $("#txbCerca").on("keyup", searchTrigger);
    $(".btn-filter").click(filterChanged);

});

function filterChanged(event) {
    searchText = $("#txbCerca").val();
    clearPages();
    current_filter = $(this).attr("data-filter");
    console.log("Filter changed : " + current_filter);
    request_id++;
    loadNewPage(request_id);
}

function searchTrigger(event) {
    searchText = $(this).val();
    clearPages();

    if(searchText !== "") {
        show_dynamic_area = true;
        $(".hide-during-search").hide();
        $(".show-during-search").show();
        request_id++;
        loadNewPage(request_id);
    } else if (default_show_dynamic_area) {
        show_dynamic_area = true;
        $(".hide-during-search").show();
        $(".show-during-search").hide();
        request_id++;
        loadNewPage(request_id);
    } else {
        show_dynamic_area =  false;
        $(".hide-during-search").show();
        $(".show-during-search").hide();
    }
}

function scrollTrigger(event) {
    /*If scrolling has reached the end of the content (not the end of the document) */
    var contentSize = $("nav.navbar").height() + $("main").height();
    if(($(window).scrollTop() >= (contentSize-$(window).height())) && !end_reached && !page_loading && show_dynamic_area) {
        /*console.log("loading new page, scrolltop: "+$(window).scrollTop()+" content: "+(contentSize-$(window).height()));*/
        loadNewPage(request_id);
    }
    /* Old function
    if (($(document).height() - $(window).height() == $(window).scrollTop()) && !end_reached) {
        loadNewPage(searchText);
    }
    */
}

function clearPages() {
    if(end_reached) {
        $(window).scroll(scrollTrigger);
    }
    end_reached = false;
    page_number = 0;
    updateDynamicArea("");
}

function updateDynamicArea(html) {
    var div = $(".product-list-dynamic-area");
    if(html) {
        div.append(html);
        div.find(".btn-personalize_ing").off("click").click(personalizeButtonClick);
        div.find(".btn-add_to_cart").off("click").click(addToCartButtonClick);
        div.find("input.radio-bread-type").off("change").change(radioBreadTypeChanged);
    } else {
        div.find(".btn-personalize_ing").off("click");
        div.find(".btn-add_to_cart").off("click");
        div.find("input.radio-bread-type").off("change");
        div.html("");
    }
}

function loadNewPage(local_request_id) {
    $('#loading').show();
    //$("footer").hide();
    page_loading = true;
    var local_page_number = ++page_number;
    var obj_data = {page_number : local_page_number};
    if(searchText) {
        obj_data["search_keyword"] = searchText;
    }
    if(current_filter) {
        obj_data["filter_product_types"] = current_filter;
    }

    $.ajax({
        type: "POST",
        url: "php/web_methods/pull_products.php",
        data: obj_data,
        success: function(html){
            if(local_request_id === request_id && show_dynamic_area) {
                if (html != "") {
                    updateDynamicArea(html);
                    /*If the viewport is larger than the content, get a new page. Necessary for first load of the page. */
                    var contentSize = $("nav.navbar").height()+$("main").height();
                    if(contentSize < $(window).height()) { 
                        loadNewPage(local_request_id);
                    }

                } else {
                    if(local_page_number === 1) {
                        updateDynamicArea('<p class="text-center">Nessun risultato</p>');
                    } 
                    end_reached = true;
                    $(window).off("scroll");
                }
                page_loading = false;
                //$("footer").show();
                $('#loading').hide();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("get new page failed");
            console.log(textStatus, errorThrown);
        },
    });
}
              					
