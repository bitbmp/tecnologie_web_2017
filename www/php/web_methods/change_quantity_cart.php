<?php

	if(isset($_POST["prod_instance_id"]) && isset($_POST["quantity"]) && $_POST["quantity"] > 0) {
		require_once "../mysql_conn.php";
		require_once "../login_utils.php";
		$conn = connect_db();
		$stmt = $conn->prepare("UPDATE prod_instance SET quantity = ? WHERE id = ? AND product_list = ?;");
		$stmt->bind_param("iii", $_POST["quantity"], $_POST["prod_instance_id"], get_user()->cart_id);  
		$stmt->execute();
		$stmt->close();
		$conn->close();
		
		echo true;
	} else {
		echo false;
	}
?>