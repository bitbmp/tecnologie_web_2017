<?php 

function echo_menu($menu_focus) { 
	require_once "php/page_parts/menu.php";
} 

function echo_head($page_title) { 
	require_once "php/page_parts/head.php";
}

function echo_footer($page_title) {
	require_once "php/page_parts/footer.php";
}

function echo_logo_image() { ?>
	<img src="img/logo.png" alt="Logo" class="rounded mx-auto d-block img-fluid mt-4"/>
<?php }

function echo_divider($text = "") { ?>
	<hr class="hr-text" data-content="<?php echo $text; ?>">
<?php }

function echo_search_bar($text = "Cerca per nome o ingredienti") {
	require_once "php/product_utils.php";
	get_search_bar($text);
}


function echo_most_bought_products() {
	require_once "php/product_utils.php";
	echo get_products_sections_from_sql_procedure("get_most_bought", false, "i", 1);
}
function echo_usual() {
	require_once "php/page_parts/cart.php";
	require_once "php/login_utils.php";
	?>
	<form>
		<div class="row">
					<div class="col-7">
						<p class="font-weight-bold">Articolo</p>
					</div>
					<div class="col-2 text-right">
						<p class="font-weight-bold">Q.tà</p>
					</div>
					<div class="col-3 text-right">
						<p class="font-weight-bold">Prezzo</p>
					</div>
					
			</div>
		<?php
		echo get_all_cart_prods_divs(get_user()->usual, true);
		?>
		<div class="row">
			<div class="col-6">
				<p class="add_to_cart_message" style="display:none;">
					<small class="mt-0 text-left add_to_cart_text"></small>
				</p>
			</div>
			<div class="col-6 text-right">
				<button type="button" class="btn btn-primary btn-add_usual_to_cart"><span class="fa fa-cart-plus fa-lg"></span></button>
			</div>
		</div>
	</form>
	<?php
}


function echo_all_cart_prods_divs() {
	require_once "php/login_utils.php";
	require_once "php/page_parts/cart.php";
	echo get_all_cart_prods_divs(get_user()->cart_id, false);
}
function echo_cmb_user_addresses() {
	require_once "php/page_parts/cart.php";
	echo get_cmb_user_addresses();
}
function echo_cmb_point_addresses() {
	require_once "php/page_parts/cart.php";
	echo get_cmb_point_addresses();
}
function echo_cmb_cards() {
	require_once "php/page_parts/cart.php";
	echo get_cmb_cards();
}

?>
 