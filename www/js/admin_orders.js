$(document).ready(function(){
  setInterval(check_new_order, 5000);
});

function check_new_order() {
	get_all_orders_divs($("#orders-to-accept"), 0);
	get_all_orders_divs($("#orders-to-prepare"), 1);
	get_all_orders_divs($("#orders-to-deliver"), 2);
	get_all_orders_divs($("#orders-delivered"), 3);
	get_all_orders_divs($("#orders-cancelled"), 4);
	console.log("check_new_order success");
}

function get_all_orders_divs(div, state_id) {
	$.ajax({
        type: "POST",
		data: "state_id="+state_id,
        url: "php/web_methods/get_all_orders_divs.php",
        success: function (r) {
			div.html(r);
        },
        failure: function (r) {
            console.log("get_all_orders_divs failed");
        }
    });
}