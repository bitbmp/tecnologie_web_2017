CREATE DATABASE  IF NOT EXISTS `tecweb2017` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tecweb2017`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.1.10    Database: tecweb2017
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(200) NOT NULL,
  `type` int(11) NOT NULL,
  `alias` varchar(45) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `user_fk_idx` (`user`),
  CONSTRAINT `user_fk_of_address` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Via cerca 364, Cesena, IT',0,'Casa mia',104,''),(2,'Via della piadina 2',1,'Al chiosco',1,'\0'),(3,'Via università 3',1,'Campus universita\'',1,'\0'),(4,'Via del negozio 4',0,'Negozio',104,''),(12,'Via rovereto 62, Mercato Saraceno 47025 (FC), IT',0,'Casa',108,'\0'),(13,'Via del Giorgione 11, Cervia 48015 (RA), IT',0,'Casa del lory',108,''),(14,'Via Sacrestano 231, AS 45545 (RA), IT',0,'Casa 2',108,''),(16,'Via Roveretoa 62, Mercato Saraceno 47025 (FC), IT',0,'ASd',108,''),(18,'roveretoa 62, Mercato Saraceno 47025 (FC), IT',0,'Casa2',108,''),(20,'asd 123, asd 12334 (FC), IT',0,'asd',104,'\0'),(23,'asd 12, asdas 12345 (DS), IT',0,'Casa',104,'\0'),(24,'Via rossi 123, Cesena 12345 (FC), IT',0,'Casa mia',118,'\0');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bread_type`
--

DROP TABLE IF EXISTS `bread_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bread_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bread_type`
--

LOCK TABLES `bread_type` WRITE;
/*!40000 ALTER TABLE `bread_type` DISABLE KEYS */;
INSERT INTO `bread_type` VALUES (1,'farina 0',0.00),(2,'farina kamut',0.50),(3,'farina integrale',0.50),(4,'farina 00',0.00);
/*!40000 ALTER TABLE `bread_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(45) NOT NULL,
  `expiration` date NOT NULL,
  `code` decimal(16,0) NOT NULL,
  `cvc` decimal(3,0) NOT NULL,
  `card_type` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `deleted` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `card_type_fk_idx` (`card_type`),
  KEY `user_fk_idx` (`user`),
  CONSTRAINT `card_type_fk_of_card` FOREIGN KEY (`card_type`) REFERENCES `card_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_fk_of_card` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES (1,'Edoardo Barbieri','2020-11-01',1234567890123456,123,1,104,'\0'),(27,'Me','2031-01-01',1234567856785678,123,1,104,'\0'),(28,'sadf','2017-01-01',1234123412342134,123,2,104,''),(56,'Lorenzo Mondani','2030-12-01',7542132659451325,754,2,113,'\0'),(68,'Emanuele Pancisi','2020-02-01',1282734739271792,231,1,108,''),(79,'Emanuele Pancisi','2020-03-01',4736573928329432,432,1,108,''),(80,'Emanuele Pancisi','2021-02-01',2736462371929312,999,3,108,'\0'),(81,'Emanuele Pancisi','2026-11-01',1231234523432424,343,2,108,''),(83,'asd','2018-01-02',2131232131223123,123,1,108,''),(84,'asd','2018-01-01',1232331113111111,321,1,108,''),(85,'asd','2018-01-01',2323213123133222,321,1,108,''),(86,'asd','2018-01-01',3213123231133123,321,1,108,''),(87,'asd','2018-01-01',2131231231231231,231,1,108,''),(88,'as','2018-01-01',2312322123123123,321,1,108,''),(89,'QWE','2018-01-01',1232132321321321,231,1,108,''),(90,'asd','2018-01-01',3123232131213323,123,1,108,''),(91,'Emanuele Pancisi','2021-05-01',5798646654486495,257,2,108,'\0');
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card_type`
--

DROP TABLE IF EXISTS `card_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `card_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card_type`
--

LOCK TABLES `card_type` WRITE;
/*!40000 ALTER TABLE `card_type` DISABLE KEYS */;
INSERT INTO `card_type` VALUES (1,'Visa'),(2,'Postepay'),(3,'MasterCard');
/*!40000 ALTER TABLE `card_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredient`
--

DROP TABLE IF EXISTS `ingredient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredient`
--

LOCK TABLES `ingredient` WRITE;
/*!40000 ALTER TABLE `ingredient` DISABLE KEYS */;
INSERT INTO `ingredient` VALUES (1,'prosciutto crudo',0.50),(2,'squacquerone',0.50),(3,'rucola',0.30),(4,'pomodoro',0.50),(5,'mozzarella',0.50),(6,'salame piccante',0.50),(7,'salsiccia',0.50),(8,'\'nduja',0.50),(9,'erbe',0.50),(10,'pecorino',0.50),(11,'prosciutto cotto',0.50),(12,'fontina',0.50),(13,'cipolla',0.50),(14,'radicchio',0.50),(15,'funghi',0.50),(16,'pancetta',0.50),(17,'gorgonzola',0.50),(25,'Bella fra',0.67),(31,'scamorza',0.50),(32,'melanzane',0.50),(33,'wurstel',0.50),(34,'bresaola',0.50),(35,'grana padano',0.50),(36,'speck',0.50),(37,'noci',0.50),(38,'radicchio rosso',0.50),(39,'pomodoro fresco',0.50),(40,'tonno',0.60),(41,'maionese',0.50);
/*!40000 ALTER TABLE `ingredient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempt`
--

DROP TABLE IF EXISTS `login_attempt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `success` tinyint(4) NOT NULL,
  `date` datetime NOT NULL,
  `from` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `login_attemp_user_fk_idx` (`user`),
  CONSTRAINT `login_attemp_user_fk` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempt`
--

LOCK TABLES `login_attempt` WRITE;
/*!40000 ALTER TABLE `login_attempt` DISABLE KEYS */;
INSERT INTO `login_attempt` VALUES (1,NULL,0,'2017-12-21 14:49:18',''),(39,104,1,'2017-12-21 18:19:00','192.168.1.54'),(40,104,1,'2017-12-21 18:23:52','79.25.7.103'),(41,104,1,'2017-12-21 18:55:53','192.168.1.54'),(44,104,1,'2017-12-21 19:46:23','192.168.1.54'),(45,104,1,'2017-12-21 19:47:54','79.25.7.103'),(46,104,1,'2017-12-21 20:42:35','192.168.1.54'),(47,104,1,'2017-12-21 20:44:20','192.168.1.54'),(49,104,1,'2017-12-21 23:56:58','93.40.208.173'),(50,104,1,'2017-12-22 00:00:03','93.40.208.173'),(52,108,1,'2017-12-22 11:30:42','130.193.72.73'),(53,104,1,'2017-12-22 14:00:03','192.168.1.54'),(54,108,1,'2017-12-22 14:50:59','130.193.72.73'),(55,104,1,'2017-12-22 15:13:01','79.25.7.103'),(56,108,1,'2017-12-22 16:14:36','130.193.72.73'),(59,104,1,'2017-12-22 18:52:30','192.168.1.54'),(60,108,1,'2017-12-22 19:21:44','5.90.223.216'),(61,104,1,'2017-12-22 19:26:36','151.38.82.12'),(62,104,1,'2017-12-22 21:23:00','79.25.7.103'),(63,104,1,'2017-12-22 22:26:19','79.25.7.103'),(66,104,1,'2017-12-23 01:58:18','151.18.135.110'),(67,108,1,'2017-12-23 11:08:58','130.193.72.73'),(70,108,1,'2017-12-24 15:42:41','130.193.72.73'),(71,104,1,'2017-12-24 17:27:13','192.168.1.54'),(72,113,1,'2017-12-24 17:44:38','95.239.226.156'),(73,104,1,'2017-12-24 18:34:38','192.168.1.54'),(74,108,1,'2017-12-24 19:00:01','5.90.223.216'),(76,104,1,'2017-12-24 19:01:19','79.40.60.92'),(77,104,1,'2017-12-24 19:14:33','192.168.1.54'),(79,104,1,'2017-12-25 16:58:47','151.34.56.54'),(80,108,1,'2017-12-27 11:22:16','130.193.72.73'),(81,104,1,'2017-12-27 18:44:48','151.38.51.119'),(82,108,1,'2017-12-28 11:25:47','130.193.72.73'),(83,113,1,'2017-12-28 16:37:44','79.27.143.205'),(84,108,1,'2017-12-28 19:23:21','5.90.212.220'),(85,108,1,'2017-12-29 12:34:29','130.193.72.73'),(86,104,1,'2017-12-29 16:26:57','151.38.12.214'),(87,113,1,'2017-12-29 17:53:43','80.104.159.10'),(88,108,1,'2017-12-30 02:29:21','5.90.217.24'),(89,108,1,'2017-12-30 11:44:28','130.193.72.73'),(90,108,1,'2017-12-30 15:51:51','5.90.217.24'),(91,113,1,'2017-12-30 21:07:31','79.18.246.195'),(93,104,1,'2017-12-31 14:18:16','192.168.1.54'),(94,108,1,'2017-12-31 16:02:18','130.193.72.73'),(96,113,1,'2018-01-01 10:40:55','2.234.140.14'),(97,108,1,'2018-01-02 12:43:35','130.193.72.73'),(98,104,1,'2018-01-02 17:59:19','80.180.130.223'),(99,104,1,'2018-01-02 18:11:36','80.180.130.223'),(100,113,1,'2018-01-02 18:12:44','80.104.159.10'),(101,104,1,'2018-01-02 22:56:13','80.180.130.223'),(102,104,1,'2018-01-03 11:39:08','80.180.130.223'),(103,108,1,'2018-01-03 11:41:18','130.193.72.73'),(104,104,1,'2018-01-03 11:42:12','80.180.130.223'),(105,104,1,'2018-01-03 11:47:28','80.180.130.223'),(106,108,1,'2018-01-03 11:57:21','130.193.72.73'),(107,104,1,'2018-01-03 12:29:58','80.180.130.223'),(108,104,1,'2018-01-03 12:30:42','80.180.130.223'),(110,108,1,'2018-01-03 12:38:31','5.90.44.170'),(112,113,1,'2018-01-03 14:35:05','80.104.159.10'),(113,108,1,'2018-01-03 14:37:46','130.193.72.73'),(114,108,1,'2018-01-03 14:38:38','130.193.72.73'),(115,108,1,'2018-01-03 14:52:50','5.90.220.17'),(116,104,1,'2018-01-03 14:58:26','80.180.130.223'),(118,104,1,'2018-01-03 15:48:07','80.180.130.223'),(119,108,1,'2018-01-03 16:20:06','130.193.72.73'),(122,113,1,'2018-01-03 16:20:35','80.104.159.10'),(123,108,1,'2018-01-03 16:25:07','5.90.220.17'),(124,104,1,'2018-01-03 16:31:02','80.180.130.223'),(125,108,1,'2018-01-03 16:32:05','130.193.72.73'),(126,104,1,'2018-01-03 16:40:18','80.180.130.223'),(127,113,1,'2018-01-03 16:40:40','80.104.159.10'),(128,108,1,'2018-01-03 16:40:41','130.193.72.73'),(129,108,1,'2018-01-03 16:43:14','130.193.72.73'),(130,108,1,'2018-01-03 17:02:29','130.193.72.73'),(132,104,1,'2018-01-03 23:25:41','80.180.130.223'),(133,108,1,'2018-01-03 23:49:47','5.90.73.230'),(134,108,1,'2018-01-04 10:07:39','130.193.72.73'),(135,104,1,'2018-01-04 11:01:56','80.180.130.223'),(136,104,1,'2018-01-04 14:50:22','80.180.130.223'),(137,113,1,'2018-01-04 15:11:48','80.104.159.10'),(138,108,1,'2018-01-04 16:21:03','130.193.72.73'),(139,104,1,'2018-01-04 16:22:28','80.180.130.223'),(140,104,1,'2018-01-04 16:23:00','80.180.130.223'),(141,108,1,'2018-01-04 16:23:42','130.193.72.73'),(142,104,1,'2018-01-04 17:59:33','80.180.130.223'),(145,108,1,'2018-01-05 13:15:47','5.90.73.26'),(146,108,1,'2018-01-05 14:08:26','130.193.72.73'),(148,108,1,'2018-01-05 15:14:42','130.193.72.73'),(150,104,1,'2018-01-05 16:33:55','80.180.130.223'),(157,104,1,'2018-01-05 17:58:50','80.180.130.223'),(158,104,1,'2018-01-05 18:00:33','80.180.130.223'),(159,108,1,'2018-01-05 18:09:24','130.193.72.73'),(160,113,1,'2018-01-05 19:42:17','80.104.159.10'),(161,104,1,'2018-01-05 22:00:44','80.180.130.223'),(162,104,1,'2018-01-06 19:51:12','151.38.114.181'),(163,113,1,'2018-01-07 16:55:49','80.104.159.10'),(164,104,1,'2018-01-07 17:09:54','80.180.130.223'),(165,118,1,'2018-01-07 17:29:17','80.180.130.223'),(166,104,1,'2018-01-07 17:31:05','80.180.130.223');
/*!40000 ALTER TABLE `login_attempt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `disposed` tinyint(4) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `notifications_fk_user` (`user`),
  CONSTRAINT `notifications_fk_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7223 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (6983,113,'		Il tuo <a href=\"order_details.php?id=142\">ordine #142</a> Ã¨ in preparazione\r\n		',1,'2018-01-04 19:32:38'),(6984,1,'		Hai accettato <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',0,'2018-01-04 19:32:38'),(6985,104,'		Hai accettato <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',1,'2018-01-04 19:32:38'),(6986,108,'		Hai accettato <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',1,'2018-01-04 19:32:39'),(6987,113,'		Hai accettato <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',1,'2018-01-04 19:32:39'),(6988,113,'		Il tuo <a href=\"order_details.php?id=143\">ordine #143</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 19:35:44'),(6989,1,'		Un nuovo <a href=\"order_details.php?id=143\">ordine #143</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-04 19:35:44'),(6990,104,'		Un nuovo <a href=\"order_details.php?id=143\">ordine #143</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 19:35:44'),(6991,108,'		Un nuovo <a href=\"order_details.php?id=143\">ordine #143</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 19:35:44'),(6992,113,'		Un nuovo <a href=\"order_details.php?id=143\">ordine #143</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 19:35:44'),(6993,113,'		Il tuo <a href=\"order_details.php?id=143\">ordine #143</a> Ã¨ stato cancellato dal venditore\r\n		',1,'2018-01-04 19:39:00'),(6994,1,'		Hai cancellato <a href=\"order_details.php?id=143\">l\'ordine #143</a>\r\n		',0,'2018-01-04 19:39:00'),(6995,104,'		Hai cancellato <a href=\"order_details.php?id=143\">l\'ordine #143</a>\r\n		',1,'2018-01-04 19:39:00'),(6996,108,'		Hai cancellato <a href=\"order_details.php?id=143\">l\'ordine #143</a>\r\n		',1,'2018-01-04 19:39:00'),(6997,113,'		Hai cancellato <a href=\"order_details.php?id=143\">l\'ordine #143</a>\r\n		',1,'2018-01-04 19:39:00'),(6999,1,'		Hai cancellato <a href=\"order_details.php?id=102\">l\'ordine #102</a>\r\n		',0,'2018-01-04 20:08:37'),(7000,104,'		Hai cancellato <a href=\"order_details.php?id=102\">l\'ordine #102</a>\r\n		',1,'2018-01-04 20:08:37'),(7001,108,'		Hai cancellato <a href=\"order_details.php?id=102\">l\'ordine #102</a>\r\n		',1,'2018-01-04 20:08:37'),(7002,113,'		Hai cancellato <a href=\"order_details.php?id=102\">l\'ordine #102</a>\r\n		',1,'2018-01-04 20:08:37'),(7004,1,'		Un nuovo <a href=\"order_details.php?id=144\">ordine #144</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-04 20:25:06'),(7005,104,'		Un nuovo <a href=\"order_details.php?id=144\">ordine #144</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:25:06'),(7006,108,'		Un nuovo <a href=\"order_details.php?id=144\">ordine #144</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:25:06'),(7007,113,'		Un nuovo <a href=\"order_details.php?id=144\">ordine #144</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:25:06'),(7009,1,'	<a href=\"order_details.php?id=144\">L\'ordine #144</a> Ã¨ stato annullato dal cliente\r\n	',0,'2018-01-04 20:26:08'),(7010,104,'	<a href=\"order_details.php?id=144\">L\'ordine #144</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-04 20:26:08'),(7011,108,'	<a href=\"order_details.php?id=144\">L\'ordine #144</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-04 20:26:08'),(7012,113,'	<a href=\"order_details.php?id=144\">L\'ordine #144</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-04 20:26:08'),(7014,1,'		Un nuovo <a href=\"order_details.php?id=145\">ordine #145</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-04 20:26:35'),(7015,104,'		Un nuovo <a href=\"order_details.php?id=145\">ordine #145</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:26:35'),(7016,108,'		Un nuovo <a href=\"order_details.php?id=145\">ordine #145</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:26:35'),(7017,113,'		Un nuovo <a href=\"order_details.php?id=145\">ordine #145</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:26:35'),(7019,1,'		Hai accettato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',0,'2018-01-04 20:26:45'),(7020,104,'		Hai accettato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:26:45'),(7021,108,'		Hai accettato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:26:45'),(7022,113,'		Hai accettato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:26:45'),(7024,1,'		Hai messo in consegna <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',0,'2018-01-04 20:28:51'),(7025,104,'		Hai messo in consegna <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:28:52'),(7026,108,'		Hai messo in consegna <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:28:52'),(7027,113,'		Hai messo in consegna <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:28:52'),(7029,1,'		Hai consegnato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',0,'2018-01-04 20:29:09'),(7030,104,'		Hai consegnato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:29:09'),(7031,108,'		Hai consegnato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:29:10'),(7032,113,'		Hai consegnato <a href=\"order_details.php?id=145\">l\'ordine #145</a>\r\n		',1,'2018-01-04 20:29:10'),(7034,1,'		Un nuovo <a href=\"order_details.php?id=146\">ordine #146</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-04 20:40:04'),(7035,104,'		Un nuovo <a href=\"order_details.php?id=146\">ordine #146</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:40:04'),(7036,108,'		Un nuovo <a href=\"order_details.php?id=146\">ordine #146</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:40:04'),(7037,113,'		Un nuovo <a href=\"order_details.php?id=146\">ordine #146</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-04 20:40:05'),(7039,1,'	<a href=\"order_details.php?id=146\">L\'ordine #146</a> Ã¨ stato annullato dal cliente\r\n	',0,'2018-01-04 20:40:23'),(7040,104,'	<a href=\"order_details.php?id=146\">L\'ordine #146</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-04 20:40:23'),(7041,108,'	<a href=\"order_details.php?id=146\">L\'ordine #146</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-04 20:40:23'),(7042,113,'	<a href=\"order_details.php?id=146\">L\'ordine #146</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-04 20:40:23'),(7043,113,'		Il tuo <a href=\"order_details.php?id=142\">ordine #142</a> Ã¨ pronto\r\n		',1,'2018-01-04 20:42:02'),(7044,1,'		Hai messo in consegna <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',0,'2018-01-04 20:42:03'),(7045,104,'		Hai messo in consegna <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',1,'2018-01-04 20:42:03'),(7046,108,'		Hai messo in consegna <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',1,'2018-01-04 20:42:03'),(7047,113,'		Hai messo in consegna <a href=\"order_details.php?id=142\">l\'ordine #142</a>\r\n		',1,'2018-01-04 20:42:03'),(7048,108,'	Il tuo <a href=\"order_details.php?id=87\">ordine #87</a> Ã¨ stato annullato come richiesto\r\n	',1,'2018-01-05 13:17:41'),(7049,1,'	<a href=\"order_details.php?id=87\">L\'ordine #87</a> Ã¨ stato annullato dal cliente\r\n	',0,'2018-01-05 13:17:41'),(7050,104,'	<a href=\"order_details.php?id=87\">L\'ordine #87</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-05 13:17:41'),(7051,108,'	<a href=\"order_details.php?id=87\">L\'ordine #87</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-05 13:17:41'),(7052,113,'	<a href=\"order_details.php?id=87\">L\'ordine #87</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-05 13:17:41'),(7054,1,'		Un nuovo <a href=\"order_details.php?id=147\">ordine #147</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 15:13:54'),(7055,104,'		Un nuovo <a href=\"order_details.php?id=147\">ordine #147</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 15:13:54'),(7056,108,'		Un nuovo <a href=\"order_details.php?id=147\">ordine #147</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 15:13:54'),(7057,113,'		Un nuovo <a href=\"order_details.php?id=147\">ordine #147</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 15:13:55'),(7059,1,'		Hai accettato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',0,'2018-01-05 15:15:22'),(7060,104,'		Hai accettato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:15:22'),(7061,108,'		Hai accettato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:15:22'),(7062,113,'		Hai accettato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:15:22'),(7064,1,'		Hai messo in consegna <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',0,'2018-01-05 15:18:01'),(7065,104,'		Hai messo in consegna <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:18:01'),(7066,108,'		Hai messo in consegna <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:18:01'),(7067,113,'		Hai messo in consegna <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:18:01'),(7069,1,'		Hai consegnato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',0,'2018-01-05 15:18:35'),(7070,104,'		Hai consegnato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:18:35'),(7071,108,'		Hai consegnato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:18:35'),(7072,113,'		Hai consegnato <a href=\"order_details.php?id=147\">l\'ordine #147</a>\r\n		',1,'2018-01-05 15:18:35'),(7073,104,'		Il tuo <a href=\"order_details.php?id=148\">ordine #148</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:34:51'),(7074,1,'		Un nuovo <a href=\"order_details.php?id=148\">ordine #148</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 16:34:52'),(7075,104,'		Un nuovo <a href=\"order_details.php?id=148\">ordine #148</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:34:52'),(7076,108,'		Un nuovo <a href=\"order_details.php?id=148\">ordine #148</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:34:52'),(7077,113,'		Un nuovo <a href=\"order_details.php?id=148\">ordine #148</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:34:52'),(7078,104,'		Il tuo <a href=\"order_details.php?id=149\">ordine #149</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:47:40'),(7079,1,'		Un nuovo <a href=\"order_details.php?id=149\">ordine #149</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 16:47:40'),(7080,104,'		Un nuovo <a href=\"order_details.php?id=149\">ordine #149</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:47:40'),(7081,108,'		Un nuovo <a href=\"order_details.php?id=149\">ordine #149</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:47:40'),(7082,113,'		Un nuovo <a href=\"order_details.php?id=149\">ordine #149</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 16:47:40'),(7083,104,'		Il tuo <a href=\"order_details.php?id=149\">ordine #149</a> Ã¨ stato cancellato dal venditore\r\n		',1,'2018-01-05 17:47:35'),(7084,1,'		Hai cancellato <a href=\"order_details.php?id=149\">l\'ordine #149</a>\r\n		',0,'2018-01-05 17:47:35'),(7085,104,'		Hai cancellato <a href=\"order_details.php?id=149\">l\'ordine #149</a>\r\n		',1,'2018-01-05 17:47:35'),(7086,108,'		Hai cancellato <a href=\"order_details.php?id=149\">l\'ordine #149</a>\r\n		',1,'2018-01-05 17:47:35'),(7087,113,'		Hai cancellato <a href=\"order_details.php?id=149\">l\'ordine #149</a>\r\n		',1,'2018-01-05 17:47:35'),(7088,104,'		Il tuo <a href=\"order_details.php?id=148\">ordine #148</a> Ã¨ stato cancellato dal venditore\r\n		',1,'2018-01-05 17:47:40'),(7089,1,'		Hai cancellato <a href=\"order_details.php?id=148\">l\'ordine #148</a>\r\n		',0,'2018-01-05 17:47:40'),(7090,104,'		Hai cancellato <a href=\"order_details.php?id=148\">l\'ordine #148</a>\r\n		',1,'2018-01-05 17:47:40'),(7091,108,'		Hai cancellato <a href=\"order_details.php?id=148\">l\'ordine #148</a>\r\n		',1,'2018-01-05 17:47:40'),(7092,113,'		Hai cancellato <a href=\"order_details.php?id=148\">l\'ordine #148</a>\r\n		',1,'2018-01-05 17:47:40'),(7094,1,'		Un nuovo <a href=\"order_details.php?id=150\">ordine #150</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 17:58:30'),(7095,104,'		Un nuovo <a href=\"order_details.php?id=150\">ordine #150</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 17:58:30'),(7096,108,'		Un nuovo <a href=\"order_details.php?id=150\">ordine #150</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 17:58:30'),(7097,113,'		Un nuovo <a href=\"order_details.php?id=150\">ordine #150</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 17:58:30'),(7099,1,'		Hai accettato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',0,'2018-01-05 17:59:05'),(7100,104,'		Hai accettato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:05'),(7101,108,'		Hai accettato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:05'),(7102,113,'		Hai accettato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:05'),(7104,1,'		Hai cancellato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',0,'2018-01-05 17:59:21'),(7105,104,'		Hai cancellato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:21'),(7106,108,'		Hai cancellato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:21'),(7107,113,'		Hai cancellato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:21'),(7109,1,'		Hai messo in consegna <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',0,'2018-01-05 17:59:21'),(7110,104,'		Hai messo in consegna <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:21'),(7111,108,'		Hai messo in consegna <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:21'),(7112,113,'		Hai messo in consegna <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:21'),(7114,1,'		Hai consegnato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',0,'2018-01-05 17:59:24'),(7115,104,'		Hai consegnato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:24'),(7116,108,'		Hai consegnato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:25'),(7117,113,'		Hai consegnato <a href=\"order_details.php?id=150\">l\'ordine #150</a>\r\n		',1,'2018-01-05 17:59:25'),(7118,108,'		Il tuo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:04:15'),(7119,1,'		Un nuovo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 18:04:15'),(7120,104,'		Un nuovo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:04:15'),(7121,108,'		Un nuovo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:04:15'),(7122,113,'		Un nuovo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:04:15'),(7123,108,'		Il tuo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ in preparazione\r\n		',1,'2018-01-05 18:14:27'),(7124,1,'		Hai accettato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',0,'2018-01-05 18:14:27'),(7125,104,'		Hai accettato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:27'),(7126,108,'		Hai accettato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:27'),(7127,113,'		Hai accettato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:27'),(7128,108,'		Il tuo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ pronto\r\n		',1,'2018-01-05 18:14:30'),(7129,1,'		Hai messo in consegna <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',0,'2018-01-05 18:14:30'),(7130,104,'		Hai messo in consegna <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:30'),(7131,108,'		Hai messo in consegna <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:30'),(7132,113,'		Hai messo in consegna <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:30'),(7133,108,'		Il tuo <a href=\"order_details.php?id=151\">ordine #151</a> Ã¨ stato cancellato dal venditore\r\n		',1,'2018-01-05 18:14:39'),(7134,1,'		Hai cancellato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',0,'2018-01-05 18:14:39'),(7135,104,'		Hai cancellato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:39'),(7136,108,'		Hai cancellato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:39'),(7137,113,'		Hai cancellato <a href=\"order_details.php?id=151\">l\'ordine #151</a>\r\n		',1,'2018-01-05 18:14:39'),(7138,104,'		Il tuo <a href=\"order_details.php?id=152\">ordine #152</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:05'),(7139,1,'		Un nuovo <a href=\"order_details.php?id=152\">ordine #152</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 18:30:05'),(7140,104,'		Un nuovo <a href=\"order_details.php?id=152\">ordine #152</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:05'),(7141,108,'		Un nuovo <a href=\"order_details.php?id=152\">ordine #152</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:05'),(7142,113,'		Un nuovo <a href=\"order_details.php?id=152\">ordine #152</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:05'),(7143,104,'		Il tuo <a href=\"order_details.php?id=152\">ordine #152</a> Ã¨ in preparazione\r\n		',1,'2018-01-05 18:30:18'),(7144,1,'		Hai accettato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',0,'2018-01-05 18:30:18'),(7145,104,'		Hai accettato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',1,'2018-01-05 18:30:18'),(7146,108,'		Hai accettato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',1,'2018-01-05 18:30:18'),(7147,113,'		Hai accettato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',1,'2018-01-05 18:30:19'),(7148,104,'		Il tuo <a href=\"order_details.php?id=152\">ordine #152</a> Ã¨ stato cancellato dal venditore\r\n		',1,'2018-01-05 18:30:19'),(7149,1,'		Hai cancellato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',0,'2018-01-05 18:30:19'),(7150,104,'		Hai cancellato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',1,'2018-01-05 18:30:19'),(7151,108,'		Hai cancellato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',1,'2018-01-05 18:30:19'),(7152,113,'		Hai cancellato <a href=\"order_details.php?id=152\">l\'ordine #152</a>\r\n		',1,'2018-01-05 18:30:19'),(7153,104,'		Il tuo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:42'),(7154,1,'		Un nuovo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 18:30:42'),(7155,104,'		Un nuovo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:42'),(7156,108,'		Un nuovo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:42'),(7157,113,'		Un nuovo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:30:42'),(7158,104,'		Il tuo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ in preparazione\r\n		',1,'2018-01-05 18:30:48'),(7159,1,'		Hai accettato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',0,'2018-01-05 18:30:48'),(7160,104,'		Hai accettato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:48'),(7161,108,'		Hai accettato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:49'),(7162,113,'		Hai accettato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:49'),(7163,104,'		Il tuo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ pronto\r\n		',1,'2018-01-05 18:30:49'),(7164,1,'		Hai messo in consegna <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',0,'2018-01-05 18:30:49'),(7165,104,'		Hai messo in consegna <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:49'),(7166,108,'		Hai messo in consegna <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:49'),(7167,113,'		Hai messo in consegna <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:49'),(7168,104,'		Il tuo <a href=\"order_details.php?id=153\">ordine #153</a> Ã¨ stato ritirato\r\n		',1,'2018-01-05 18:30:50'),(7169,1,'		Hai consegnato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',0,'2018-01-05 18:30:50'),(7170,104,'		Hai consegnato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:50'),(7171,108,'		Hai consegnato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:50'),(7172,113,'		Hai consegnato <a href=\"order_details.php?id=153\">l\'ordine #153</a>\r\n		',1,'2018-01-05 18:30:50'),(7173,104,'		Il tuo <a href=\"order_details.php?id=154\">ordine #154</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:32:11'),(7174,1,'		Un nuovo <a href=\"order_details.php?id=154\">ordine #154</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-05 18:32:11'),(7175,104,'		Un nuovo <a href=\"order_details.php?id=154\">ordine #154</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:32:11'),(7176,108,'		Un nuovo <a href=\"order_details.php?id=154\">ordine #154</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:32:11'),(7177,113,'		Un nuovo <a href=\"order_details.php?id=154\">ordine #154</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-05 18:32:11'),(7178,104,'		Il tuo <a href=\"order_details.php?id=154\">ordine #154</a> Ã¨ in preparazione\r\n		',1,'2018-01-05 18:32:19'),(7179,1,'		Hai accettato <a href=\"order_details.php?id=154\">l\'ordine #154</a>\r\n		',0,'2018-01-05 18:32:19'),(7180,104,'		Hai accettato <a href=\"order_details.php?id=154\">l\'ordine #154</a>\r\n		',1,'2018-01-05 18:32:19'),(7181,108,'		Hai accettato <a href=\"order_details.php?id=154\">l\'ordine #154</a>\r\n		',1,'2018-01-05 18:32:19'),(7182,113,'		Hai accettato <a href=\"order_details.php?id=154\">l\'ordine #154</a>\r\n		',1,'2018-01-05 18:32:20'),(7183,108,'		Il tuo <a href=\"order_details.php?id=98\">ordine #98</a> Ã¨ in preparazione\r\n		',1,'2018-01-05 18:35:43'),(7184,1,'		Hai accettato <a href=\"order_details.php?id=98\">l\'ordine #98</a>\r\n		',0,'2018-01-05 18:35:43'),(7185,104,'		Hai accettato <a href=\"order_details.php?id=98\">l\'ordine #98</a>\r\n		',1,'2018-01-05 18:35:43'),(7186,108,'		Hai accettato <a href=\"order_details.php?id=98\">l\'ordine #98</a>\r\n		',1,'2018-01-05 18:35:43'),(7187,113,'		Hai accettato <a href=\"order_details.php?id=98\">l\'ordine #98</a>\r\n		',1,'2018-01-05 18:35:43'),(7188,104,'		Il tuo <a href=\"order_details.php?id=155\">ordine #155</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-06 19:51:35'),(7189,1,'		Un nuovo <a href=\"order_details.php?id=155\">ordine #155</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-06 19:51:35'),(7190,104,'		Un nuovo <a href=\"order_details.php?id=155\">ordine #155</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-06 19:51:35'),(7191,108,'		Un nuovo <a href=\"order_details.php?id=155\">ordine #155</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-06 19:51:35'),(7192,113,'		Un nuovo <a href=\"order_details.php?id=155\">ordine #155</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-06 19:51:35'),(7193,104,'		Il tuo <a href=\"order_details.php?id=155\">ordine #155</a> Ã¨ in preparazione\r\n		',1,'2018-01-06 19:51:43'),(7194,1,'		Hai accettato <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',0,'2018-01-06 19:51:43'),(7195,104,'		Hai accettato <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',1,'2018-01-06 19:51:43'),(7196,108,'		Hai accettato <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',0,'2018-01-06 19:51:43'),(7197,113,'		Hai accettato <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',1,'2018-01-06 19:51:43'),(7198,104,'		Il tuo <a href=\"order_details.php?id=155\">ordine #155</a> Ã¨ pronto\r\n		',1,'2018-01-06 19:53:47'),(7199,1,'		Hai messo in consegna <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',0,'2018-01-06 19:53:47'),(7200,104,'		Hai messo in consegna <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',1,'2018-01-06 19:53:47'),(7201,108,'		Hai messo in consegna <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',0,'2018-01-06 19:53:47'),(7202,113,'		Hai messo in consegna <a href=\"order_details.php?id=155\">l\'ordine #155</a>\r\n		',1,'2018-01-06 19:53:48'),(7203,104,'		Il tuo <a href=\"order_details.php?id=156\">ordine #156</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-07 17:08:40'),(7204,1,'		Un nuovo <a href=\"order_details.php?id=156\">ordine #156</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:08:40'),(7205,104,'		Un nuovo <a href=\"order_details.php?id=156\">ordine #156</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-07 17:08:40'),(7206,108,'		Un nuovo <a href=\"order_details.php?id=156\">ordine #156</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:08:40'),(7207,113,'		Un nuovo <a href=\"order_details.php?id=156\">ordine #156</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-07 17:08:40'),(7208,118,'		Il tuo <a href=\"order_details.php?id=157\">ordine #157</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-07 17:30:12'),(7209,1,'		Un nuovo <a href=\"order_details.php?id=157\">ordine #157</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:30:12'),(7210,104,'		Un nuovo <a href=\"order_details.php?id=157\">ordine #157</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-07 17:30:12'),(7211,108,'		Un nuovo <a href=\"order_details.php?id=157\">ordine #157</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:30:12'),(7212,113,'		Un nuovo <a href=\"order_details.php?id=157\">ordine #157</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:30:12'),(7213,118,'	Il tuo <a href=\"order_details.php?id=157\">ordine #157</a> Ã¨ stato annullato come richiesto\r\n	',1,'2018-01-07 17:30:33'),(7214,1,'	<a href=\"order_details.php?id=157\">L\'ordine #157</a> Ã¨ stato annullato dal cliente\r\n	',0,'2018-01-07 17:30:33'),(7215,104,'	<a href=\"order_details.php?id=157\">L\'ordine #157</a> Ã¨ stato annullato dal cliente\r\n	',1,'2018-01-07 17:30:33'),(7216,108,'	<a href=\"order_details.php?id=157\">L\'ordine #157</a> Ã¨ stato annullato dal cliente\r\n	',0,'2018-01-07 17:30:33'),(7217,113,'	<a href=\"order_details.php?id=157\">L\'ordine #157</a> Ã¨ stato annullato dal cliente\r\n	',0,'2018-01-07 17:30:33'),(7218,118,'		Il tuo <a href=\"order_details.php?id=158\">ordine #158</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-07 17:30:48'),(7219,1,'		Un nuovo <a href=\"order_details.php?id=158\">ordine #158</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:30:48'),(7220,104,'		Un nuovo <a href=\"order_details.php?id=158\">ordine #158</a> Ã¨ stato ricevuto\r\n		',1,'2018-01-07 17:30:49'),(7221,108,'		Un nuovo <a href=\"order_details.php?id=158\">ordine #158</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:30:49'),(7222,113,'		Un nuovo <a href=\"order_details.php?id=158\">ordine #158</a> Ã¨ stato ricevuto\r\n		',0,'2018-01-07 17:30:49');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime NOT NULL,
  `desired_date` datetime NOT NULL,
  `pay_state` int(11) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `deliver_price` decimal(5,2) NOT NULL DEFAULT '0.00',
  `user` int(11) NOT NULL,
  `address` int(11) NOT NULL,
  `card` int(11) DEFAULT NULL,
  `product_list` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_fk_of_order_idx` (`user`),
  KEY `address_fk_of_order_idx` (`address`),
  KEY `card_fk_of_order_idx` (`card`),
  KEY `product_list_fk_of_order_idx` (`product_list`),
  CONSTRAINT `address_fk_of_order` FOREIGN KEY (`address`) REFERENCES `address` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `card_fk_of_order` FOREIGN KEY (`card`) REFERENCES `card` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `product_list_fk_of_order` FOREIGN KEY (`product_list`) REFERENCES `product_list` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_fk_of_order` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (11,'2017-12-24 21:10:58','2017-12-24 20:00:00',0,1.00,1.00,104,2,27,125),(12,'2017-12-24 21:11:05','2017-12-24 20:00:00',0,5.00,1.00,104,2,27,126),(16,'2017-12-24 21:13:37','2017-12-24 20:00:00',0,5.00,1.00,104,2,27,130),(22,'2017-12-24 21:15:23','2017-12-24 20:00:00',0,1.00,1.00,104,2,1,136),(28,'2017-12-24 21:17:11','2017-12-24 20:00:00',0,1.00,1.00,104,2,NULL,142),(29,'2017-12-24 21:17:21','2017-12-24 20:00:00',0,1.00,1.00,104,2,1,143),(30,'2017-12-24 21:17:30','2017-12-24 20:00:00',0,1.00,1.00,104,2,1,144),(31,'2017-12-24 21:17:33','2017-12-24 20:00:00',0,1.00,1.00,104,2,NULL,145),(32,'2017-12-24 21:17:39','2017-12-24 20:00:00',0,1.00,1.00,104,2,NULL,146),(33,'2017-12-24 21:17:56','2017-12-24 20:00:00',0,1.00,1.00,104,1,NULL,147),(34,'2017-12-24 21:18:00','2017-12-24 20:00:00',0,1.00,1.00,104,2,NULL,148),(35,'2017-12-24 21:18:33','2017-12-24 20:00:00',0,4.00,0.00,104,2,NULL,149),(36,'2017-12-24 21:18:48','2017-12-24 20:00:00',0,5.50,2.00,104,1,NULL,150),(37,'2017-12-24 21:19:47','2017-12-24 20:00:00',0,7.50,0.00,104,2,NULL,151),(38,'2017-12-24 21:25:34','2017-12-24 20:00:00',0,4.00,0.00,104,2,NULL,152),(39,'2017-12-24 21:27:21','2017-12-24 20:00:00',0,14.00,2.00,104,4,27,153),(40,'2017-12-24 22:28:32','2017-12-24 21:00:00',0,32.00,0.00,104,2,NULL,154),(42,'2017-12-25 14:34:56','2017-12-25 14:00:00',0,11.00,0.00,108,2,NULL,156),(43,'2017-12-25 14:36:12','2017-12-25 14:45:00',0,6.00,0.00,108,2,68,157),(44,'2017-12-25 17:01:32','2017-12-25 16:00:00',0,12.50,0.00,104,2,27,158),(45,'2017-12-26 12:04:13','2017-12-26 11:00:00',0,4.00,0.00,104,2,NULL,159),(46,'2017-12-27 16:41:20','2017-12-29 16:00:00',0,4.00,0.00,113,2,56,160),(47,'2017-12-27 16:41:26','2017-12-27 16:00:00',0,1.00,0.00,108,2,68,161),(48,'2017-12-28 15:54:26','2017-12-28 15:00:00',0,3.00,2.00,108,12,80,162),(49,'2017-12-28 17:29:49','2017-12-28 16:00:00',0,4.00,2.00,108,12,80,163),(50,'2017-12-28 17:41:22','2017-12-28 17:00:00',0,10.50,2.00,108,12,80,164),(51,'2017-12-28 19:08:05','2017-12-28 18:00:00',0,10.00,2.00,108,12,80,165),(52,'2017-12-28 19:08:52','2017-12-28 18:00:00',0,2.00,2.00,108,12,80,166),(57,'2017-12-28 19:15:06','2017-12-28 18:00:00',0,1.00,0.00,108,3,80,171),(58,'2017-12-28 19:16:08','2017-12-28 18:00:00',0,3.00,2.00,108,12,80,172),(59,'2017-12-28 19:18:51','2017-12-28 18:00:00',0,11.50,0.00,113,2,NULL,173),(60,'2017-12-29 13:03:00','2017-12-29 12:00:00',0,3.00,2.00,108,12,80,174),(61,'2017-12-29 16:27:17','2017-12-29 15:00:00',0,4.00,0.00,104,2,27,175),(62,'2017-12-30 11:44:09','2017-12-30 11:00:00',0,7.50,0.00,113,2,NULL,176),(63,'2017-12-30 11:44:55','2017-12-30 11:00:00',0,1007.49,0.00,113,3,56,177),(64,'2017-12-30 14:58:06','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,178),(65,'2017-12-30 14:58:53','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,179),(66,'2017-12-30 15:02:05','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,180),(67,'2017-12-30 15:07:04','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,181),(68,'2017-12-30 15:08:10','2017-12-30 14:00:00',0,7.50,0.00,113,2,56,182),(69,'2017-12-30 15:09:58','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,183),(70,'2017-12-30 15:12:09','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,184),(71,'2017-12-30 15:12:28','2017-12-30 14:00:00',0,7.00,0.00,113,2,56,185),(72,'2017-12-30 15:13:45','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,186),(73,'2017-12-30 15:15:21','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,187),(74,'2017-12-30 15:15:44','2017-12-30 14:00:00',0,1003.99,0.00,113,2,56,188),(75,'2017-12-30 15:20:04','2017-12-30 14:00:00',0,1003.99,0.00,113,2,56,189),(76,'2017-12-30 15:20:13','2017-12-30 14:00:00',0,6.00,2.00,108,12,80,190),(77,'2017-12-30 15:20:58','2017-12-30 14:00:00',0,1003.49,0.00,113,2,56,191),(78,'2017-12-30 15:22:35','2017-12-30 14:00:00',0,7.50,0.00,113,2,56,192),(79,'2017-12-30 15:22:39','2017-12-30 14:00:00',0,3.00,2.00,108,12,80,193),(80,'2017-12-30 15:24:23','2017-12-30 14:00:00',0,1003.99,0.00,113,2,56,194),(81,'2017-12-30 15:25:39','2017-12-30 14:00:00',0,4.00,0.00,113,2,56,195),(82,'2017-12-30 15:27:20','2017-12-30 14:00:00',0,4.00,0.00,113,2,56,196),(83,'2017-12-30 15:27:48','2017-12-30 14:00:00',0,4.00,0.00,113,2,56,197),(84,'2017-12-30 15:51:59','2017-12-30 15:00:00',0,6.00,2.00,108,12,80,198),(85,'2017-12-30 22:16:14','2017-12-30 21:00:00',0,3.00,0.00,113,2,56,199),(87,'2017-12-31 13:09:58','2017-12-31 12:00:00',0,1.00,0.00,108,2,NULL,201),(89,'2017-12-31 13:17:13','2017-12-31 12:00:00',0,1.00,0.00,108,2,80,203),(90,'2017-12-31 14:18:20','2017-12-31 13:00:00',0,6.00,2.00,104,1,27,204),(92,'2018-01-01 10:41:48','2018-01-01 10:00:00',0,7.50,0.00,113,2,56,206),(93,'2018-01-02 12:26:29','2018-01-02 11:00:00',0,1.00,0.00,113,2,56,207),(94,'2018-01-02 18:18:30','2018-01-02 17:00:00',0,10.00,2.00,104,1,27,208),(95,'2018-01-02 19:37:41','2018-01-02 19:00:00',0,7.50,0.00,104,2,NULL,209),(96,'2018-01-02 23:01:10','2018-01-02 22:00:00',0,10.50,2.00,104,1,27,210),(97,'2018-01-02 23:13:01','2018-01-02 22:00:00',0,16.50,2.00,104,1,27,211),(98,'2018-01-03 11:42:25','2018-01-03 11:00:00',0,1.00,0.00,108,3,80,212),(103,'2018-01-03 12:37:32','2018-01-03 12:00:00',0,28.50,2.00,104,1,27,218),(108,'2018-01-03 12:39:10','2018-01-03 12:00:00',0,6.00,2.00,104,1,27,223),(109,'2018-01-03 12:39:10','2018-01-03 12:00:00',0,2.00,2.00,104,1,27,224),(110,'2018-01-03 12:39:25','2018-01-03 12:00:00',0,6.00,2.00,104,1,27,225),(111,'2018-01-03 12:39:25','2018-01-03 12:00:00',0,2.00,2.00,104,1,27,226),(112,'2018-01-03 12:39:47','2018-01-03 13:00:00',0,6.00,2.00,108,12,NULL,227),(114,'2018-01-03 15:12:10','2018-01-03 14:00:00',0,10.00,2.00,104,1,27,229),(115,'2018-01-03 15:12:16','2018-01-03 14:00:00',0,9.50,2.00,104,1,27,230),(116,'2018-01-03 15:24:57','2018-01-03 14:00:00',0,9.50,2.00,104,1,27,231),(117,'2018-01-03 15:25:40','2018-01-03 14:00:00',0,9.50,2.00,104,1,27,232),(118,'2018-01-03 15:25:55','2018-01-03 14:00:00',0,4.00,0.00,113,2,56,233),(119,'2018-01-03 15:25:57','2018-01-03 14:00:00',0,3.00,2.00,108,12,80,234),(120,'2018-01-03 15:26:01','2018-01-03 14:00:00',0,9.50,2.00,104,1,27,235),(121,'2018-01-03 15:26:45','2018-01-03 14:00:00',0,5.50,2.00,104,1,27,236),(122,'2018-01-03 15:26:50','2018-01-03 14:00:00',0,3.60,0.00,113,2,56,237),(123,'2018-01-03 15:27:09','2018-01-03 14:00:00',0,5.60,2.00,104,1,27,238),(124,'2018-01-03 15:37:22','2018-01-03 15:00:00',0,6.00,2.00,104,1,27,239),(125,'2018-01-03 15:54:41','2018-01-03 15:00:00',0,9.50,2.00,104,1,27,241),(126,'2018-01-03 16:02:05','2018-01-03 15:00:00',0,10.00,2.00,104,1,27,242),(127,'2018-01-03 16:22:46','2018-01-03 15:00:00',0,5.50,0.00,113,2,56,243),(128,'2018-01-03 16:34:44','2018-01-03 16:00:00',0,21.60,2.00,104,1,27,244),(129,'2018-01-03 17:30:49','2018-01-03 17:00:00',0,13.50,0.00,108,2,NULL,245),(131,'2018-01-03 23:26:33','2018-01-03 22:00:00',0,6.00,2.00,104,1,27,247),(132,'2018-01-04 15:22:25','2018-01-04 16:18:00',0,9.50,2.00,104,1,27,248),(133,'2018-01-04 16:12:44','2018-01-04 17:00:00',0,6.00,2.00,104,1,27,249),(134,'2018-01-04 16:14:08','2018-01-04 17:00:00',0,5.50,2.00,104,1,27,250),(135,'2018-01-04 17:06:16','2018-01-04 18:00:00',0,324.50,2.00,104,1,27,251),(136,'2018-01-04 17:25:17','2018-01-04 18:00:00',0,607.50,0.00,113,3,56,252),(137,'2018-01-04 17:44:05','2018-01-04 18:00:00',0,7290.00,0.00,113,2,56,253),(138,'2018-01-04 17:45:23','2018-01-04 18:00:00',0,335340.00,0.00,113,3,56,254),(139,'2018-01-04 18:01:02','2018-01-04 19:00:00',0,4.00,0.00,104,2,NULL,255),(140,'2018-01-04 18:26:29','2018-01-04 19:00:00',0,33534000.00,0.00,113,2,56,256),(142,'2018-01-04 19:27:29','2018-01-04 20:00:00',0,33534000.00,0.00,113,2,NULL,259),(143,'2018-01-04 19:35:43','2018-01-04 20:00:00',0,99999999.99,0.00,113,2,56,260),(148,'2018-01-05 16:34:51','2018-01-05 18:00:00',0,9.50,2.00,104,1,27,266),(149,'2018-01-05 16:47:40','2018-01-05 18:00:00',0,30.00,2.00,104,1,27,267),(151,'2018-01-05 18:04:15','2018-01-05 18:10:00',0,20.00,2.00,108,12,91,269),(152,'2018-01-05 18:30:04','2018-01-05 19:00:00',0,25.50,0.00,104,2,27,270),(153,'2018-01-05 18:30:41','2018-01-05 19:00:00',0,7.50,0.00,104,2,27,271),(154,'2018-01-05 18:32:11','2018-01-05 19:00:00',0,7.50,0.00,104,2,27,272),(155,'2018-01-06 19:51:34','2018-01-06 20:00:00',0,7.50,0.00,104,2,27,273),(156,'2018-01-07 17:08:40','2018-01-07 18:00:00',0,5.00,0.00,104,2,27,274),(157,'2018-01-07 17:30:12','2018-01-07 18:00:00',0,8.00,2.00,118,24,NULL,276),(158,'2018-01-07 17:30:48','2018-01-07 18:00:00',0,4.00,0.00,118,2,NULL,277);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_state`
--

DROP TABLE IF EXISTS `order_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`state`),
  KEY `order_fk_of_order_state_idx` (`order`),
  CONSTRAINT `order_fk_of_order_state` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1568 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_state`
--

LOCK TABLES `order_state` WRITE;
/*!40000 ALTER TABLE `order_state` DISABLE KEYS */;
INSERT INTO `order_state` VALUES (1,0,'2017-12-28 00:00:00',48),(2,1,'2017-12-28 17:36:06',48),(3,0,'2017-12-28 17:42:20',50),(4,4,'2017-12-28 18:58:10',50),(5,0,'2017-12-28 19:15:06',57),(6,0,'2017-12-28 19:16:08',58),(7,3,'2017-12-28 19:17:54',48),(8,0,'2017-12-28 19:18:51',59),(9,0,'2017-12-29 13:03:01',60),(10,0,'2017-12-29 16:27:17',61),(11,0,'2017-12-30 11:44:09',62),(12,0,'2017-12-30 11:44:55',63),(13,4,'2017-12-30 13:41:15',60),(14,4,'2017-12-30 14:09:37',58),(15,4,'2017-12-30 14:18:34',57),(16,0,'2017-12-30 14:58:06',64),(17,4,'2017-12-30 14:58:20',64),(18,0,'2017-12-30 14:58:53',65),(19,4,'2017-12-30 14:58:57',65),(20,0,'2017-12-30 15:02:05',66),(21,4,'2017-12-30 15:04:07',66),(22,4,'2017-12-30 15:05:32',63),(23,0,'2017-12-30 15:07:04',67),(24,4,'2017-12-30 15:07:07',67),(25,0,'2017-12-30 15:08:10',68),(26,4,'2017-12-30 15:08:16',68),(27,0,'2017-12-30 15:09:58',69),(28,4,'2017-12-30 15:10:21',69),(29,0,'2017-12-30 15:12:09',70),(30,4,'2017-12-30 15:12:11',70),(31,0,'2017-12-30 15:12:28',71),(32,4,'2017-12-30 15:12:34',71),(33,0,'2017-12-30 15:13:45',72),(34,4,'2017-12-30 15:13:48',72),(35,0,'2017-12-30 15:15:21',73),(36,4,'2017-12-30 15:15:23',73),(37,0,'2017-12-30 15:15:44',74),(38,4,'2017-12-30 15:15:53',74),(39,0,'2017-12-30 15:20:04',75),(40,4,'2017-12-30 15:20:09',75),(41,0,'2017-12-30 15:20:13',76),(42,4,'2017-12-30 15:20:22',76),(43,0,'2017-12-30 15:20:59',77),(44,4,'2017-12-30 15:21:01',77),(45,0,'2017-12-30 15:22:35',78),(46,0,'2017-12-30 15:22:39',79),(47,4,'2017-12-30 15:22:40',78),(48,4,'2017-12-30 15:22:41',79),(49,0,'2017-12-30 15:24:23',80),(50,4,'2017-12-30 15:24:29',80),(51,0,'2017-12-30 15:25:39',81),(52,4,'2017-12-30 15:25:47',81),(53,0,'2017-12-30 15:27:20',82),(54,4,'2017-12-30 15:27:26',82),(55,0,'2017-12-30 15:27:48',83),(56,4,'2017-12-30 15:27:53',83),(57,0,'2017-12-30 15:51:59',84),(58,4,'2017-12-30 15:52:02',84),(59,0,'2017-12-30 22:16:15',85),(60,4,'2017-12-30 22:16:17',85),(62,0,'2017-12-31 13:09:58',87),(65,0,'2017-12-31 13:17:13',89),(66,4,'2017-12-31 13:17:32',89),(67,0,'2017-12-31 14:18:20',90),(68,4,'2017-12-31 14:18:26',90),(70,0,'2018-01-01 10:41:48',92),(71,4,'2018-01-01 10:41:55',92),(72,0,'2018-01-02 12:26:30',93),(73,0,'2018-01-02 18:18:30',94),(74,0,'2018-01-02 19:37:41',95),(75,0,'2018-01-02 23:01:10',96),(76,4,'2018-01-02 23:10:23',96),(77,0,'2018-01-02 23:13:01',97),(78,0,'2018-01-03 11:42:25',98),(83,0,'2018-01-03 12:37:32',103),(88,0,'2018-01-03 12:39:10',108),(89,0,'2018-01-03 12:39:10',109),(90,0,'2018-01-03 12:39:25',110),(91,0,'2018-01-03 12:39:25',111),(92,0,'2018-01-03 12:39:47',112),(95,0,'2018-01-03 15:12:10',114),(96,0,'2018-01-03 15:12:16',115),(97,0,'2018-01-03 15:24:57',116),(98,0,'2018-01-03 15:25:40',117),(99,0,'2018-01-03 15:25:55',118),(100,0,'2018-01-03 15:25:57',119),(101,0,'2018-01-03 15:26:01',120),(102,0,'2018-01-03 15:26:45',121),(103,0,'2018-01-03 15:26:50',122),(104,0,'2018-01-03 15:27:09',123),(105,4,'2018-01-03 15:27:14',122),(106,0,'2018-01-03 15:37:22',124),(107,4,'2018-01-03 15:40:25',124),(108,4,'2018-01-03 15:40:49',123),(109,4,'2018-01-03 15:40:52',121),(110,4,'2018-01-03 15:48:24',120),(111,0,'2018-01-03 15:54:41',125),(112,0,'2018-01-03 16:02:05',126),(113,0,'2018-01-03 16:22:47',127),(114,0,'2018-01-03 16:34:44',128),(115,0,'2018-01-03 17:30:49',129),(117,0,'2018-01-03 23:26:33',131),(118,0,'2018-01-04 15:22:25',132),(119,4,'2018-01-04 15:25:04',132),(120,0,'2018-01-04 16:12:44',133),(121,0,'2018-01-04 16:14:08',134),(122,4,'2018-01-04 16:15:08',134),(123,4,'2018-01-04 16:17:02',131),(124,4,'2018-01-04 16:17:04',128),(125,4,'2018-01-04 16:17:05',126),(126,4,'2018-01-04 16:17:07',125),(127,4,'2018-01-04 16:22:11',129),(128,4,'2018-01-04 16:25:11',117),(129,0,'2018-01-04 17:06:16',135),(130,0,'2018-01-04 17:25:18',136),(132,1,'2018-01-04 17:34:08',133),(133,2,'2018-01-04 17:34:11',133),(134,3,'2018-01-04 17:34:16',133),(135,1,'2018-01-04 17:41:14',59),(136,0,'2018-01-04 17:44:05',137),(137,0,'2018-01-04 17:45:23',138),(138,4,'2018-01-04 17:47:28',138),(143,1,'2018-01-04 17:50:09',137),(144,3,'2018-01-04 17:50:14',137),(145,1,'2018-01-04 17:51:05',136),(146,2,'2018-01-04 17:51:14',136),(147,3,'2018-01-04 17:52:07',136),(148,1,'2018-01-04 17:53:40',135),(149,2,'2018-01-04 17:54:12',135),(150,1,'2018-01-04 17:56:32',127),(151,2,'2018-01-04 17:56:37',127),(152,3,'2018-01-04 17:59:26',127),(153,1,'2018-01-04 18:00:22',118),(154,2,'2018-01-04 18:00:32',118),(155,0,'2018-01-04 18:01:02',139),(156,1,'2018-01-04 18:01:43',115),(157,2,'2018-01-04 18:01:47',115),(158,3,'2018-01-04 18:01:54',115),(159,1,'2018-01-04 18:02:54',111),(160,2,'2018-01-04 18:02:59',111),(161,3,'2018-01-04 18:03:05',111),(162,1,'2018-01-04 18:05:08',59),(163,2,'2018-01-04 18:05:12',59),(164,0,'2018-01-04 18:26:29',140),(165,1,'2018-01-04 18:30:52',114),(166,2,'2018-01-04 18:30:57',114),(167,3,'2018-01-04 18:31:36',59),(168,4,'2018-01-04 18:32:16',140),(169,4,'2018-01-04 18:33:33',140),(170,4,'2018-01-04 18:33:39',140),(171,1,'2018-01-04 18:33:58',139),(172,2,'2018-01-04 18:34:08',139),(173,3,'2018-01-04 18:34:12',139),(174,4,'2018-01-04 18:34:18',108),(202,1,'2018-01-04 19:00:03',93),(203,2,'2018-01-04 19:00:05',93),(204,3,'2018-01-04 19:00:06',93),(205,1,'2018-01-04 19:00:16',97),(206,2,'2018-01-04 19:00:17',97),(207,4,'2018-01-04 19:00:19',97),(208,4,'2018-01-04 19:00:19',97),(209,4,'2018-01-04 19:00:19',97),(211,4,'2018-01-04 19:04:55',135),(212,4,'2018-01-04 19:05:25',119),(213,1,'2018-01-04 19:05:41',116),(214,4,'2018-01-04 19:05:42',116),(215,4,'2018-01-04 19:05:42',116),(216,1,'2018-01-04 19:05:54',112),(217,2,'2018-01-04 19:05:56',112),(218,1,'2018-01-04 19:06:09',110),(219,2,'2018-01-04 19:06:11',110),(220,1,'2018-01-04 19:06:29',109),(221,2,'2018-01-04 19:06:40',109),(222,3,'2018-01-04 19:07:03',114),(223,3,'2018-01-04 19:07:09',114),(224,3,'2018-01-04 19:07:09',114),(225,3,'2018-01-04 19:07:15',114),(226,3,'2018-01-04 19:07:15',114),(227,3,'2018-01-04 19:07:15',114),(228,3,'2018-01-04 19:07:15',114),(229,3,'2018-01-04 19:07:18',114),(230,3,'2018-01-04 19:07:18',114),(231,3,'2018-01-04 19:07:18',114),(232,3,'2018-01-04 19:07:18',114),(233,3,'2018-01-04 19:07:18',114),(234,3,'2018-01-04 19:07:18',114),(235,3,'2018-01-04 19:07:19',114),(236,3,'2018-01-04 19:07:19',114),(237,3,'2018-01-04 19:07:20',114),(238,3,'2018-01-04 19:07:20',114),(239,3,'2018-01-04 19:07:20',114),(240,3,'2018-01-04 19:07:21',114),(241,3,'2018-01-04 19:07:21',114),(242,3,'2018-01-04 19:07:21',114),(243,3,'2018-01-04 19:07:21',114),(244,3,'2018-01-04 19:07:21',114),(245,3,'2018-01-04 19:07:21',114),(246,3,'2018-01-04 19:07:22',114),(247,3,'2018-01-04 19:07:22',114),(248,3,'2018-01-04 19:07:22',114),(249,3,'2018-01-04 19:07:22',114),(250,3,'2018-01-04 19:07:22',114),(251,3,'2018-01-04 19:07:22',114),(252,3,'2018-01-04 19:07:23',114),(253,3,'2018-01-04 19:07:31',114),(254,3,'2018-01-04 19:07:32',114),(255,3,'2018-01-04 19:07:32',114),(256,3,'2018-01-04 19:07:32',114),(257,3,'2018-01-04 19:07:32',114),(258,3,'2018-01-04 19:07:32',114),(259,3,'2018-01-04 19:07:32',114),(260,3,'2018-01-04 19:07:32',114),(261,3,'2018-01-04 19:07:33',114),(262,3,'2018-01-04 19:07:33',114),(263,3,'2018-01-04 19:07:33',114),(264,3,'2018-01-04 19:07:33',114),(265,3,'2018-01-04 19:07:33',114),(266,3,'2018-01-04 19:07:33',114),(267,3,'2018-01-04 19:07:33',114),(268,3,'2018-01-04 19:07:34',114),(269,3,'2018-01-04 19:07:34',114),(270,3,'2018-01-04 19:07:34',114),(271,3,'2018-01-04 19:07:34',114),(272,3,'2018-01-04 19:07:34',114),(273,3,'2018-01-04 19:07:34',114),(274,3,'2018-01-04 19:07:35',114),(275,3,'2018-01-04 19:07:35',114),(276,3,'2018-01-04 19:07:35',114),(277,3,'2018-01-04 19:07:35',114),(278,3,'2018-01-04 19:07:35',114),(279,3,'2018-01-04 19:07:35',114),(280,3,'2018-01-04 19:07:35',114),(281,3,'2018-01-04 19:07:36',114),(282,3,'2018-01-04 19:07:36',114),(283,3,'2018-01-04 19:07:36',114),(284,3,'2018-01-04 19:07:36',114),(285,3,'2018-01-04 19:07:36',114),(286,3,'2018-01-04 19:07:36',114),(287,3,'2018-01-04 19:07:36',114),(288,3,'2018-01-04 19:07:37',114),(289,3,'2018-01-04 19:07:37',114),(290,3,'2018-01-04 19:07:37',114),(291,3,'2018-01-04 19:07:37',114),(292,3,'2018-01-04 19:07:37',114),(293,3,'2018-01-04 19:07:37',114),(294,3,'2018-01-04 19:07:37',114),(295,3,'2018-01-04 19:07:38',114),(296,3,'2018-01-04 19:07:38',114),(297,3,'2018-01-04 19:07:38',114),(298,3,'2018-01-04 19:07:38',114),(299,3,'2018-01-04 19:07:38',114),(300,3,'2018-01-04 19:07:39',114),(301,3,'2018-01-04 19:07:39',114),(302,3,'2018-01-04 19:07:39',114),(303,3,'2018-01-04 19:07:39',114),(304,3,'2018-01-04 19:07:39',114),(305,3,'2018-01-04 19:07:39',114),(306,3,'2018-01-04 19:07:39',114),(307,3,'2018-01-04 19:07:40',114),(308,3,'2018-01-04 19:07:40',114),(309,3,'2018-01-04 19:07:40',114),(310,3,'2018-01-04 19:07:40',114),(311,3,'2018-01-04 19:07:40',114),(312,3,'2018-01-04 19:07:40',114),(313,3,'2018-01-04 19:07:40',114),(314,3,'2018-01-04 19:07:41',114),(315,3,'2018-01-04 19:07:41',114),(316,3,'2018-01-04 19:07:41',114),(317,3,'2018-01-04 19:07:41',114),(318,3,'2018-01-04 19:07:41',114),(319,3,'2018-01-04 19:07:41',114),(320,3,'2018-01-04 19:07:41',114),(321,3,'2018-01-04 19:07:42',114),(322,3,'2018-01-04 19:07:42',114),(323,3,'2018-01-04 19:07:42',114),(324,3,'2018-01-04 19:07:42',114),(325,3,'2018-01-04 19:07:42',114),(326,3,'2018-01-04 19:07:42',114),(327,3,'2018-01-04 19:07:42',114),(328,3,'2018-01-04 19:07:43',114),(329,3,'2018-01-04 19:07:43',114),(330,3,'2018-01-04 19:07:43',114),(331,3,'2018-01-04 19:07:43',114),(332,3,'2018-01-04 19:07:43',114),(333,3,'2018-01-04 19:07:43',114),(334,3,'2018-01-04 19:07:44',114),(335,3,'2018-01-04 19:07:44',114),(336,3,'2018-01-04 19:07:44',114),(337,3,'2018-01-04 19:07:44',114),(338,3,'2018-01-04 19:07:44',114),(339,3,'2018-01-04 19:07:44',114),(340,3,'2018-01-04 19:07:44',114),(341,3,'2018-01-04 19:07:44',114),(342,3,'2018-01-04 19:07:45',114),(343,3,'2018-01-04 19:07:45',114),(344,3,'2018-01-04 19:07:45',114),(345,3,'2018-01-04 19:07:45',114),(346,3,'2018-01-04 19:07:45',114),(347,3,'2018-01-04 19:07:45',114),(348,3,'2018-01-04 19:07:45',114),(349,3,'2018-01-04 19:07:46',114),(350,3,'2018-01-04 19:07:46',114),(351,3,'2018-01-04 19:07:46',114),(352,3,'2018-01-04 19:07:46',114),(353,3,'2018-01-04 19:07:46',114),(354,3,'2018-01-04 19:07:46',114),(355,3,'2018-01-04 19:07:46',114),(356,3,'2018-01-04 19:07:47',114),(357,3,'2018-01-04 19:07:47',114),(358,3,'2018-01-04 19:07:47',114),(359,3,'2018-01-04 19:07:47',114),(360,3,'2018-01-04 19:07:47',114),(361,3,'2018-01-04 19:07:47',114),(362,3,'2018-01-04 19:07:48',114),(363,3,'2018-01-04 19:07:48',114),(364,3,'2018-01-04 19:07:48',114),(365,3,'2018-01-04 19:07:48',114),(366,3,'2018-01-04 19:07:48',114),(367,3,'2018-01-04 19:07:48',114),(368,3,'2018-01-04 19:07:48',114),(369,3,'2018-01-04 19:07:49',114),(370,3,'2018-01-04 19:07:49',114),(371,3,'2018-01-04 19:07:49',114),(372,3,'2018-01-04 19:07:49',114),(373,3,'2018-01-04 19:07:49',114),(374,3,'2018-01-04 19:07:49',114),(375,3,'2018-01-04 19:07:49',114),(376,3,'2018-01-04 19:07:50',114),(377,3,'2018-01-04 19:07:50',114),(378,3,'2018-01-04 19:07:50',114),(379,3,'2018-01-04 19:07:50',114),(380,3,'2018-01-04 19:07:50',114),(381,3,'2018-01-04 19:07:50',114),(382,3,'2018-01-04 19:07:50',114),(383,3,'2018-01-04 19:07:51',114),(384,3,'2018-01-04 19:07:51',114),(385,3,'2018-01-04 19:07:51',114),(386,3,'2018-01-04 19:07:51',114),(387,3,'2018-01-04 19:07:51',114),(388,3,'2018-01-04 19:07:51',114),(389,3,'2018-01-04 19:07:51',114),(390,3,'2018-01-04 19:07:52',114),(391,3,'2018-01-04 19:07:52',114),(392,3,'2018-01-04 19:07:52',114),(393,3,'2018-01-04 19:07:52',114),(394,3,'2018-01-04 19:07:52',114),(395,3,'2018-01-04 19:07:52',114),(396,3,'2018-01-04 19:07:52',114),(397,3,'2018-01-04 19:07:53',114),(398,3,'2018-01-04 19:07:53',114),(399,3,'2018-01-04 19:07:53',114),(400,3,'2018-01-04 19:07:53',114),(401,3,'2018-01-04 19:07:53',114),(402,3,'2018-01-04 19:07:53',114),(403,3,'2018-01-04 19:07:54',114),(404,3,'2018-01-04 19:07:54',114),(405,3,'2018-01-04 19:07:54',114),(406,3,'2018-01-04 19:07:54',114),(407,3,'2018-01-04 19:07:54',114),(408,3,'2018-01-04 19:07:54',114),(409,3,'2018-01-04 19:07:55',114),(410,3,'2018-01-04 19:07:55',114),(411,3,'2018-01-04 19:07:55',114),(412,3,'2018-01-04 19:07:55',114),(413,3,'2018-01-04 19:07:55',114),(414,3,'2018-01-04 19:07:55',114),(415,3,'2018-01-04 19:07:55',114),(416,3,'2018-01-04 19:07:56',114),(417,3,'2018-01-04 19:07:56',114),(418,3,'2018-01-04 19:07:56',114),(419,3,'2018-01-04 19:07:56',114),(420,3,'2018-01-04 19:07:56',114),(421,3,'2018-01-04 19:07:56',114),(422,3,'2018-01-04 19:07:57',114),(423,3,'2018-01-04 19:07:57',114),(424,3,'2018-01-04 19:07:57',114),(425,3,'2018-01-04 19:07:57',114),(426,3,'2018-01-04 19:07:57',114),(427,3,'2018-01-04 19:07:57',114),(428,3,'2018-01-04 19:07:58',114),(429,3,'2018-01-04 19:07:58',114),(430,3,'2018-01-04 19:07:58',114),(431,3,'2018-01-04 19:07:58',114),(432,3,'2018-01-04 19:07:58',114),(433,3,'2018-01-04 19:07:58',114),(434,3,'2018-01-04 19:07:58',114),(435,3,'2018-01-04 19:07:59',114),(436,3,'2018-01-04 19:07:59',114),(437,3,'2018-01-04 19:07:59',114),(438,3,'2018-01-04 19:07:59',114),(439,3,'2018-01-04 19:07:59',114),(440,3,'2018-01-04 19:07:59',114),(441,3,'2018-01-04 19:07:59',114),(442,3,'2018-01-04 19:08:00',114),(443,3,'2018-01-04 19:08:00',114),(444,3,'2018-01-04 19:08:00',114),(445,3,'2018-01-04 19:08:00',114),(446,3,'2018-01-04 19:08:00',114),(447,3,'2018-01-04 19:08:00',114),(448,3,'2018-01-04 19:08:01',114),(449,3,'2018-01-04 19:08:01',114),(450,3,'2018-01-04 19:08:01',114),(451,3,'2018-01-04 19:08:01',114),(452,3,'2018-01-04 19:08:01',114),(453,3,'2018-01-04 19:08:01',114),(454,3,'2018-01-04 19:08:01',114),(455,3,'2018-01-04 19:08:02',114),(456,3,'2018-01-04 19:08:02',114),(457,3,'2018-01-04 19:08:02',114),(458,3,'2018-01-04 19:08:02',114),(459,3,'2018-01-04 19:08:02',114),(460,3,'2018-01-04 19:08:02',114),(461,3,'2018-01-04 19:08:02',114),(462,3,'2018-01-04 19:08:03',114),(463,3,'2018-01-04 19:08:03',114),(464,3,'2018-01-04 19:08:03',114),(465,3,'2018-01-04 19:08:03',114),(466,3,'2018-01-04 19:08:03',114),(467,3,'2018-01-04 19:08:03',114),(468,3,'2018-01-04 19:08:04',114),(469,3,'2018-01-04 19:08:04',114),(470,3,'2018-01-04 19:08:04',114),(471,3,'2018-01-04 19:08:04',114),(472,3,'2018-01-04 19:08:04',114),(473,3,'2018-01-04 19:08:04',114),(474,3,'2018-01-04 19:08:04',114),(475,3,'2018-01-04 19:08:05',114),(476,3,'2018-01-04 19:08:05',114),(477,3,'2018-01-04 19:08:05',114),(478,3,'2018-01-04 19:08:05',114),(479,3,'2018-01-04 19:08:05',114),(480,3,'2018-01-04 19:08:05',114),(481,3,'2018-01-04 19:08:05',114),(482,3,'2018-01-04 19:08:06',114),(483,3,'2018-01-04 19:08:06',114),(484,3,'2018-01-04 19:08:06',114),(485,3,'2018-01-04 19:08:06',114),(486,3,'2018-01-04 19:08:06',114),(487,3,'2018-01-04 19:08:06',114),(488,3,'2018-01-04 19:08:06',114),(489,3,'2018-01-04 19:08:07',114),(490,3,'2018-01-04 19:08:07',114),(491,3,'2018-01-04 19:08:07',114),(492,3,'2018-01-04 19:08:07',114),(493,3,'2018-01-04 19:08:07',114),(494,3,'2018-01-04 19:08:07',114),(495,3,'2018-01-04 19:08:08',114),(496,3,'2018-01-04 19:08:08',114),(497,3,'2018-01-04 19:08:08',114),(498,3,'2018-01-04 19:08:08',114),(499,3,'2018-01-04 19:08:08',114),(500,3,'2018-01-04 19:08:08',114),(501,3,'2018-01-04 19:08:09',114),(502,3,'2018-01-04 19:08:09',114),(503,3,'2018-01-04 19:08:09',114),(504,3,'2018-01-04 19:08:09',114),(505,3,'2018-01-04 19:08:09',114),(506,3,'2018-01-04 19:08:09',114),(507,3,'2018-01-04 19:08:09',114),(508,3,'2018-01-04 19:08:10',114),(509,3,'2018-01-04 19:08:10',114),(510,3,'2018-01-04 19:08:10',114),(511,3,'2018-01-04 19:08:10',114),(512,3,'2018-01-04 19:08:10',114),(513,3,'2018-01-04 19:08:10',114),(514,3,'2018-01-04 19:08:10',114),(515,3,'2018-01-04 19:08:11',114),(516,3,'2018-01-04 19:08:11',114),(517,3,'2018-01-04 19:08:11',114),(518,3,'2018-01-04 19:08:24',114),(519,3,'2018-01-04 19:08:24',114),(520,3,'2018-01-04 19:08:25',114),(521,3,'2018-01-04 19:08:25',114),(522,3,'2018-01-04 19:08:25',114),(523,3,'2018-01-04 19:08:25',114),(524,3,'2018-01-04 19:08:25',114),(525,3,'2018-01-04 19:08:25',114),(526,3,'2018-01-04 19:08:26',114),(527,3,'2018-01-04 19:08:26',114),(528,3,'2018-01-04 19:08:26',114),(529,3,'2018-01-04 19:08:26',114),(530,3,'2018-01-04 19:08:26',114),(531,3,'2018-01-04 19:08:26',114),(532,3,'2018-01-04 19:08:26',114),(533,3,'2018-01-04 19:08:27',114),(534,3,'2018-01-04 19:08:27',114),(535,3,'2018-01-04 19:08:27',114),(536,3,'2018-01-04 19:08:27',114),(537,3,'2018-01-04 19:08:27',114),(538,3,'2018-01-04 19:08:27',114),(539,3,'2018-01-04 19:08:28',114),(540,3,'2018-01-04 19:08:28',114),(541,3,'2018-01-04 19:08:28',114),(542,3,'2018-01-04 19:08:28',114),(543,3,'2018-01-04 19:08:28',114),(544,3,'2018-01-04 19:08:28',114),(545,3,'2018-01-04 19:08:29',114),(546,3,'2018-01-04 19:08:29',114),(547,3,'2018-01-04 19:08:29',114),(548,3,'2018-01-04 19:08:29',114),(549,3,'2018-01-04 19:08:29',114),(550,3,'2018-01-04 19:08:29',114),(551,3,'2018-01-04 19:08:29',114),(552,3,'2018-01-04 19:08:30',114),(553,3,'2018-01-04 19:08:30',114),(554,3,'2018-01-04 19:08:30',114),(555,3,'2018-01-04 19:08:30',114),(556,3,'2018-01-04 19:08:30',114),(557,3,'2018-01-04 19:08:30',114),(558,3,'2018-01-04 19:08:31',114),(559,3,'2018-01-04 19:08:31',114),(560,3,'2018-01-04 19:08:31',114),(561,3,'2018-01-04 19:08:31',114),(562,3,'2018-01-04 19:08:31',114),(563,3,'2018-01-04 19:08:32',114),(564,3,'2018-01-04 19:08:32',114),(565,3,'2018-01-04 19:08:32',114),(566,3,'2018-01-04 19:08:32',114),(567,3,'2018-01-04 19:08:32',114),(568,3,'2018-01-04 19:08:32',114),(569,3,'2018-01-04 19:08:32',114),(570,3,'2018-01-04 19:08:33',114),(571,3,'2018-01-04 19:08:33',114),(572,3,'2018-01-04 19:08:33',114),(573,3,'2018-01-04 19:08:33',114),(574,3,'2018-01-04 19:08:33',114),(575,3,'2018-01-04 19:08:33',114),(576,3,'2018-01-04 19:08:33',114),(577,3,'2018-01-04 19:08:34',114),(578,3,'2018-01-04 19:08:34',114),(579,3,'2018-01-04 19:08:34',114),(580,3,'2018-01-04 19:08:34',114),(581,3,'2018-01-04 19:08:34',114),(582,3,'2018-01-04 19:08:35',114),(583,3,'2018-01-04 19:08:35',114),(584,3,'2018-01-04 19:08:35',114),(585,3,'2018-01-04 19:08:35',114),(586,3,'2018-01-04 19:08:35',114),(587,3,'2018-01-04 19:08:35',114),(588,3,'2018-01-04 19:08:36',114),(589,3,'2018-01-04 19:08:36',114),(590,3,'2018-01-04 19:08:36',114),(591,3,'2018-01-04 19:08:36',114),(592,3,'2018-01-04 19:08:36',114),(593,3,'2018-01-04 19:08:36',114),(594,3,'2018-01-04 19:08:36',114),(595,3,'2018-01-04 19:08:36',114),(596,3,'2018-01-04 19:08:37',114),(597,3,'2018-01-04 19:08:37',114),(598,3,'2018-01-04 19:08:37',114),(599,3,'2018-01-04 19:08:37',114),(600,3,'2018-01-04 19:08:37',114),(601,3,'2018-01-04 19:08:38',114),(602,3,'2018-01-04 19:08:39',114),(603,3,'2018-01-04 19:08:39',114),(604,3,'2018-01-04 19:08:39',114),(605,3,'2018-01-04 19:08:39',114),(606,3,'2018-01-04 19:08:39',114),(607,3,'2018-01-04 19:08:40',114),(608,3,'2018-01-04 19:08:40',114),(609,3,'2018-01-04 19:08:40',114),(610,3,'2018-01-04 19:08:40',114),(611,3,'2018-01-04 19:08:40',114),(612,3,'2018-01-04 19:08:41',114),(613,3,'2018-01-04 19:08:41',114),(614,3,'2018-01-04 19:08:41',114),(615,3,'2018-01-04 19:08:41',114),(616,3,'2018-01-04 19:08:41',114),(617,3,'2018-01-04 19:08:41',114),(618,3,'2018-01-04 19:08:42',114),(619,3,'2018-01-04 19:08:42',114),(620,3,'2018-01-04 19:08:42',114),(621,3,'2018-01-04 19:08:42',114),(622,3,'2018-01-04 19:08:42',114),(623,3,'2018-01-04 19:08:43',114),(624,3,'2018-01-04 19:08:43',114),(625,3,'2018-01-04 19:08:43',114),(626,3,'2018-01-04 19:08:43',114),(627,3,'2018-01-04 19:08:43',114),(628,3,'2018-01-04 19:08:43',114),(629,3,'2018-01-04 19:08:44',114),(630,3,'2018-01-04 19:08:44',114),(631,3,'2018-01-04 19:08:44',114),(632,3,'2018-01-04 19:08:44',114),(633,3,'2018-01-04 19:08:44',114),(634,3,'2018-01-04 19:08:44',114),(635,3,'2018-01-04 19:08:45',114),(636,3,'2018-01-04 19:08:45',114),(637,3,'2018-01-04 19:08:45',114),(638,3,'2018-01-04 19:08:45',114),(639,3,'2018-01-04 19:08:45',114),(640,3,'2018-01-04 19:08:45',114),(641,3,'2018-01-04 19:08:45',114),(642,3,'2018-01-04 19:08:46',114),(643,3,'2018-01-04 19:08:46',114),(644,3,'2018-01-04 19:08:46',114),(645,3,'2018-01-04 19:08:46',114),(646,3,'2018-01-04 19:08:46',114),(647,3,'2018-01-04 19:08:46',114),(648,3,'2018-01-04 19:08:46',114),(649,3,'2018-01-04 19:08:47',114),(650,3,'2018-01-04 19:08:47',114),(651,3,'2018-01-04 19:08:47',114),(652,3,'2018-01-04 19:08:47',114),(653,3,'2018-01-04 19:08:47',114),(654,3,'2018-01-04 19:08:47',114),(655,3,'2018-01-04 19:08:47',114),(656,3,'2018-01-04 19:08:47',114),(657,3,'2018-01-04 19:08:48',114),(658,3,'2018-01-04 19:08:48',114),(659,3,'2018-01-04 19:08:48',114),(660,3,'2018-01-04 19:08:48',114),(661,3,'2018-01-04 19:08:48',114),(662,3,'2018-01-04 19:08:48',114),(663,3,'2018-01-04 19:08:48',114),(664,3,'2018-01-04 19:08:48',114),(665,3,'2018-01-04 19:08:49',114),(666,3,'2018-01-04 19:08:49',114),(667,3,'2018-01-04 19:08:49',114),(668,3,'2018-01-04 19:08:49',114),(669,3,'2018-01-04 19:08:49',114),(670,3,'2018-01-04 19:08:49',114),(671,3,'2018-01-04 19:08:49',114),(672,3,'2018-01-04 19:08:49',114),(673,3,'2018-01-04 19:08:50',114),(674,3,'2018-01-04 19:08:50',114),(675,3,'2018-01-04 19:08:50',114),(676,3,'2018-01-04 19:08:50',114),(677,3,'2018-01-04 19:08:50',114),(678,3,'2018-01-04 19:08:50',114),(679,3,'2018-01-04 19:08:50',114),(680,3,'2018-01-04 19:08:50',114),(681,3,'2018-01-04 19:08:51',114),(682,3,'2018-01-04 19:08:51',114),(683,3,'2018-01-04 19:08:51',114),(684,3,'2018-01-04 19:08:51',114),(685,3,'2018-01-04 19:08:51',114),(686,3,'2018-01-04 19:08:51',114),(687,3,'2018-01-04 19:08:51',114),(688,3,'2018-01-04 19:08:51',114),(689,3,'2018-01-04 19:08:51',114),(690,3,'2018-01-04 19:08:52',114),(691,3,'2018-01-04 19:08:52',114),(692,3,'2018-01-04 19:08:52',114),(693,3,'2018-01-04 19:08:52',114),(694,3,'2018-01-04 19:08:52',114),(695,3,'2018-01-04 19:08:53',114),(696,3,'2018-01-04 19:08:53',114),(697,3,'2018-01-04 19:08:53',114),(698,3,'2018-01-04 19:08:53',114),(699,3,'2018-01-04 19:08:53',114),(700,3,'2018-01-04 19:08:53',114),(701,3,'2018-01-04 19:08:54',114),(702,3,'2018-01-04 19:08:54',114),(703,3,'2018-01-04 19:08:54',114),(704,3,'2018-01-04 19:08:54',114),(705,3,'2018-01-04 19:08:54',114),(706,3,'2018-01-04 19:08:54',114),(707,3,'2018-01-04 19:08:55',114),(708,3,'2018-01-04 19:08:55',114),(709,3,'2018-01-04 19:08:55',114),(710,3,'2018-01-04 19:08:55',114),(711,3,'2018-01-04 19:08:55',114),(712,3,'2018-01-04 19:08:55',114),(713,3,'2018-01-04 19:08:56',114),(714,3,'2018-01-04 19:08:56',114),(715,3,'2018-01-04 19:08:56',114),(716,3,'2018-01-04 19:08:56',114),(717,3,'2018-01-04 19:08:56',114),(718,3,'2018-01-04 19:08:56',114),(719,3,'2018-01-04 19:08:56',114),(720,3,'2018-01-04 19:08:57',114),(721,3,'2018-01-04 19:08:57',114),(722,3,'2018-01-04 19:08:57',114),(723,3,'2018-01-04 19:08:57',114),(724,3,'2018-01-04 19:08:57',114),(725,3,'2018-01-04 19:08:57',114),(726,3,'2018-01-04 19:08:58',114),(727,3,'2018-01-04 19:08:58',114),(728,3,'2018-01-04 19:08:58',114),(729,3,'2018-01-04 19:08:58',114),(730,3,'2018-01-04 19:08:58',114),(731,3,'2018-01-04 19:08:58',114),(732,3,'2018-01-04 19:08:58',114),(733,3,'2018-01-04 19:08:59',114),(734,3,'2018-01-04 19:08:59',114),(735,3,'2018-01-04 19:08:59',114),(736,3,'2018-01-04 19:08:59',114),(737,3,'2018-01-04 19:08:59',114),(738,3,'2018-01-04 19:08:59',114),(739,3,'2018-01-04 19:08:59',114),(740,3,'2018-01-04 19:09:00',114),(741,3,'2018-01-04 19:09:00',114),(742,3,'2018-01-04 19:09:00',114),(743,3,'2018-01-04 19:09:00',114),(744,3,'2018-01-04 19:09:00',114),(745,3,'2018-01-04 19:09:00',114),(746,3,'2018-01-04 19:09:01',114),(747,3,'2018-01-04 19:09:01',114),(748,3,'2018-01-04 19:09:01',114),(749,3,'2018-01-04 19:09:01',114),(750,3,'2018-01-04 19:09:01',114),(751,3,'2018-01-04 19:09:01',114),(752,3,'2018-01-04 19:09:01',114),(753,3,'2018-01-04 19:09:02',114),(754,3,'2018-01-04 19:09:02',114),(755,3,'2018-01-04 19:09:02',114),(756,3,'2018-01-04 19:09:02',114),(757,3,'2018-01-04 19:09:02',114),(758,3,'2018-01-04 19:09:02',114),(759,3,'2018-01-04 19:09:02',114),(760,3,'2018-01-04 19:09:03',114),(761,3,'2018-01-04 19:09:03',114),(762,3,'2018-01-04 19:09:03',114),(763,3,'2018-01-04 19:09:03',114),(764,3,'2018-01-04 19:09:03',114),(765,3,'2018-01-04 19:09:03',114),(766,3,'2018-01-04 19:09:04',114),(767,3,'2018-01-04 19:09:04',114),(768,3,'2018-01-04 19:09:04',114),(769,3,'2018-01-04 19:09:04',114),(770,3,'2018-01-04 19:09:04',114),(771,3,'2018-01-04 19:09:04',114),(772,3,'2018-01-04 19:09:04',114),(773,3,'2018-01-04 19:09:05',114),(774,3,'2018-01-04 19:09:05',114),(775,3,'2018-01-04 19:09:05',114),(776,3,'2018-01-04 19:09:05',114),(777,3,'2018-01-04 19:09:05',114),(778,3,'2018-01-04 19:09:05',114),(779,3,'2018-01-04 19:09:05',114),(780,3,'2018-01-04 19:09:06',114),(781,3,'2018-01-04 19:09:06',114),(782,3,'2018-01-04 19:09:06',114),(783,3,'2018-01-04 19:09:06',114),(784,3,'2018-01-04 19:09:06',114),(785,3,'2018-01-04 19:09:06',114),(786,3,'2018-01-04 19:09:07',114),(787,3,'2018-01-04 19:09:07',114),(788,3,'2018-01-04 19:09:07',114),(789,3,'2018-01-04 19:09:07',114),(790,3,'2018-01-04 19:09:07',114),(791,3,'2018-01-04 19:09:07',114),(792,3,'2018-01-04 19:09:08',114),(793,3,'2018-01-04 19:09:08',114),(794,3,'2018-01-04 19:09:08',114),(795,3,'2018-01-04 19:09:08',114),(796,3,'2018-01-04 19:09:08',114),(797,3,'2018-01-04 19:09:08',114),(798,3,'2018-01-04 19:09:08',114),(799,3,'2018-01-04 19:09:09',114),(800,3,'2018-01-04 19:09:09',114),(801,3,'2018-01-04 19:09:09',114),(802,3,'2018-01-04 19:09:09',114),(803,3,'2018-01-04 19:09:09',114),(804,3,'2018-01-04 19:09:09',114),(805,3,'2018-01-04 19:09:09',114),(806,3,'2018-01-04 19:09:10',114),(807,3,'2018-01-04 19:09:10',114),(808,3,'2018-01-04 19:09:10',114),(809,3,'2018-01-04 19:09:10',114),(810,3,'2018-01-04 19:09:10',114),(811,3,'2018-01-04 19:09:10',114),(812,3,'2018-01-04 19:09:10',114),(813,3,'2018-01-04 19:09:11',114),(814,3,'2018-01-04 19:09:11',114),(815,4,'2018-01-04 19:09:23',114),(816,4,'2018-01-04 19:09:23',114),(817,4,'2018-01-04 19:09:23',114),(818,4,'2018-01-04 19:09:23',114),(819,4,'2018-01-04 19:09:24',114),(820,4,'2018-01-04 19:09:24',114),(821,4,'2018-01-04 19:09:24',114),(822,4,'2018-01-04 19:09:24',114),(823,4,'2018-01-04 19:09:24',114),(824,4,'2018-01-04 19:09:24',114),(825,4,'2018-01-04 19:09:25',114),(826,4,'2018-01-04 19:09:25',114),(827,4,'2018-01-04 19:09:25',114),(828,4,'2018-01-04 19:09:25',114),(829,4,'2018-01-04 19:09:25',114),(830,4,'2018-01-04 19:09:25',114),(831,4,'2018-01-04 19:09:25',114),(832,4,'2018-01-04 19:09:26',114),(833,4,'2018-01-04 19:09:26',114),(834,4,'2018-01-04 19:09:26',114),(835,4,'2018-01-04 19:09:26',114),(836,4,'2018-01-04 19:09:26',114),(837,4,'2018-01-04 19:09:26',114),(838,4,'2018-01-04 19:09:27',114),(839,4,'2018-01-04 19:09:27',114),(840,4,'2018-01-04 19:09:27',114),(841,4,'2018-01-04 19:09:27',114),(842,4,'2018-01-04 19:09:27',114),(843,4,'2018-01-04 19:09:27',114),(844,4,'2018-01-04 19:09:27',114),(845,4,'2018-01-04 19:09:28',114),(846,4,'2018-01-04 19:09:28',114),(847,4,'2018-01-04 19:09:28',114),(848,4,'2018-01-04 19:09:28',114),(849,4,'2018-01-04 19:09:28',114),(850,4,'2018-01-04 19:09:28',114),(851,4,'2018-01-04 19:09:29',114),(852,4,'2018-01-04 19:09:29',114),(853,4,'2018-01-04 19:09:29',114),(854,4,'2018-01-04 19:09:29',114),(855,4,'2018-01-04 19:09:29',114),(856,4,'2018-01-04 19:09:29',114),(857,4,'2018-01-04 19:09:30',114),(858,4,'2018-01-04 19:09:30',114),(859,4,'2018-01-04 19:09:30',114),(860,4,'2018-01-04 19:09:30',114),(861,4,'2018-01-04 19:09:30',114),(862,4,'2018-01-04 19:09:30',114),(863,4,'2018-01-04 19:09:31',114),(864,4,'2018-01-04 19:09:31',114),(865,4,'2018-01-04 19:09:31',114),(866,4,'2018-01-04 19:09:31',114),(867,4,'2018-01-04 19:09:31',114),(868,4,'2018-01-04 19:09:31',114),(869,4,'2018-01-04 19:09:32',114),(870,4,'2018-01-04 19:09:32',114),(871,4,'2018-01-04 19:09:32',114),(872,4,'2018-01-04 19:09:32',114),(873,4,'2018-01-04 19:09:32',114),(874,4,'2018-01-04 19:09:32',114),(875,4,'2018-01-04 19:09:33',114),(876,4,'2018-01-04 19:09:33',114),(877,4,'2018-01-04 19:09:33',114),(878,4,'2018-01-04 19:09:33',114),(879,4,'2018-01-04 19:09:33',114),(880,4,'2018-01-04 19:09:34',114),(881,4,'2018-01-04 19:09:34',114),(882,4,'2018-01-04 19:09:34',114),(883,4,'2018-01-04 19:09:34',114),(884,4,'2018-01-04 19:09:34',114),(885,4,'2018-01-04 19:09:34',114),(886,4,'2018-01-04 19:09:35',114),(887,4,'2018-01-04 19:09:35',114),(888,4,'2018-01-04 19:09:35',114),(889,4,'2018-01-04 19:09:35',114),(890,4,'2018-01-04 19:09:35',114),(891,4,'2018-01-04 19:09:35',114),(892,4,'2018-01-04 19:09:36',114),(893,4,'2018-01-04 19:09:36',114),(894,4,'2018-01-04 19:09:36',114),(895,4,'2018-01-04 19:09:36',114),(896,4,'2018-01-04 19:09:36',114),(897,4,'2018-01-04 19:09:36',114),(898,4,'2018-01-04 19:09:36',114),(899,4,'2018-01-04 19:09:37',114),(900,4,'2018-01-04 19:09:37',114),(901,4,'2018-01-04 19:09:37',114),(902,4,'2018-01-04 19:09:37',114),(903,4,'2018-01-04 19:09:37',114),(904,4,'2018-01-04 19:09:37',114),(905,4,'2018-01-04 19:09:38',114),(906,4,'2018-01-04 19:09:38',114),(907,4,'2018-01-04 19:09:38',114),(908,4,'2018-01-04 19:09:38',114),(909,4,'2018-01-04 19:09:38',114),(910,4,'2018-01-04 19:09:38',114),(911,4,'2018-01-04 19:09:39',114),(912,4,'2018-01-04 19:09:39',114),(913,4,'2018-01-04 19:09:39',114),(914,4,'2018-01-04 19:09:39',114),(915,4,'2018-01-04 19:09:39',114),(916,4,'2018-01-04 19:09:39',114),(917,4,'2018-01-04 19:09:40',114),(918,4,'2018-01-04 19:09:40',114),(919,4,'2018-01-04 19:09:40',114),(920,4,'2018-01-04 19:09:40',114),(921,4,'2018-01-04 19:09:40',114),(922,4,'2018-01-04 19:09:40',114),(923,4,'2018-01-04 19:09:40',114),(924,4,'2018-01-04 19:09:41',114),(925,4,'2018-01-04 19:09:41',114),(926,4,'2018-01-04 19:09:41',114),(927,4,'2018-01-04 19:09:41',114),(928,4,'2018-01-04 19:09:41',114),(929,4,'2018-01-04 19:09:41',114),(930,4,'2018-01-04 19:09:42',114),(931,4,'2018-01-04 19:09:42',114),(932,4,'2018-01-04 19:09:42',114),(933,4,'2018-01-04 19:09:42',114),(934,4,'2018-01-04 19:09:42',114),(935,4,'2018-01-04 19:09:42',114),(936,4,'2018-01-04 19:09:43',114),(937,4,'2018-01-04 19:09:43',114),(938,4,'2018-01-04 19:09:43',114),(939,4,'2018-01-04 19:09:43',114),(940,4,'2018-01-04 19:09:43',114),(941,4,'2018-01-04 19:09:43',114),(942,4,'2018-01-04 19:09:44',114),(943,4,'2018-01-04 19:09:44',114),(944,4,'2018-01-04 19:09:44',114),(945,4,'2018-01-04 19:09:44',114),(946,4,'2018-01-04 19:09:44',114),(947,4,'2018-01-04 19:09:44',114),(948,4,'2018-01-04 19:09:44',114),(949,4,'2018-01-04 19:09:45',114),(950,4,'2018-01-04 19:09:45',114),(951,4,'2018-01-04 19:09:45',114),(952,4,'2018-01-04 19:09:45',114),(953,4,'2018-01-04 19:09:45',114),(954,4,'2018-01-04 19:09:45',114),(955,4,'2018-01-04 19:09:46',114),(956,4,'2018-01-04 19:09:46',114),(957,4,'2018-01-04 19:09:46',114),(958,4,'2018-01-04 19:09:46',114),(959,4,'2018-01-04 19:09:46',114),(960,4,'2018-01-04 19:09:46',114),(961,4,'2018-01-04 19:09:47',114),(962,4,'2018-01-04 19:09:47',114),(963,4,'2018-01-04 19:09:47',114),(964,4,'2018-01-04 19:09:47',114),(965,4,'2018-01-04 19:09:47',114),(966,4,'2018-01-04 19:09:48',114),(967,4,'2018-01-04 19:09:48',114),(968,4,'2018-01-04 19:09:48',114),(969,4,'2018-01-04 19:09:48',114),(970,4,'2018-01-04 19:09:48',114),(971,4,'2018-01-04 19:09:49',114),(972,4,'2018-01-04 19:09:49',114),(973,4,'2018-01-04 19:09:49',114),(974,4,'2018-01-04 19:09:49',114),(975,4,'2018-01-04 19:09:49',114),(976,4,'2018-01-04 19:09:49',114),(977,4,'2018-01-04 19:09:50',114),(978,4,'2018-01-04 19:09:50',114),(979,4,'2018-01-04 19:09:50',114),(980,4,'2018-01-04 19:09:50',114),(981,4,'2018-01-04 19:09:51',114),(982,4,'2018-01-04 19:09:51',114),(983,4,'2018-01-04 19:09:51',114),(984,4,'2018-01-04 19:09:51',114),(985,4,'2018-01-04 19:09:52',114),(986,4,'2018-01-04 19:09:52',114),(987,4,'2018-01-04 19:09:52',114),(988,4,'2018-01-04 19:09:52',114),(989,4,'2018-01-04 19:09:52',114),(990,4,'2018-01-04 19:09:52',114),(991,4,'2018-01-04 19:09:53',114),(992,4,'2018-01-04 19:09:53',114),(993,4,'2018-01-04 19:09:53',114),(994,4,'2018-01-04 19:09:53',114),(995,4,'2018-01-04 19:09:54',114),(996,4,'2018-01-04 19:09:54',114),(997,4,'2018-01-04 19:09:54',114),(998,4,'2018-01-04 19:09:54',114),(999,4,'2018-01-04 19:09:54',114),(1000,4,'2018-01-04 19:09:54',114),(1001,4,'2018-01-04 19:09:55',114),(1002,4,'2018-01-04 19:09:55',114),(1003,4,'2018-01-04 19:09:55',114),(1004,4,'2018-01-04 19:09:55',114),(1005,4,'2018-01-04 19:09:55',114),(1006,4,'2018-01-04 19:09:55',114),(1007,4,'2018-01-04 19:09:56',114),(1008,4,'2018-01-04 19:09:56',114),(1009,4,'2018-01-04 19:09:56',114),(1010,4,'2018-01-04 19:09:56',114),(1011,4,'2018-01-04 19:09:56',114),(1012,4,'2018-01-04 19:09:57',114),(1013,4,'2018-01-04 19:09:57',114),(1014,4,'2018-01-04 19:09:57',114),(1015,4,'2018-01-04 19:09:57',114),(1016,4,'2018-01-04 19:09:57',114),(1017,4,'2018-01-04 19:09:57',114),(1018,4,'2018-01-04 19:09:57',114),(1019,4,'2018-01-04 19:09:58',114),(1020,4,'2018-01-04 19:09:58',114),(1021,4,'2018-01-04 19:09:58',114),(1022,4,'2018-01-04 19:09:59',114),(1023,4,'2018-01-04 19:09:59',114),(1024,4,'2018-01-04 19:09:59',114),(1025,4,'2018-01-04 19:09:59',114),(1026,4,'2018-01-04 19:09:59',114),(1027,4,'2018-01-04 19:09:59',114),(1028,4,'2018-01-04 19:10:00',114),(1029,4,'2018-01-04 19:10:00',114),(1030,4,'2018-01-04 19:10:00',114),(1031,4,'2018-01-04 19:10:00',114),(1032,4,'2018-01-04 19:10:00',114),(1033,4,'2018-01-04 19:10:00',114),(1034,4,'2018-01-04 19:10:00',114),(1035,4,'2018-01-04 19:10:01',114),(1036,4,'2018-01-04 19:10:01',114),(1037,4,'2018-01-04 19:10:01',114),(1038,4,'2018-01-04 19:10:01',114),(1039,4,'2018-01-04 19:10:01',114),(1040,4,'2018-01-04 19:10:01',114),(1041,4,'2018-01-04 19:10:02',114),(1042,4,'2018-01-04 19:10:02',114),(1043,4,'2018-01-04 19:10:02',114),(1044,4,'2018-01-04 19:10:02',114),(1045,4,'2018-01-04 19:10:02',114),(1046,4,'2018-01-04 19:10:02',114),(1047,4,'2018-01-04 19:10:03',114),(1048,4,'2018-01-04 19:10:03',114),(1049,4,'2018-01-04 19:10:03',114),(1050,4,'2018-01-04 19:10:03',114),(1051,4,'2018-01-04 19:10:03',114),(1052,4,'2018-01-04 19:10:04',114),(1053,4,'2018-01-04 19:10:04',114),(1054,4,'2018-01-04 19:10:04',114),(1055,4,'2018-01-04 19:10:04',114),(1056,4,'2018-01-04 19:10:05',114),(1057,4,'2018-01-04 19:10:05',114),(1058,4,'2018-01-04 19:10:05',114),(1059,4,'2018-01-04 19:10:05',114),(1060,4,'2018-01-04 19:10:05',114),(1061,4,'2018-01-04 19:10:06',114),(1062,4,'2018-01-04 19:10:06',114),(1063,4,'2018-01-04 19:10:06',114),(1064,4,'2018-01-04 19:10:06',114),(1065,4,'2018-01-04 19:10:06',114),(1066,4,'2018-01-04 19:10:06',114),(1067,4,'2018-01-04 19:10:06',114),(1068,4,'2018-01-04 19:10:07',114),(1069,4,'2018-01-04 19:10:07',114),(1070,4,'2018-01-04 19:10:07',114),(1071,4,'2018-01-04 19:10:07',114),(1072,4,'2018-01-04 19:10:07',114),(1073,4,'2018-01-04 19:10:07',114),(1074,4,'2018-01-04 19:10:08',114),(1075,4,'2018-01-04 19:10:08',114),(1076,4,'2018-01-04 19:10:08',114),(1077,4,'2018-01-04 19:10:08',114),(1078,4,'2018-01-04 19:10:08',114),(1079,4,'2018-01-04 19:10:09',114),(1080,4,'2018-01-04 19:10:09',114),(1081,4,'2018-01-04 19:10:09',114),(1082,4,'2018-01-04 19:10:09',114),(1083,4,'2018-01-04 19:10:09',114),(1084,4,'2018-01-04 19:10:09',114),(1085,4,'2018-01-04 19:10:09',114),(1086,4,'2018-01-04 19:10:10',114),(1087,4,'2018-01-04 19:10:10',114),(1088,4,'2018-01-04 19:10:10',114),(1089,4,'2018-01-04 19:10:10',114),(1090,4,'2018-01-04 19:10:10',114),(1091,4,'2018-01-04 19:10:10',114),(1092,4,'2018-01-04 19:10:10',114),(1093,4,'2018-01-04 19:10:11',114),(1094,4,'2018-01-04 19:10:11',114),(1095,4,'2018-01-04 19:10:11',114),(1096,4,'2018-01-04 19:10:11',114),(1097,4,'2018-01-04 19:10:11',114),(1098,4,'2018-01-04 19:10:11',114),(1099,4,'2018-01-04 19:10:12',114),(1100,4,'2018-01-04 19:10:12',114),(1101,4,'2018-01-04 19:10:12',114),(1102,4,'2018-01-04 19:10:12',114),(1103,4,'2018-01-04 19:10:12',114),(1104,4,'2018-01-04 19:10:12',114),(1105,4,'2018-01-04 19:10:13',114),(1106,4,'2018-01-04 19:10:13',114),(1107,4,'2018-01-04 19:10:13',114),(1108,4,'2018-01-04 19:10:13',114),(1109,4,'2018-01-04 19:10:13',114),(1110,4,'2018-01-04 19:10:13',114),(1111,4,'2018-01-04 19:10:13',114),(1112,4,'2018-01-04 19:10:14',114),(1113,4,'2018-01-04 19:10:14',114),(1114,4,'2018-01-04 19:10:14',114),(1115,4,'2018-01-04 19:10:14',114),(1116,4,'2018-01-04 19:10:14',114),(1117,4,'2018-01-04 19:10:14',114),(1118,4,'2018-01-04 19:10:14',114),(1119,4,'2018-01-04 19:10:15',114),(1120,4,'2018-01-04 19:10:15',114),(1121,4,'2018-01-04 19:10:15',114),(1122,4,'2018-01-04 19:10:15',114),(1123,4,'2018-01-04 19:10:15',114),(1124,4,'2018-01-04 19:10:15',114),(1125,4,'2018-01-04 19:10:16',114),(1126,4,'2018-01-04 19:10:16',114),(1127,4,'2018-01-04 19:10:16',114),(1128,4,'2018-01-04 19:10:16',114),(1129,4,'2018-01-04 19:10:16',114),(1130,4,'2018-01-04 19:10:16',114),(1131,4,'2018-01-04 19:10:16',114),(1132,4,'2018-01-04 19:10:17',114),(1133,4,'2018-01-04 19:10:17',114),(1134,4,'2018-01-04 19:10:17',114),(1135,4,'2018-01-04 19:10:17',114),(1136,4,'2018-01-04 19:10:17',114),(1137,4,'2018-01-04 19:10:17',114),(1138,4,'2018-01-04 19:10:17',114),(1139,4,'2018-01-04 19:10:18',114),(1140,4,'2018-01-04 19:10:18',114),(1141,4,'2018-01-04 19:10:18',114),(1142,4,'2018-01-04 19:10:18',114),(1143,4,'2018-01-04 19:10:18',114),(1144,4,'2018-01-04 19:10:19',114),(1145,4,'2018-01-04 19:10:19',114),(1146,4,'2018-01-04 19:10:19',114),(1147,4,'2018-01-04 19:10:19',114),(1148,4,'2018-01-04 19:10:19',114),(1149,4,'2018-01-04 19:10:19',114),(1150,4,'2018-01-04 19:10:19',114),(1151,4,'2018-01-04 19:10:20',114),(1152,4,'2018-01-04 19:10:20',114),(1153,4,'2018-01-04 19:10:20',114),(1154,4,'2018-01-04 19:10:20',114),(1155,4,'2018-01-04 19:10:20',114),(1156,4,'2018-01-04 19:10:20',114),(1157,4,'2018-01-04 19:10:21',114),(1158,4,'2018-01-04 19:10:21',114),(1159,4,'2018-01-04 19:10:21',114),(1160,4,'2018-01-04 19:10:21',114),(1161,4,'2018-01-04 19:10:21',114),(1162,4,'2018-01-04 19:10:21',114),(1163,4,'2018-01-04 19:10:21',114),(1164,4,'2018-01-04 19:10:22',114),(1165,4,'2018-01-04 19:10:22',114),(1166,4,'2018-01-04 19:10:22',114),(1167,4,'2018-01-04 19:10:22',114),(1168,4,'2018-01-04 19:10:22',114),(1169,4,'2018-01-04 19:10:22',114),(1170,4,'2018-01-04 19:10:23',114),(1171,4,'2018-01-04 19:10:23',114),(1172,4,'2018-01-04 19:10:23',114),(1173,4,'2018-01-04 19:10:23',114),(1174,4,'2018-01-04 19:10:23',114),(1175,4,'2018-01-04 19:10:23',114),(1176,4,'2018-01-04 19:10:24',114),(1177,4,'2018-01-04 19:10:24',114),(1178,4,'2018-01-04 19:10:24',114),(1179,4,'2018-01-04 19:10:24',114),(1180,4,'2018-01-04 19:10:24',114),(1181,4,'2018-01-04 19:10:24',114),(1182,4,'2018-01-04 19:10:25',114),(1183,4,'2018-01-04 19:10:25',114),(1184,4,'2018-01-04 19:10:25',114),(1185,4,'2018-01-04 19:10:25',114),(1186,4,'2018-01-04 19:10:25',114),(1187,4,'2018-01-04 19:10:25',114),(1188,4,'2018-01-04 19:10:25',114),(1189,4,'2018-01-04 19:10:26',114),(1190,4,'2018-01-04 19:10:26',114),(1191,4,'2018-01-04 19:10:26',114),(1192,4,'2018-01-04 19:10:26',114),(1193,4,'2018-01-04 19:10:26',114),(1194,4,'2018-01-04 19:10:26',114),(1195,4,'2018-01-04 19:10:27',114),(1196,4,'2018-01-04 19:10:27',114),(1197,4,'2018-01-04 19:10:27',114),(1198,4,'2018-01-04 19:10:27',114),(1199,4,'2018-01-04 19:10:27',114),(1200,4,'2018-01-04 19:10:27',114),(1201,4,'2018-01-04 19:10:28',114),(1202,4,'2018-01-04 19:10:28',114),(1203,4,'2018-01-04 19:10:28',114),(1204,4,'2018-01-04 19:10:28',114),(1205,4,'2018-01-04 19:10:28',114),(1206,4,'2018-01-04 19:10:28',114),(1207,4,'2018-01-04 19:10:29',114),(1208,4,'2018-01-04 19:10:29',114),(1209,4,'2018-01-04 19:10:29',114),(1210,4,'2018-01-04 19:10:29',114),(1211,4,'2018-01-04 19:10:29',114),(1212,4,'2018-01-04 19:10:29',114),(1213,4,'2018-01-04 19:10:30',114),(1214,4,'2018-01-04 19:10:30',114),(1215,4,'2018-01-04 19:10:30',114),(1216,4,'2018-01-04 19:10:30',114),(1217,4,'2018-01-04 19:10:30',114),(1218,4,'2018-01-04 19:10:30',114),(1219,4,'2018-01-04 19:10:30',114),(1220,4,'2018-01-04 19:10:31',114),(1221,4,'2018-01-04 19:10:31',114),(1222,4,'2018-01-04 19:10:31',114),(1223,4,'2018-01-04 19:10:31',114),(1224,4,'2018-01-04 19:10:31',114),(1225,4,'2018-01-04 19:10:31',114),(1226,4,'2018-01-04 19:10:32',114),(1227,4,'2018-01-04 19:10:32',114),(1228,4,'2018-01-04 19:10:32',114),(1229,4,'2018-01-04 19:10:32',114),(1230,4,'2018-01-04 19:10:32',114),(1231,4,'2018-01-04 19:10:32',114),(1232,4,'2018-01-04 19:10:32',114),(1233,4,'2018-01-04 19:10:32',114),(1234,4,'2018-01-04 19:10:33',114),(1235,4,'2018-01-04 19:10:33',114),(1236,4,'2018-01-04 19:10:33',114),(1237,4,'2018-01-04 19:10:33',114),(1238,4,'2018-01-04 19:10:33',114),(1239,4,'2018-01-04 19:10:33',114),(1240,4,'2018-01-04 19:10:34',114),(1241,4,'2018-01-04 19:10:34',114),(1242,4,'2018-01-04 19:10:34',114),(1243,4,'2018-01-04 19:10:34',114),(1244,4,'2018-01-04 19:10:34',114),(1245,4,'2018-01-04 19:10:34',114),(1246,4,'2018-01-04 19:10:34',114),(1247,4,'2018-01-04 19:10:34',114),(1248,4,'2018-01-04 19:10:35',114),(1249,4,'2018-01-04 19:10:35',114),(1250,4,'2018-01-04 19:10:35',114),(1251,4,'2018-01-04 19:10:35',114),(1252,4,'2018-01-04 19:10:35',114),(1253,4,'2018-01-04 19:10:35',114),(1254,4,'2018-01-04 19:10:35',114),(1255,4,'2018-01-04 19:10:36',114),(1256,4,'2018-01-04 19:10:36',114),(1257,4,'2018-01-04 19:10:36',114),(1258,4,'2018-01-04 19:10:36',114),(1259,4,'2018-01-04 19:10:36',114),(1260,4,'2018-01-04 19:10:36',114),(1261,4,'2018-01-04 19:10:36',114),(1262,4,'2018-01-04 19:10:36',114),(1263,4,'2018-01-04 19:10:37',114),(1264,4,'2018-01-04 19:10:37',114),(1265,4,'2018-01-04 19:10:37',114),(1266,4,'2018-01-04 19:10:37',114),(1267,4,'2018-01-04 19:10:37',114),(1268,4,'2018-01-04 19:10:37',114),(1269,4,'2018-01-04 19:10:37',114),(1270,4,'2018-01-04 19:10:38',114),(1271,4,'2018-01-04 19:10:38',114),(1272,4,'2018-01-04 19:10:38',114),(1273,4,'2018-01-04 19:10:38',114),(1274,4,'2018-01-04 19:10:38',114),(1275,4,'2018-01-04 19:10:38',114),(1276,4,'2018-01-04 19:10:38',114),(1277,4,'2018-01-04 19:10:39',114),(1278,4,'2018-01-04 19:10:39',114),(1279,4,'2018-01-04 19:10:39',114),(1280,4,'2018-01-04 19:10:39',114),(1281,4,'2018-01-04 19:10:39',114),(1282,4,'2018-01-04 19:10:39',114),(1283,4,'2018-01-04 19:10:39',114),(1284,4,'2018-01-04 19:10:40',114),(1285,4,'2018-01-04 19:10:40',114),(1286,4,'2018-01-04 19:10:40',114),(1287,4,'2018-01-04 19:10:40',114),(1288,4,'2018-01-04 19:10:40',114),(1289,4,'2018-01-04 19:10:40',114),(1290,4,'2018-01-04 19:10:40',114),(1291,4,'2018-01-04 19:10:40',114),(1292,4,'2018-01-04 19:10:41',114),(1293,4,'2018-01-04 19:10:41',114),(1294,4,'2018-01-04 19:10:41',114),(1295,4,'2018-01-04 19:10:41',114),(1296,4,'2018-01-04 19:10:41',114),(1297,4,'2018-01-04 19:10:41',114),(1298,4,'2018-01-04 19:10:41',114),(1299,4,'2018-01-04 19:10:41',114),(1300,4,'2018-01-04 19:10:42',114),(1301,4,'2018-01-04 19:10:42',114),(1302,4,'2018-01-04 19:10:42',114),(1303,4,'2018-01-04 19:10:42',114),(1304,4,'2018-01-04 19:10:42',114),(1305,4,'2018-01-04 19:10:42',114),(1306,4,'2018-01-04 19:10:42',114),(1307,4,'2018-01-04 19:10:42',114),(1308,4,'2018-01-04 19:10:43',114),(1309,4,'2018-01-04 19:10:43',114),(1310,4,'2018-01-04 19:10:43',114),(1311,4,'2018-01-04 19:10:43',114),(1312,4,'2018-01-04 19:10:43',114),(1313,4,'2018-01-04 19:10:43',114),(1314,4,'2018-01-04 19:10:43',114),(1315,4,'2018-01-04 19:10:43',114),(1316,4,'2018-01-04 19:10:44',114),(1317,4,'2018-01-04 19:10:44',114),(1318,4,'2018-01-04 19:10:44',114),(1319,4,'2018-01-04 19:10:44',114),(1320,4,'2018-01-04 19:10:44',114),(1321,4,'2018-01-04 19:10:44',114),(1322,4,'2018-01-04 19:10:44',114),(1323,4,'2018-01-04 19:10:45',114),(1324,4,'2018-01-04 19:10:45',114),(1325,4,'2018-01-04 19:10:45',114),(1326,4,'2018-01-04 19:10:45',114),(1327,4,'2018-01-04 19:10:45',114),(1328,4,'2018-01-04 19:10:45',114),(1329,4,'2018-01-04 19:10:45',114),(1330,4,'2018-01-04 19:10:46',114),(1331,4,'2018-01-04 19:10:46',114),(1332,4,'2018-01-04 19:10:46',114),(1333,4,'2018-01-04 19:10:46',114),(1334,4,'2018-01-04 19:10:46',114),(1335,4,'2018-01-04 19:10:46',114),(1336,4,'2018-01-04 19:10:46',114),(1337,4,'2018-01-04 19:10:46',114),(1338,4,'2018-01-04 19:10:46',114),(1339,4,'2018-01-04 19:10:47',114),(1340,4,'2018-01-04 19:10:47',114),(1341,4,'2018-01-04 19:10:47',114),(1342,4,'2018-01-04 19:10:47',114),(1343,4,'2018-01-04 19:10:47',114),(1344,4,'2018-01-04 19:10:47',114),(1345,4,'2018-01-04 19:10:47',114),(1346,4,'2018-01-04 19:10:47',114),(1347,4,'2018-01-04 19:10:48',114),(1348,4,'2018-01-04 19:10:48',114),(1349,4,'2018-01-04 19:10:48',114),(1350,4,'2018-01-04 19:10:48',114),(1351,4,'2018-01-04 19:10:48',114),(1352,4,'2018-01-04 19:10:48',114),(1353,4,'2018-01-04 19:10:48',114),(1354,4,'2018-01-04 19:10:49',114),(1355,4,'2018-01-04 19:10:49',114),(1356,4,'2018-01-04 19:10:49',114),(1357,4,'2018-01-04 19:10:49',114),(1358,4,'2018-01-04 19:10:49',114),(1359,4,'2018-01-04 19:10:49',114),(1360,4,'2018-01-04 19:10:49',114),(1361,4,'2018-01-04 19:10:50',114),(1362,4,'2018-01-04 19:10:50',114),(1363,4,'2018-01-04 19:10:50',114),(1364,4,'2018-01-04 19:10:50',114),(1365,4,'2018-01-04 19:10:50',114),(1366,4,'2018-01-04 19:10:51',114),(1367,4,'2018-01-04 19:10:51',114),(1368,4,'2018-01-04 19:10:51',114),(1369,4,'2018-01-04 19:10:51',114),(1370,4,'2018-01-04 19:10:51',114),(1371,4,'2018-01-04 19:10:51',114),(1372,4,'2018-01-04 19:10:51',114),(1373,4,'2018-01-04 19:10:52',114),(1374,4,'2018-01-04 19:10:52',114),(1375,4,'2018-01-04 19:10:52',114),(1376,4,'2018-01-04 19:10:52',114),(1377,4,'2018-01-04 19:10:52',114),(1378,4,'2018-01-04 19:10:52',114),(1379,4,'2018-01-04 19:10:52',114),(1380,4,'2018-01-04 19:10:53',114),(1381,4,'2018-01-04 19:10:53',114),(1382,4,'2018-01-04 19:10:53',114),(1383,4,'2018-01-04 19:10:53',114),(1384,4,'2018-01-04 19:10:54',114),(1385,4,'2018-01-04 19:10:54',114),(1386,4,'2018-01-04 19:10:54',114),(1387,4,'2018-01-04 19:10:54',114),(1388,4,'2018-01-04 19:10:54',114),(1389,4,'2018-01-04 19:10:55',114),(1390,4,'2018-01-04 19:10:55',114),(1391,4,'2018-01-04 19:10:55',114),(1392,4,'2018-01-04 19:10:55',114),(1393,4,'2018-01-04 19:10:55',114),(1394,4,'2018-01-04 19:10:55',114),(1395,4,'2018-01-04 19:10:55',114),(1396,4,'2018-01-04 19:10:56',114),(1397,4,'2018-01-04 19:10:56',114),(1398,4,'2018-01-04 19:10:56',114),(1399,4,'2018-01-04 19:10:56',114),(1400,4,'2018-01-04 19:10:56',114),(1401,4,'2018-01-04 19:10:56',114),(1402,4,'2018-01-04 19:10:57',114),(1403,4,'2018-01-04 19:10:57',114),(1404,4,'2018-01-04 19:10:57',114),(1405,4,'2018-01-04 19:10:57',114),(1406,4,'2018-01-04 19:10:58',114),(1407,4,'2018-01-04 19:10:58',114),(1408,4,'2018-01-04 19:10:58',114),(1474,1,'2018-01-04 19:16:01',103),(1475,2,'2018-01-04 19:16:01',103),(1476,3,'2018-01-04 19:16:02',103),(1477,3,'2018-01-04 19:16:03',103),(1478,3,'2018-01-04 19:16:03',103),(1479,3,'2018-01-04 19:16:03',103),(1480,3,'2018-01-04 19:16:04',103),(1481,3,'2018-01-04 19:16:04',103),(1482,3,'2018-01-04 19:16:04',103),(1483,3,'2018-01-04 19:16:05',103),(1484,3,'2018-01-04 19:16:06',103),(1485,3,'2018-01-04 19:16:06',103),(1486,3,'2018-01-04 19:16:06',103),(1487,3,'2018-01-04 19:16:06',103),(1488,3,'2018-01-04 19:16:06',103),(1489,3,'2018-01-04 19:16:06',103),(1490,3,'2018-01-04 19:16:06',103),(1491,3,'2018-01-04 19:16:09',103),(1492,3,'2018-01-04 19:16:09',103),(1493,3,'2018-01-04 19:16:09',103),(1494,3,'2018-01-04 19:16:09',103),(1495,3,'2018-01-04 19:16:09',103),(1496,3,'2018-01-04 19:16:09',103),(1497,3,'2018-01-04 19:16:09',103),(1498,3,'2018-01-04 19:16:09',103),(1499,3,'2018-01-04 19:16:10',103),(1500,3,'2018-01-04 19:16:10',103),(1501,3,'2018-01-04 19:16:10',103),(1502,3,'2018-01-04 19:16:10',103),(1503,3,'2018-01-04 19:16:10',103),(1504,3,'2018-01-04 19:16:10',103),(1505,3,'2018-01-04 19:16:10',103),(1506,3,'2018-01-04 19:16:11',103),(1519,0,'2018-01-04 19:27:29',142),(1520,1,'2018-01-04 19:32:38',142),(1521,0,'2018-01-04 19:35:43',143),(1522,4,'2018-01-04 19:39:00',143),(1532,2,'2018-01-04 20:42:02',142),(1533,4,'2018-01-05 13:17:41',87),(1538,0,'2018-01-05 16:34:51',148),(1539,0,'2018-01-05 16:47:40',149),(1540,4,'2018-01-05 17:47:35',149),(1541,4,'2018-01-05 17:47:40',148),(1547,0,'2018-01-05 18:04:15',151),(1548,1,'2018-01-05 18:14:27',151),(1549,2,'2018-01-05 18:14:30',151),(1550,4,'2018-01-05 18:14:39',151),(1551,0,'2018-01-05 18:30:05',152),(1552,1,'2018-01-05 18:30:18',152),(1553,4,'2018-01-05 18:30:19',152),(1554,0,'2018-01-05 18:30:41',153),(1555,1,'2018-01-05 18:30:48',153),(1556,2,'2018-01-05 18:30:49',153),(1557,3,'2018-01-05 18:30:50',153),(1558,0,'2018-01-05 18:32:11',154),(1559,1,'2018-01-05 18:32:19',154),(1560,1,'2018-01-05 18:35:43',98),(1561,0,'2018-01-06 19:51:34',155),(1562,1,'2018-01-06 19:51:42',155),(1563,2,'2018-01-06 19:53:47',155),(1564,0,'2018-01-07 17:08:40',156),(1565,0,'2018-01-07 17:30:12',157),(1566,4,'2018-01-07 17:30:33',157),(1567,0,'2018-01-07 17:30:48',158);
/*!40000 ALTER TABLE `order_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_recovery`
--

DROP TABLE IF EXISTS `password_recovery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_recovery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `pin` varchar(4) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `password_recovery_fk_user_idx` (`user`),
  CONSTRAINT `password_recovery_fk_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_recovery`
--

LOCK TABLES `password_recovery` WRITE;
/*!40000 ALTER TABLE `password_recovery` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_recovery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod_ing`
--

DROP TABLE IF EXISTS `prod_ing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_ing` (
  `product` int(11) NOT NULL,
  `ingredient` int(11) NOT NULL,
  PRIMARY KEY (`product`,`ingredient`),
  KEY `ingredient_id_fk_of_prod_ing_idx` (`ingredient`),
  CONSTRAINT `ingredient_id_fk_of_prod_ing` FOREIGN KEY (`ingredient`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `product_id_fk_of_prod_ing` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod_ing`
--

LOCK TABLES `prod_ing` WRITE;
/*!40000 ALTER TABLE `prod_ing` DISABLE KEYS */;
INSERT INTO `prod_ing` VALUES (1,1),(1,2),(1,3),(4,4),(4,5),(4,6),(4,7),(4,8),(6,8),(6,9),(6,10),(7,11),(7,12),(8,4),(8,5),(8,13),(8,14),(8,15),(15,5),(15,15),(15,16),(15,17),(19,9),(19,13),(20,3),(20,5),(20,6),(20,15),(20,31),(21,5),(21,17),(21,32),(22,5),(22,7),(22,15),(22,31),(22,33),(23,3),(23,5),(23,34),(23,35),(24,5),(24,17),(24,36),(24,37),(24,38),(25,3),(25,5),(25,36),(25,39),(26,2),(26,3),(26,5),(26,7),(26,13),(27,3),(27,5),(27,39),(27,40),(27,41);
/*!40000 ALTER TABLE `prod_ing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prod_instance`
--

DROP TABLE IF EXISTS `prod_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prod_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) NOT NULL,
  `product_list` int(11) NOT NULL,
  `bread_type` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `bread_type_fk_of_prod_instance_idx` (`bread_type`),
  KEY `product_list_fk_of_prod_instance_idx` (`product_list`),
  KEY `product_fk_of_prod_instance_idx` (`product`),
  CONSTRAINT `bread_type_fk_of_prod_instance` FOREIGN KEY (`bread_type`) REFERENCES `bread_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `product_fk_of_prod_instance` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `product_list_fk_of_prod_instance` FOREIGN KEY (`product_list`) REFERENCES `product_list` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=944 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prod_instance`
--

LOCK TABLES `prod_instance` WRITE;
/*!40000 ALTER TABLE `prod_instance` DISABLE KEYS */;
INSERT INTO `prod_instance` VALUES (1,1,1,1,7),(2,1,1,2,2),(15,2,98,NULL,2),(16,3,98,NULL,2),(356,1,93,1,1),(376,1,94,1,1),(377,1,94,1,2),(378,2,94,NULL,2),(379,1,94,3,1),(380,1,94,1,1),(381,1,105,1,3),(382,2,105,NULL,2),(415,4,107,1,1),(423,2,107,NULL,2),(436,10,107,NULL,1),(437,6,107,3,1),(480,9,155,NULL,25),(481,5,155,NULL,19),(482,2,155,NULL,8),(483,14,155,NULL,8),(484,10,155,NULL,10),(485,3,155,NULL,6),(486,8,155,1,11),(487,7,155,1,8),(488,12,155,NULL,1),(489,11,155,NULL,6),(490,1,155,1,5),(491,4,155,1,2),(492,9,110,NULL,1),(528,4,156,1,1),(529,3,156,NULL,1),(530,4,156,2,1),(551,2,111,NULL,5),(555,5,114,NULL,1),(556,7,200,1,1),(557,3,200,NULL,1),(562,15,114,3,1),(563,15,115,1,1),(564,15,116,1,1),(565,15,117,1,1),(566,15,123,1,1),(567,6,123,1,1),(568,15,126,1,1),(569,15,127,1,1),(570,15,128,1,1),(571,15,129,1,1),(572,15,130,1,1),(573,15,131,1,1),(574,15,132,1,1),(575,15,133,1,1),(576,15,135,1,1),(577,15,149,1,1),(578,6,150,1,1),(579,15,151,1,1),(580,6,151,1,1),(581,15,152,1,1),(582,15,153,1,3),(589,7,154,2,8),(591,15,154,1,1),(598,2,157,NULL,1),(599,1,157,3,1),(600,1,158,1,1),(601,8,158,1,2),(602,15,159,1,1),(603,6,175,2,1),(604,2,161,NULL,1),(606,15,160,1,1),(607,9,162,NULL,1),(608,9,163,NULL,1),(609,5,163,NULL,1),(610,15,164,1,1),(611,6,164,1,1),(612,5,164,NULL,1),(613,15,165,1,1),(614,15,165,1,1),(615,15,167,1,1),(616,6,167,1,1),(617,15,168,1,1),(618,9,169,NULL,1),(619,9,170,NULL,1),(620,9,171,NULL,1),(621,9,172,NULL,1),(622,15,173,1,1),(623,6,173,1,1),(624,8,173,1,1),(625,9,174,NULL,1),(629,15,204,1,1),(631,15,176,1,1),(632,6,176,1,1),(636,15,177,1,1),(637,6,177,1,1),(638,19,177,1,1),(639,9,178,NULL,1),(640,9,179,NULL,1),(641,9,180,NULL,1),(642,9,181,NULL,1),(643,15,182,1,1),(644,6,182,1,1),(645,9,183,NULL,1),(646,9,184,NULL,1),(647,8,185,1,1),(648,7,185,1,1),(649,9,186,NULL,1),(650,9,187,NULL,1),(651,19,188,1,1),(652,8,188,1,1),(653,15,190,1,1),(654,19,189,1,1),(655,8,189,1,1),(656,6,191,1,1),(657,19,191,1,1),(658,6,192,1,1),(659,15,192,1,1),(660,9,193,NULL,1),(661,19,194,1,1),(662,8,194,1,1),(663,15,195,1,1),(664,15,196,1,1),(665,15,197,1,1),(666,15,198,1,1),(667,7,199,1,1),(668,6,200,1,1),(669,9,200,NULL,1),(670,9,201,NULL,1),(671,15,202,1,1),(672,9,203,NULL,1),(673,15,205,1,71),(674,6,205,1,57),(675,8,205,1,5),(676,13,205,NULL,22),(677,15,206,1,1),(678,6,206,1,1),(679,9,207,NULL,1),(682,9,212,NULL,1),(683,15,208,1,1),(684,15,208,1,1),(685,15,209,1,1),(686,6,209,1,1),(687,15,210,1,1),(688,15,210,3,1),(689,15,211,1,1),(690,9,211,NULL,1),(691,5,211,NULL,1),(692,2,211,NULL,1),(693,14,211,NULL,1),(694,13,211,NULL,1),(695,10,211,NULL,1),(696,3,211,NULL,1),(697,15,218,1,4),(698,15,214,1,2),(699,6,218,1,3),(700,6,214,1,6),(701,15,219,1,3),(702,15,220,1,3),(703,6,220,1,2),(704,15,222,1,1),(706,15,227,1,1),(707,15,223,1,1),(708,15,225,1,1),(709,15,229,1,2),(710,9,234,NULL,1),(711,15,228,1,1),(712,15,230,1,1),(713,6,230,1,1),(720,15,231,1,1),(721,6,231,1,1),(722,6,232,1,1),(723,15,232,1,1),(724,15,233,1,1),(725,15,235,1,1),(726,6,235,1,1),(727,21,237,1,1),(728,6,236,1,1),(729,21,238,1,1),(730,15,239,1,1),(732,15,241,1,1),(733,6,241,1,1),(734,15,242,1,1),(735,6,242,3,1),(736,15,244,1,2),(737,26,243,1,1),(738,15,244,2,1),(739,21,244,1,1),(740,10,244,NULL,1),(741,14,244,NULL,1),(746,1,245,1,3),(748,15,246,1,1),(749,21,246,1,1),(750,15,247,1,1),(752,15,248,1,1),(753,6,248,1,1),(754,15,249,1,1),(760,6,250,1,1),(770,15,252,1,63),(771,15,252,2,30),(772,21,252,1,30),(773,14,252,NULL,30),(774,10,252,NULL,30),(776,15,251,1,43),(777,6,251,1,43),(778,1,269,1,4),(779,6,252,1,1),(780,8,252,1,1),(792,15,253,1,756),(793,15,253,2,360),(794,21,253,1,360),(795,14,253,NULL,360),(796,10,253,NULL,360),(797,6,253,1,12),(798,8,253,1,12),(799,15,254,1,34776),(800,21,254,1,16560),(801,6,254,1,552),(802,14,254,NULL,16560),(803,10,254,NULL,16560),(804,8,254,1,552),(805,15,254,2,16560),(806,15,255,1,1),(831,15,256,1,3477600),(832,21,256,1,1656000),(833,6,256,1,55200),(834,10,256,NULL,1656000),(835,14,256,NULL,1656000),(836,8,256,1,55200),(837,15,256,2,1656000),(839,15,258,1,1),(840,9,258,NULL,1),(841,15,266,1,1),(842,6,266,1,1),(872,15,259,1,3477600),(873,21,259,1,1656000),(874,6,259,1,55200),(875,10,259,NULL,1656000),(876,8,259,1,55200),(877,14,259,NULL,1656000),(878,15,259,2,1656000),(879,15,260,1,170402400),(880,21,260,1,81144000),(881,6,260,1,2704800),(882,10,260,NULL,81144000),(883,8,260,1,2704800),(884,14,260,NULL,81144000),(885,15,260,2,81144000),(900,9,261,NULL,1),(901,15,261,1,1),(902,9,262,NULL,1),(903,15,262,1,1),(904,15,263,1,1),(905,9,263,NULL,1),(906,7,265,1,1),(907,27,265,1,1),(908,5,265,NULL,1),(909,15,267,1,7),(912,15,270,1,1),(913,6,270,1,1),(914,1,268,1,1),(915,15,257,1,1),(916,9,257,NULL,1),(917,1,270,1,4),(918,15,271,1,1),(919,6,271,1,1),(920,15,272,1,1),(921,6,272,1,1),(922,15,273,1,1),(923,6,273,1,1),(925,1,274,3,1),(933,15,113,1,170402400),(934,21,113,1,81144000),(935,6,113,1,2704800),(936,10,113,NULL,81144000),(937,8,113,1,2704800),(938,14,113,NULL,81144000),(939,15,113,2,81144000),(940,15,276,1,1),(941,9,276,NULL,1),(942,5,276,NULL,1),(943,15,277,1,1);
/*!40000 ALTER TABLE `prod_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `prep_time` int(11) NOT NULL,
  `buy_count` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `product_type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `created_by_fk_of_product_idx` (`created_by`),
  KEY `product_type_fk_of_product_idx` (`product_type`),
  CONSTRAINT `created_by_fk_of_product` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `product_type_fk_of_product` FOREIGN KEY (`product_type`) REFERENCES `product_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Romagnolo',4.50,600,0,1,3),(2,'Acqua naturale 0.5L (frigo) ',1.00,0,0,1,100),(3,'Coca cola 0,5L (frigo)',2.50,0,0,1,100),(4,'Vulcano',4.00,600,0,1,2),(5,'Acqua frizzante 0.5L (frigo)',1.00,0,0,1,100),(6,'Baldo',3.50,480,0,1,2),(7,'Cotto e fontina',3.00,300,0,1,1),(8,'Contadino',4.00,600,0,1,2),(9,'Acqua frizzante 0.5L',1.00,0,0,1,100),(10,'Coca cola 0,5L',2.50,0,0,1,100),(11,'Fanta 0.5L (frigo)',2.50,0,0,1,100),(12,'Fanta 0.5L',2.50,0,0,1,100),(13,'Acqua naturale 1L (frigo)',2.00,0,0,1,100),(14,'Acqua naturale 1L',2.00,0,0,1,100),(15,'Alice',4.00,600,0,1,2),(19,'Giovanna',4.50,400,0,113,1),(20,'Vizioso',4.50,600,0,113,2),(21,'Calimero',3.60,600,0,113,2),(22,'Topogigio',4.50,600,0,113,2),(23,'Heidi',4.20,600,0,113,2),(24,'Remy',4.50,600,0,113,2),(25,'Primavera',5.00,600,0,113,3),(26,'Mirko',5.50,600,0,113,3),(27,'Maia',5.50,600,0,113,3);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_bread_type`
--

DROP TABLE IF EXISTS `product_bread_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_bread_type` (
  `product_type` int(11) NOT NULL,
  `bread_type` int(11) NOT NULL,
  PRIMARY KEY (`product_type`,`bread_type`),
  KEY `bread_type_id_fk_of_product_bread_type_idx` (`bread_type`),
  CONSTRAINT `bread_type_id_fk_of_product_bread_type` FOREIGN KEY (`bread_type`) REFERENCES `bread_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `product_type_id_fk_of_product_bread_type` FOREIGN KEY (`product_type`) REFERENCES `product_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_bread_type`
--

LOCK TABLES `product_bread_type` WRITE;
/*!40000 ALTER TABLE `product_bread_type` DISABLE KEYS */;
INSERT INTO `product_bread_type` VALUES (1,1),(1,2),(1,3),(2,1),(2,2),(2,3),(3,1),(3,2),(3,3),(200,1),(200,4);
/*!40000 ALTER TABLE `product_bread_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_list`
--

DROP TABLE IF EXISTS `product_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_list`
--

LOCK TABLES `product_list` WRITE;
/*!40000 ALTER TABLE `product_list` DISABLE KEYS */;
INSERT INTO `product_list` VALUES (1),(29),(32),(34),(36),(37),(38),(39),(40),(43),(45),(48),(51),(52),(54),(55),(57),(60),(64),(67),(70),(71),(75),(76),(78),(79),(80),(81),(82),(83),(84),(85),(86),(87),(88),(89),(90),(91),(92),(93),(94),(95),(96),(97),(98),(99),(100),(101),(102),(103),(104),(105),(106),(107),(108),(109),(110),(111),(112),(113),(114),(115),(116),(117),(118),(119),(120),(121),(122),(123),(124),(125),(126),(127),(128),(129),(130),(131),(132),(133),(134),(135),(136),(137),(138),(139),(140),(141),(142),(143),(144),(145),(146),(147),(148),(149),(150),(151),(152),(153),(154),(155),(156),(157),(158),(159),(160),(161),(162),(163),(164),(165),(166),(167),(168),(169),(170),(171),(172),(173),(174),(175),(176),(177),(178),(179),(180),(181),(182),(183),(184),(185),(186),(187),(188),(189),(190),(191),(192),(193),(194),(195),(196),(197),(198),(199),(200),(201),(202),(203),(204),(205),(206),(207),(208),(209),(210),(211),(212),(213),(214),(215),(216),(217),(218),(219),(220),(221),(222),(223),(224),(225),(226),(227),(228),(229),(230),(231),(232),(233),(234),(235),(236),(237),(238),(239),(240),(241),(242),(243),(244),(245),(246),(247),(248),(249),(250),(251),(252),(253),(254),(255),(256),(257),(258),(259),(260),(261),(262),(263),(264),(265),(266),(267),(268),(269),(270),(271),(272),(273),(274),(275),(276),(277);
/*!40000 ALTER TABLE `product_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_type`
--

DROP TABLE IF EXISTS `product_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_type`
--

LOCK TABLES `product_type` WRITE;
/*!40000 ALTER TABLE `product_type` DISABLE KEYS */;
INSERT INTO `product_type` VALUES (100,'Bevanda'),(2,'Crescione'),(1,'Piadina'),(200,'provaa'),(3,'Rotolo');
/*!40000 ALTER TABLE `product_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `value` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (0,'page-size-of-rows','5');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(128) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `default_card` int(11) DEFAULT NULL,
  `default_address` int(11) DEFAULT NULL,
  `cart` int(11) NOT NULL,
  `usual` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `id_idx` (`default_card`),
  KEY `default_address_fk_of_user_idx` (`default_address`),
  KEY `cart_fk_of_user_idx` (`cart`),
  KEY `usual_fk_of_user_idx` (`usual`),
  CONSTRAINT `cart_fk_of_user` FOREIGN KEY (`cart`) REFERENCES `product_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `default_address_fk_of_user` FOREIGN KEY (`default_address`) REFERENCES `address` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `default_card_fk_of_user` FOREIGN KEY (`default_card`) REFERENCES `card` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `usual_fk_of_user` FOREIGN KEY (`usual`) REFERENCES `product_list` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@gmail.com','admin','admin','admin',1,NULL,NULL,1,NULL),(104,'edoardo.barbieri.96@gmail.com','7973a2df092837c71c99cde04368a8f705f318531e09fb1e4000449e4a21b46ffa9614b7e8732ef59f1244620d03a860fa4f10ee61cc0a58d47e77aa75eaf383','Edoardo','Barbieri',1,27,NULL,104,266),(108,'ep@gmail.com','507d420d1cff6573cfc142aae61e1fc61422b8cbf5018341bd502b714eab635db01e6c84066d232f8ad01b99d4b3ce5d38ad65038be8f656518a338be6dbce06','Emanuele','Pancisi',1,91,12,108,245),(113,'lnzmnd@yahoo.it','7973a2df092837c71c99cde04368a8f705f318531e09fb1e4000449e4a21b46ffa9614b7e8732ef59f1244620d03a860fa4f10ee61cc0a58d47e77aa75eaf383','Lorenzo','Mondani',1,56,NULL,113,260),(118,'mario.rossi@gmail.com','7973a2df092837c71c99cde04368a8f705f318531e09fb1e4000449e4a21b46ffa9614b7e8732ef59f1244620d03a860fa4f10ee61cc0a58d47e77aa75eaf383','Mario','Rossi',0,NULL,24,275,277);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `view_cart_product_list`
--

DROP TABLE IF EXISTS `view_cart_product_list`;
/*!50001 DROP VIEW IF EXISTS `view_cart_product_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_cart_product_list` AS SELECT 
 1 AS `instance_id`,
 1 AS `product_id`,
 1 AS `quantity`,
 1 AS `name`,
 1 AS `price`,
 1 AS `prep_time`,
 1 AS `product_list`,
 1 AS `product_type`,
 1 AS `bread_type_id`,
 1 AS `bread_type`,
 1 AS `bread_price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_food_list`
--

DROP TABLE IF EXISTS `view_food_list`;
/*!50001 DROP VIEW IF EXISTS `view_food_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_food_list` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `price`,
 1 AS `prep_time`,
 1 AS `product_type_id`,
 1 AS `product_type`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_product_ingredients`
--

DROP TABLE IF EXISTS `view_product_ingredients`;
/*!50001 DROP VIEW IF EXISTS `view_product_ingredients`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_product_ingredients` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `price`,
 1 AS `pid`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_product_list`
--

DROP TABLE IF EXISTS `view_product_list`;
/*!50001 DROP VIEW IF EXISTS `view_product_list`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_product_list` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `price`,
 1 AS `prep_time`,
 1 AS `buy_count`,
 1 AS `product_type_id`,
 1 AS `product_type`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_product_type_bread_types`
--

DROP TABLE IF EXISTS `view_product_type_bread_types`;
/*!50001 DROP VIEW IF EXISTS `view_product_type_bread_types`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_product_type_bread_types` AS SELECT 
 1 AS `product_type`,
 1 AS `bread_type`,
 1 AS `name`,
 1 AS `price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_products_infostring`
--

DROP TABLE IF EXISTS `view_products_infostring`;
/*!50001 DROP VIEW IF EXISTS `view_products_infostring`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_products_infostring` AS SELECT 
 1 AS `id`,
 1 AS `infoString`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_valid_addresses`
--

DROP TABLE IF EXISTS `view_valid_addresses`;
/*!50001 DROP VIEW IF EXISTS `view_valid_addresses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_valid_addresses` AS SELECT 
 1 AS `id`,
 1 AS `address`,
 1 AS `type`,
 1 AS `alias`,
 1 AS `user`,
 1 AS `deleted`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_valid_cards`
--

DROP TABLE IF EXISTS `view_valid_cards`;
/*!50001 DROP VIEW IF EXISTS `view_valid_cards`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_valid_cards` AS SELECT 
 1 AS `id`,
 1 AS `owner`,
 1 AS `expiration`,
 1 AS `code`,
 1 AS `cvc`,
 1 AS `card_type`,
 1 AS `user`,
 1 AS `deleted`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `without_ing`
--

DROP TABLE IF EXISTS `without_ing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `without_ing` (
  `prod_instance` int(11) NOT NULL,
  `ingredient` int(11) NOT NULL,
  PRIMARY KEY (`prod_instance`,`ingredient`),
  KEY `ingredient_fk_of_without_ing_idx` (`ingredient`),
  CONSTRAINT `ingredient_fk_of_without_ing` FOREIGN KEY (`ingredient`) REFERENCES `ingredient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `prod_instance_fk_of_without_ing` FOREIGN KEY (`prod_instance`) REFERENCES `prod_instance` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `without_ing`
--

LOCK TABLES `without_ing` WRITE;
/*!40000 ALTER TABLE `without_ing` DISABLE KEYS */;
INSERT INTO `without_ing` VALUES (2,3),(376,2),(379,3),(380,1),(380,2),(380,3),(415,7),(415,8),(437,8),(437,9),(528,6),(528,7),(528,8),(530,6),(562,15),(589,11),(603,9),(614,15),(684,15),(688,15),(688,16),(734,15),(735,8),(738,15),(739,17),(771,15),(772,17),(793,15),(794,17),(800,17),(805,15),(832,17),(837,15),(873,17),(878,15),(880,17),(885,15),(925,1),(925,2),(925,3),(934,17),(939,15);
/*!40000 ALTER TABLE `without_ing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tecweb2017'
--

--
-- Dumping routines for database 'tecweb2017'
--
/*!50003 DROP PROCEDURE IF EXISTS `create_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `create_user`(IN p_email VARCHAR(45), 
IN p_password VARCHAR(128), 
IN p_first_name VARCHAR(45), 
IN p_last_name VARCHAR(45))
BEGIN
	DECLARE cart_id INT(11);
   
    DECLARE EXIT HANDLER FOR 1062 
		DELETE FROM product_list WHERE id = cart_id;
        SELECT 0 as result;
        
	INSERT INTO product_list() VALUES();
	SET cart_id = LAST_INSERT_ID();

	INSERT INTO user (email, password, first_name, last_name, cart) 
    VALUES(p_email, p_password, p_first_name, p_last_name, cart_id);
    
	SELECT 1 as result;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_all_ingredients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `get_all_ingredients`(
IN sorting VARCHAR(45),
IN start_index INT,
IN page_size INT
)
BEGIN
    SELECT id, name, price from ingredient ORDER BY sorting LIMIT start_index, page_size;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_most_bought` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `get_most_bought`(
IN page_number INT
)
BEGIN
    DECLARE page_size INT;
    DECLARE rows_offset INT;
    SET page_size = (SELECT value FROM tecweb2017.settings WHERE name = "page-size-of-rows");
    SET page_number = page_number - 1;
    SET rows_offset = page_number * page_size;
    
    SELECT DISTINCT view_product_list.*  FROM tecweb2017.view_product_list
    WHERE view_product_list.product_type_id <> 100
    ORDER BY view_product_list.buy_count DESC , view_product_list.name ASC
    LIMIT rows_offset,page_size;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ingredients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `ingredients`(IN prod_instance_id INT(11))
BEGIN
	SELECT ing_in.*, IF(ing_out.id is NULL, TRUE, FALSE) as exist FROM

	(SELECT ingredient.* FROM prod_instance 
	INNER JOIN prod_ing ON prod_ing.product = prod_instance.product
	INNER JOIN ingredient ON ingredient.id = prod_ing.ingredient
	WHERE prod_instance.id = prod_instance_id) as ing_in 

	LEFT JOIN

	(SELECT ingredient.* FROM prod_instance 
	INNER JOIN without_ing ON without_ing.prod_instance = prod_instance.id
	INNER JOIN ingredient ON ingredient.id = without_ing.ingredient
	WHERE prod_instance.id = prod_instance_id) as ing_out 

	ON ing_in.id = ing_out.id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pull_products` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `pull_products`(
IN p_search_key VARCHAR(45),
IN in_array VARCHAR(45),
IN page_number INT
)
BEGIN
	DECLARE p_search_key_like VARCHAR(45);
    DECLARE in_array_conc VARCHAR(45);
	DECLARE page_size INT;
    DECLARE rows_offset INT;
    
    SET p_search_key_like = CONCAT('%', p_search_key , '%');
    SET in_array_conc = CONCAT('(', in_array,')');
    SET page_size = (SELECT value FROM tecweb2017.settings WHERE name = "page-size-of-rows");
    SET page_number = page_number - 1;
    SET rows_offset = page_number * page_size;
    
    SELECT DISTINCT view_product_list.*  FROM tecweb2017.view_product_list LEFT OUTER JOIN tecweb2017.prod_ing 
	ON prod_ing.product = view_product_list.id LEFT OUTER JOIN tecweb2017.ingredient
	ON ingredient.id = prod_ing.ingredient
	WHERE FIND_IN_SET(view_product_list.product_type_id, in_array) > 0 AND ( view_product_list.name LIKE p_search_key_like OR ingredient.name LIKE p_search_key_like
    OR view_product_list.product_type LIKE p_search_key_like )
    ORDER BY view_product_list.name ASC 
    LIMIT rows_offset,page_size;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `pull_products_regex` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `pull_products_regex`(
IN p_search_regex VARCHAR(2048),
IN in_array VARCHAR(45),
IN page_number INT
)
BEGIN
    DECLARE in_array_conc VARCHAR(45);
	DECLARE page_size INT;
    DECLARE rows_offset INT;
    
    SET in_array_conc = CONCAT('(', in_array,')');
    SET page_size = (SELECT value FROM tecweb2017.settings WHERE name = "page-size-of-rows");
    SET page_number = page_number - 1;
    SET rows_offset = page_number * page_size;
    
    SELECT DISTINCT view_product_list.*  FROM tecweb2017.view_product_list LEFT OUTER JOIN view_products_infostring
    ON view_product_list.id = view_products_infostring.id
	WHERE FIND_IN_SET(view_product_list.product_type_id, in_array) > 0
    AND ( view_products_infostring.infoString REGEXP p_search_regex )
    ORDER BY view_product_list.name ASC 
    LIMIT rows_offset,page_size;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_products` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `search_products`(
IN p_search_key VARCHAR(45),
IN page_number INT
)
BEGIN
	DECLARE p_search_key_like VARCHAR(45);
	DECLARE page_size INT;
    DECLARE rows_offset INT;
    
    SET p_search_key_like = CONCAT('%', p_search_key , '%');
    SET page_size = (SELECT value FROM tecweb2017.settings WHERE name = "page-size-of-rows");
    SET page_number = page_number - 1;
    SET rows_offset = page_number * page_size;
    
    SELECT DISTINCT view_product_list.*  FROM tecweb2017.view_product_list LEFT OUTER JOIN tecweb2017.prod_ing 
	ON prod_ing.product = view_product_list.id LEFT OUTER JOIN tecweb2017.ingredient
	ON ingredient.id = prod_ing.ingredient
	WHERE view_product_list.name LIKE p_search_key_like OR ingredient.name LIKE p_search_key_like
    OR view_product_list.product_type LIKE p_search_key_like
    ORDER BY view_product_list.name ASC 
    LIMIT rows_offset,page_size;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `search_products_regex` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`tecweb`@`%` PROCEDURE `search_products_regex`(
IN p_search_regex VARCHAR(2048),
IN page_number INT
)
BEGIN
	DECLARE p_search_key_like VARCHAR(45);
	DECLARE page_size INT;
    DECLARE rows_offset INT;
    
    SET page_size = (SELECT value FROM tecweb2017.settings WHERE name = "page-size-of-rows");
    SET page_number = page_number - 1;
    SET rows_offset = page_number * page_size;
    
    SELECT DISTINCT view_product_list.*  FROM tecweb2017.view_product_list LEFT OUTER JOIN view_products_infostring
    ON view_product_list.id = view_products_infostring.id
	WHERE view_products_infostring.infoString REGEXP p_search_regex
    ORDER BY view_product_list.name ASC 
    LIMIT rows_offset,page_size;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `view_cart_product_list`
--

/*!50001 DROP VIEW IF EXISTS `view_cart_product_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_cart_product_list` AS select `prod_instance`.`id` AS `instance_id`,`prod_instance`.`product` AS `product_id`,`prod_instance`.`quantity` AS `quantity`,`product`.`name` AS `name`,`product`.`price` AS `price`,`product`.`prep_time` AS `prep_time`,`prod_instance`.`product_list` AS `product_list`,`product_type`.`name` AS `product_type`,`bread_type`.`id` AS `bread_type_id`,`bread_type`.`name` AS `bread_type`,`bread_type`.`price` AS `bread_price` from (((`prod_instance` join `product` on((`prod_instance`.`product` = `product`.`id`))) join `product_type` on((`product_type`.`id` = `product`.`product_type`))) left join `bread_type` on((`bread_type`.`id` = `prod_instance`.`bread_type`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_food_list`
--

/*!50001 DROP VIEW IF EXISTS `view_food_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_food_list` AS select `product`.`id` AS `id`,`product`.`name` AS `name`,`product`.`price` AS `price`,`product`.`prep_time` AS `prep_time`,`product_type`.`id` AS `product_type_id`,`product_type`.`name` AS `product_type` from (`product` join `product_type` on((`product`.`product_type` = `product_type`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_product_ingredients`
--

/*!50001 DROP VIEW IF EXISTS `view_product_ingredients`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_product_ingredients` AS select `ingredient`.`id` AS `id`,`ingredient`.`name` AS `name`,`ingredient`.`price` AS `price`,`product`.`id` AS `pid` from ((`product` join `prod_ing` on((`product`.`id` = `prod_ing`.`product`))) join `ingredient` on((`ingredient`.`id` = `prod_ing`.`ingredient`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_product_list`
--

/*!50001 DROP VIEW IF EXISTS `view_product_list`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_product_list` AS select `product`.`id` AS `id`,`product`.`name` AS `name`,`product`.`price` AS `price`,`product`.`prep_time` AS `prep_time`,`product`.`buy_count` AS `buy_count`,`product_type`.`id` AS `product_type_id`,`product_type`.`name` AS `product_type` from (`product` join `product_type` on((`product`.`product_type` = `product_type`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_product_type_bread_types`
--

/*!50001 DROP VIEW IF EXISTS `view_product_type_bread_types`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_product_type_bread_types` AS select `product_bread_type`.`product_type` AS `product_type`,`product_bread_type`.`bread_type` AS `bread_type`,`bread_type`.`name` AS `name`,`bread_type`.`price` AS `price` from (`product_bread_type` join `bread_type` on((`product_bread_type`.`bread_type` = `bread_type`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_products_infostring`
--

/*!50001 DROP VIEW IF EXISTS `view_products_infostring`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_products_infostring` AS select `view_product_list`.`id` AS `id`,concat(`view_product_list`.`product_type`,',',`view_product_list`.`name`,',',ifnull(group_concat(`ingredient`.`name` separator ','),'')) AS `infoString` from ((`view_product_list` left join `prod_ing` on((`prod_ing`.`product` = `view_product_list`.`id`))) left join `ingredient` on((`ingredient`.`id` = `prod_ing`.`ingredient`))) group by `view_product_list`.`id`,`view_product_list`.`name`,`view_product_list`.`product_type` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_valid_addresses`
--

/*!50001 DROP VIEW IF EXISTS `view_valid_addresses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_valid_addresses` AS select `address`.`id` AS `id`,`address`.`address` AS `address`,`address`.`type` AS `type`,`address`.`alias` AS `alias`,`address`.`user` AS `user`,`address`.`deleted` AS `deleted` from `address` where (`address`.`deleted` = 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_valid_cards`
--

/*!50001 DROP VIEW IF EXISTS `view_valid_cards`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`tecweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_valid_cards` AS select `card`.`id` AS `id`,`card`.`owner` AS `owner`,`card`.`expiration` AS `expiration`,`card`.`code` AS `code`,`card`.`cvc` AS `cvc`,`card`.`card_type` AS `card_type`,`card`.`user` AS `user`,`card`.`deleted` AS `deleted` from `card` where (`card`.`deleted` = 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-07 17:34:33
