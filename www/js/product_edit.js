$(document).ready(function() {
    $('#div-table-ing').jtable({
        title: 'Ingredienti',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: true, //Enable sorting
        defaultSorting: 'name ASC', //Set default sorting
        actions: {
            listAction: 'php/web_methods/ingredients/ingredientList.php',
            createAction: 'php/web_methods/ingredients/ingredientCreate.php',
            updateAction: 'php/web_methods/ingredients/ingredientUpdate.php',
            deleteAction: 'php/web_methods/ingredients/ingredientDelete.php'
        },
        fields: {
            id: {
                key: true,
                list: false
            },
            name: {
                title: 'Nome',
                width: '80%'
            },
            price: {
                title: 'Prezzo',
                width: '20%'
            },
        }
    });

    $('#div-table-bread').jtable({
        title: 'Tipi di farina',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: true, //Enable sorting
        defaultSorting: 'name ASC', //Set default sorting
        actions: {
            listAction: 'php/web_methods/breads/breadList.php',
            createAction: 'php/web_methods/breads/breadCreate.php',
            updateAction: 'php/web_methods/breads/breadUpdate.php',
            deleteAction: 'php/web_methods/breads/breadDelete.php'
        },
        fields: {
            id: {
                key: true,
                list: false
            },
            name: {
                title: 'Nome',
                width: '80%'
            },
            price: {
                title: 'Prezzo',
                width: '20%'
            },
        }
    });

    $('#div-table-ptype').jtable({
        title: 'Tipi di prodotto',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: true, //Enable sorting
        defaultSorting: 'name ASC', //Set default sorting
        actions: {
            listAction: 'php/web_methods/ptypes/ptypeList.php',
            createAction: 'php/web_methods/ptypes/ptypeCreate.php',
            updateAction: 'php/web_methods/ptypes/ptypeUpdate.php',
            deleteAction: 'php/web_methods/ptypes/ptypeDelete.php'
        },
        fields: {
            id: {
                title: 'Id',
                width: '8%',
                key: true,
                create: true/*,
                list: false*/
            },
            //subtable
            breadTypes: {
                title: '',
                width: '2%',
                sorting: false,
                edit: false,
                create: false,
                display: function (pTypeData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="js/jtable/list_metro.png" title="Edit bread types" alt="Edit bread types" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#div-table-ptype').jtable('openChildTable',
                                $img.closest('tr'), //Parent row
                                {
                                title: pTypeData.record.name + ' - Tipi di farina',
                                paging: true, //Enable paging
                                pageSize: 10, //Set page size (default: 10)
                                sorting: true, //Enable sorting
                                defaultSorting: 'name ASC', //Set default sorting
                                actions: {
                                    listAction: 'php/web_methods/breads_of_ptypes/breads_of_ptypeList.php?ptypeId=' + pTypeData.record.id,
                                    deleteAction: 'php/web_methods/breads_of_ptypes/breads_of_ptypeDelete.php?ptypeId=' + pTypeData.record.id,
                                    createAction: 'php/web_methods/breads_of_ptypes/breads_of_ptypeCreate.php'
                                },
                                fields: {
                                    ptypeId: {
                                        type: 'hidden',
                                        defaultValue: pTypeData.record.id,
                                        delete: true
                                    },
                                    id: {
                                        title: 'Tipo farina',
                                        key: true,
                                        create: true,
                                        edit: false,
                                        list: false,
                                        options: function(data) {
                                            data.clearCache();
                                            return 'php/web_methods/breads_of_ptypes/breads_of_ptypeGetOptions.php?ptypeId=' + pTypeData.record.id
                                        }
                                        
                                    },
                                    name: {
                                        title: 'Nome',
                                        width: '100%',
                                        edit: false,
                                        create: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            name: {
                title: 'Nome',
                width: '90%'
            }
        }
    });


    $('#div-table-prod').jtable({
        title: 'Prodotti',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: true, //Enable sorting
        defaultSorting: 'name ASC', //Set default sorting
        actions: {
            listAction: 'php/web_methods/products/productList.php',
            createAction: 'php/web_methods/products/productCreate.php',
            updateAction: 'php/web_methods/products/productUpdate.php',
            deleteAction: 'php/web_methods/products/productDelete.php'
        },
        fields: {
            id: {
                title: 'Id',
                width: '8%',
                key: true,
                create: false,
                list: false
            },
            //subtable
            breadTypes: {
                title: '',
                width: '2%',
                sorting: false,
                edit: false,
                create: false,
                display: function (productData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="js/jtable/list_metro.png" title="Edit ingredients" alt="Edit ingredients" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#div-table-prod').jtable('openChildTable',
                                $img.closest('tr'), //Parent row
                                {
                                title: productData.record.name + ' - Ingredienti',
                                paging: true, //Enable paging
                                pageSize: 10, //Set page size (default: 10)
                                sorting: true, //Enable sorting
                                defaultSorting: 'name ASC', //Set default sorting
                                actions: {
                                    listAction: 'php/web_methods/ings_of_prods/ings_of_prodList.php?productId=' + productData.record.id,
                                    deleteAction: 'php/web_methods/ings_of_prods/ings_of_prodDelete.php?productId=' + productData.record.id,
                                    createAction: 'php/web_methods/ings_of_prods/ings_of_prodCreate.php'
                                },
                                fields: {
                                    productId: {
                                        type: 'hidden',
                                        defaultValue: productData.record.id,
                                        delete: true
                                    },
                                    id: {
                                        title: 'Ingrediente',
                                        key: true,
                                        create: true,
                                        edit: false,
                                        list: false,
                                        options: function(data) {
                                            data.clearCache();
                                            return 'php/web_methods/ings_of_prods/ings_of_prodGetOptions.php?productId=' + productData.record.id
                                        }
                                        
                                    },
                                    name: {
                                        title: 'Nome',
                                        width: '100%',
                                        edit: false,
                                        create: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            name: {
                title: 'Nome',
                width: '38%',
                create: true,
                edit: true
            },
            price: {
                title: 'Prezzo',
                width: '10%',
                create: true,
                edit: true
            },
            prep_time: {
                title: 'Tempo prep',
                width: '10%',
                create: true,
                edit: true
            },
            buy_count: {
                title: 'Comprato',
                width: '10%',
                create: false,
                edit: false
            },
            created_by: {
                title: 'Creato da',
                width: '15%',
                create: false,
                edit: false
            },
            product_type: {
                title: 'Tipo prodotto',
                width: '15%',
                create: true,
                edit: true,
                options: 'php/web_methods/products/productGetOptionsPtypes.php'
            }
        }
    });
    
    $('#div-table-ing').jtable('load');
    $('#div-table-bread').jtable('load');
    $('#div-table-ptype').jtable('load');
    $('#div-table-prod').jtable('load');
});        					
