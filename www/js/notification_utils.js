$(document).ready(function(){
  $('#popover_notification').popover({
	html:true,
	//trigger: 'focus'
  });   
  $('#popover_notification').on('click', function (e) {
	dismiss_notification();
	popover_show = !popover_show;
  });
  
 
  
  
  
  setInterval(check_notification, 5000);
  update_lbl_cart_count_ajax();
  update_lbl_notification_count_ajax();
});


function check_notification() {
	console.log("checking for notifications...");
	update_lbl_notification_count_ajax();
}

last_notification_count = "0";
popover_show = false;
function update_lbl_notification_count_ajax() {
	$.ajax({
        type: "POST",
        url: "php/web_methods/get_notification_count.php",
        success: function (r) {
			console.log("update_lbl_notification_count_ajax success");
			if(r == "0") {
				set_no_notification_content();
			} else {
				$("#lbl_notify_count").html(r);
				if(last_notification_count != r) {
					last_notification_count = r;
					update_popover_notification_content_ajax();
				}
			}
        },
        failure: function (r) {
            console.log("update_lbl_notification_count_ajax failed");
        }
    });
}
function update_popover_notification_content_ajax() {
	$.ajax({
        type: "POST",
        url: "php/web_methods/get_notification_content.php",
        success: function (r) {
			console.log("update_popover_notification_content_ajax success");
			$("#popover_notification").attr('data-content', r);
			if(popover_show) {
				$("#popover_notification").popover('show');
			}
        },
        failure: function (r) {
            console.log("update_popover_notification_content_ajax failed");
        }
    });
}

function dismiss_notification() {
	$.ajax({
        type: "POST",
        url: "php/web_methods/set_notification_disposed.php",
        success: function (r) {
			console.log("dismiss_notification success");
        },
        failure: function (r) {
            console.log("dismiss_notification failed");
        }
    });
	set_no_notification_content();
	
}
function set_no_notification_content() {
	$("#lbl_notify_count").html("");
	$("#popover_notification").attr('data-content','Nessuna nuova notifica</br><a href="notifies.php">Guarda tutte le notifiche</a>');
	last_notification_count = "0";
}


function update_lbl_cart_count_ajax(){
	 $.ajax({
        type: "POST",
        url: "php/web_methods/get_cart_products_count.php",
        success: function (r) {
            update_lbl_cart_count(r);
			console.log("update_lbl_cart_count_ajax success");
        },
        failure: function (r) {
            console.log("update_lbl_cart_count_ajax failed");
        }
    });
}
function update_lbl_cart_count(count){
	if(count == 0) {
		$("#lbl_cart_count").text("");
	}
	else {
		$("#lbl_cart_count").text(count);
	}
}