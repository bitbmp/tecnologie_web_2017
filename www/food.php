<!DOCTYPE html>
<html lang="it">

<head>
	<?php 
		require "php/page_parts.php"; 
		echo_head("Cibo");
	?>
	<script src="js/personalize_ingredients.js"></script>
	<script src="js/add_to_cart.js"></script>
	<script src="js/pull_products.js"></script>
	<script src="js/update_price_bread_type.js"></script>
	<script>
		current_filter = "1,2,3";
		default_show_dynamic_area = true;
	</script>
</head>

<body>

	<?php echo_menu("food.php"); ?>
	<main>
	<div class="container">
	
		<?php echo_search_bar(); ?>

		<div class="hide-during-search">
			<?php echo_divider("Cibo"); ?>	
		</div>

		<div class="row">
			<div class="col-12">
				<div class="btn-group btn-group-lg btn-block mb-2" role="group" aria-label="...">
					<button type="button" class="btn btn-secondary col-lg-12 btn-filter" data-filter="1,2,3">Tutto</button>
				</div>
			</div>
			<div class="col-12">
				<div class="btn-group btn-group-lg btn-block mb-3" role="group" aria-label="...">
					<button type="button" class="btn btn-secondary col-lg-4 btn-filter" data-filter="1">Piadine</button>
					<button type="button" class="btn btn-secondary col-lg-4 btn-filter" data-filter="2">Crescioni</button>
					<button type="button" class="btn btn-secondary col-lg-4 btn-filter" data-filter="3">Rotoli</button>
				</div>
			</div>
		</div>

		<div class="show-during-search" style="display:none;">
			<?php echo_divider("Ricerca"); ?>
		</div>
		<div class="product-list-dynamic-area"></div>

		<div id="loading" class="loader-ellips" style="display:none;">
			<span class="loader-ellips__dot"></span>
			<span class="loader-ellips__dot"></span>
			<span class="loader-ellips__dot"></span>
			<span class="loader-ellips__dot"></span>
		</div>

	</div>
	</main>

	<?php echo_footer("Food"); ?>



</body>

</html>