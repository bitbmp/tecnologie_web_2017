var win;
var page_number = 0;
var end_reached = false;

$(document).ready(function() {
    win = $(window);
    loadNewPage();
	// Each time the user scrolls
	win.scroll(scrollTrigger);
});

function scrollTrigger(event) {
    // End of the document reached?
    if (($(document).height() - win.height() == win.scrollTop()) && !end_reached) {
        loadNewPage();
    }
}

function loadNewPage() {
    $('#loading').show();
    page_number++;
    var obj_data = {page_number : page_number};
    $.ajax({
        type: "POST",
        url: "php/web_methods/infinite_scroll.php",
        data: obj_data,
        success: function(html){
            if (html != "") {
                $('#product-list-area').append(html);
                $(".btn-personalize_ing").off("click").click(personalizeButtonClick);
                $(".btn-add_to_cart").off("click").click(addToCartButtonClick);        
            } else {
                end_reached = true;
                win.off("scroll");
            }
            $('#loading').hide();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("get new page failed");
            console.log(textStatus, errorThrown);
        },
    });
}