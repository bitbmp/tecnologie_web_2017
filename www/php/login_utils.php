<?php
require_once "mysql_conn.php";

class user {
    public $id;
	public $email;
	public $first_name;
	public $last_name;
	public $cart_id;
	public $default_card;
	public $default_address;
	public $usual;
	public $admin;
	
	function __construct($id, $email, $first_name, $last_name, $cart_id, $default_card, $default_address, $usual, $admin) {
       $this->id = $id;
	   $this->email = $email;
	   $this->first_name = $first_name;
	   $this->last_name = $last_name;
	   $this->cart_id = $cart_id;
	   
	   $this->default_card = $default_card;
	   $this->default_address = $default_address;
	   $this->usual = $usual;
	   $this->admin = $admin;
   }
}

function is_user_logged_in() {
	if(session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	return isset($_SESSION["user"]);
}

function is_user_admin() {
	return is_user_logged_in() && get_user()->admin;
}


function is_new_user_email_valid($email) {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT count(id) FROM user WHERE email=?");
	$stmt->bind_param("s", $email);
	$stmt->execute();
	$stmt->bind_result($count);
	$stmt->fetch();
	$stmt->close();
	$conn->close();	
	
	return ($count == 0) && filter_var($email, FILTER_VALIDATE_EMAIL);
}

function insert_new_user($email, $password, $firsr_name, $last_name) {
	$conn = connect_db();
	$stmt = $conn->prepare("CALL create_user(?,?,?,?)");
	$stmt->bind_param("ssss", $email, hash_password($password), $firsr_name, $last_name);
	$stmt->execute();
	$res;
	do {
		if($result = $stmt->get_result()) {
			$res = mysqli_fetch_all($result);
		}
	} while ($stmt->more_results() && $stmt->next_result());
	$stmt->close();
	$conn->close();	
	return $res[0][0] == 1;
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
function hash_password($password) {
	$salt = "kus937HdR82Jdkasdufkasjfdbnasoijdyoi";
	return hash('sha512', $password.$salt);
}

function is_attempts_ok() {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT count(id) FROM login_attempt WHERE `from` = ? AND success = 0 AND date > DATE_SUB(NOW(), INTERVAL 10 MINUTE)");
	$stmt->bind_param("s", getRealIpAddr());
	$stmt->execute();
	$stmt->bind_result($count);
	$stmt->fetch();
	$stmt->close();
	
	return $count < 5;
}
function check_login($email, $password) {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT id,admin,email,first_name,last_name,cart,IF(password = ?, TRUE, FALSE) as password_ok, default_card, default_address, usual FROM user WHERE email=?");
	$stmt->bind_param("ss", hash_password($password), $email);
	$stmt->execute();
	$stmt->bind_result($id, $admin, $email, $first_name, $last_name, $cart_id, $password_ok, $default_card, $default_address, $usual);
	$stmt->fetch();
	$stmt->close();
	
	$stmt = $conn->prepare("INSERT INTO login_attempt (user, success, date, `from`) VALUES(?,?,now(),?)");
	$stmt->bind_param("iis", $id, $password_ok, getRealIpAddr());
	$stmt->execute();
	$stmt->close();
	
	$conn->close();	
	$user = new user($id, $email, $first_name, $last_name, $cart_id, $default_card, $default_address, $usual, $admin);
	if($password_ok) return $user;
	return null;
}
function reload_user() {
	user_login(get_user_by_id(get_user()->id));
}
function get_user_by_id($id) {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT id, admin, email, first_name, last_name, cart, default_card, default_address, usual FROM user WHERE id=?");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->bind_result($id, $admin, $email, $first_name, $last_name, $cart_id, $default_card, $default_address, $usual);
	$stmt->fetch();
	$stmt->close();
	$conn->close();	
	
	return new user($id, $email, $first_name, $last_name, $cart_id, $default_card, $default_address, $usual, $admin);
}

function user_login($user) {
	if(session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	$_SESSION["user"] = $user;
}

function user_logout() {
	if(session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	unset($_SESSION["user"]);
}

function get_user() {
	if(!is_user_logged_in()) {
		return NULL;
	}
	return $_SESSION["user"];
}


?>