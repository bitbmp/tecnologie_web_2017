<!DOCTYPE html>
<html lang="it">

<head>
	<?php 
        require_once "php/login_utils.php";
        require_once "php/orders_utils.php";        
		if($_SERVER['REQUEST_METHOD'] != 'GET' || !isset($_GET["id"]) || !is_user_logged_in() || (!user_has_order(get_user()->id, $_GET["id"]) && !is_user_admin())) {
			header("Location: login.php");
		} else {
            $order_id = $_GET["id"];
        }
        require_once "php/page_parts.php";         
		
		echo_head("Dettagli prodotto");
	?>
	<script src="js/order_details.js"></script>
	<script src="js/add_to_cart.js"></script>
</head>

<body>

	<?php echo_menu("order_details.php"); ?>
	<main>
		<div class="container">
			<div class="order-details-section">
				<?php echo_divider("Dettagli dell'ordine"); ?>
				<?php echo_order_details_section($order_id); ?>

				<?php echo_divider("Destinazione"); ?>
				<?php echo_order_destination_section($order_id); ?>

				<?php echo_divider("Pagamento"); ?>
				<?php echo_order_payment_section($order_id); ?>

				<?php echo_divider("Riepilogo"); ?>
				<?php echo_order_summary_section($order_id); ?>
				
				<?php echo_divider("Stato"); ?>
				<?php echo_order_state_section($order_id); 
				
				$state = get_order_last_state($order_id);

				if(is_user_admin() /*&& !user_has_order(get_user()->id, $_GET["id"])*/) {			
					if($state != 3 && $state != 4) { 
						echo_divider("Amministrazione"); ?>
						<div id="button-row" class="row">
						<div class="col-6">
							<button type="button" id="btn-reject" class="btn btn-danger">Annulla&nbsp;&nbsp;<div class="far fa-trash-alt"></div></button>
						</div>
						<div id="button-col"class="col-6 text-right">
								<?php if($state == 0) { ?>
									<button type="button" id="btn-accept" class="btn btn-success">Accetta&nbsp;&nbsp;<div class="far fa-check-circle"></div></button>
								<?php } else if ($state == 1) { ?>
									<button type="button" id="btn-deliver" class="btn btn-success">Pronto&nbsp;&nbsp;<div class="far fa-check-circle"></div></button>
								<?php } else if ($state == 2) { ?>
									<button type="button" id="btn-finish" class="btn btn-success">Consegnato&nbsp;&nbsp;<div class="far fa-check-circle"></div></button>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				<?php }  /*else {*/?>
					<div class="product-separator mt-4 mb-0"></div>
					<div class="row mt-0 text-left">
						<div class="col-6">
							<p class="mb-0" ><small>Ti è piaciuto?</small></p>
						</div>
						<div class="col-6 text-right">
							<button type="button" class="btn btn-link m-0 p-0" id="buy-again"><small>Compra di nuovo</small></button>
						</div>
					</div>
					<div class="row mt-0 text-right">
						<div class="col-12">
							<p class="add_to_cart_message" style="display:none;">
								<small class="mt-0 text-left add_to_cart_text"></small>
							</p>
						</div>
					</div>
					<div class="row mt-0 text-left">
						<div class="col-6">
							<p class="mb-0" ><small>Non puoi farne a meno?</small></p>
						</div>
						<div class="col-6 text-right">
							<button type="button" class="btn btn-link m-0 p-0" id="usual-order"><small>Impostalo come 'Il solito'</small></button>
						</div>
					</div>
					<div class="row mt-0 text-right">
						<div class="col-12">
							<p class="text-success" id="message-container" style="display:none;"> <small id="message-output"></small> </p>
						</div>
					</div>	
				<?php /* } */ ?>
			</div>
		</div>
	</main>

	<?php echo_footer("order_details.php"); ?>


</body>

</html>