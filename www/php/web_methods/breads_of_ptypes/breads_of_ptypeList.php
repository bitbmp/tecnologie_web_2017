<?php	
    require_once "../../product_edit.php";
    require_once "../../login_utils.php";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(is_user_logged_in() && is_user_admin())	{
            if(is_user_logged_in() && is_user_admin())	{
                if(isset($_GET["jtSorting"]) && isset($_GET["jtStartIndex"]) && isset($_GET["jtPageSize"]) && isset($_GET["ptypeId"])) {
                    echo json_encode(list_breads_of_ptype($_GET["jtSorting"], $_GET["jtStartIndex"], $_GET["jtPageSize"], $_GET["ptypeId"])); 
                } else {
                    //echo json_encode(list_breads_of_ptype()); 
                    /*
                    ob_start();
                    var_dump($_REQUEST);
                    var_dump(isset($_REQUEST["jtSorting"]));
                    var_dump(isset($_REQUEST["jtStartIndex"]));
                    var_dump(isset($_REQUEST["jtPageSize"]));
                    
                    */
                    echo json_encode(array('Result' => 'ERROR', 'Message' => "Invalid parameters"));
                }
            } else {
                echo "Devi essere loggato per accedere.";
            }
        } else {
            echo "Devi essere loggato per accedere.";
        }	
    } else {
        echo "Questa non è una richiesta post.";
    }
?>