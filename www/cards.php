<!DOCTYPE html>
<html lang="it">

<head>
    <?php
        require_once "php/page_parts.php";
        require_once "php/login_utils.php"; 
		
		if(!is_user_logged_in()) {
			header("Location: index.php");
        }
         
		echo echo_head("Le mie carte");
	?>
    <script src="js/input.js"></script>
    <script src="js/cards.js"></script>
</head>
<body>
    <?php echo_menu("cards.php"); ?>	
    <main>
        <div class="container">
            <?php echo_divider("Le mie carte"); ?>
            <div id="cards-list-area">
            </div>
            <section id="add_card" style="padding-top:10px">
                <h2 class="sr-only">Add card area</h2>
                <form method="post">
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary btn-toggle-newcard">Aggiungi metodo di pagamento&nbsp;&nbsp;&nbsp;&nbsp;<span class="far fa-credit-card"></span></button>
                        </div>
                    </div>
                </form>
                <form method="post" id="add-card-form" style="display:none;">
                    <div class="row">
                        <div class="col-12 text-center">
                            <label class="sr-only" for="txbOwner">Intestatario</label>
                            <div class="input-group mb-2 mt-2">
                                <div class="input-group-addon"><div class="fas fa-user"></div></div>
                                <input type="text" class="form-control" id="txbOwner" name="txbOwner" placeholder="Intestatario carta" required>
                            </div>
                            <label class="sr-only" for="txbCartNumber">Numero carta</label>
                            <div class="input-group mb-2 mt-2">
                                <label class="sr-only" for="card_type">Tipo di carta</label>
                                <select class="form-control" id="card_type">
                                </select>
                                <input type="number" class="form-control" id="txbCartNumber" name="txbCartNumber" placeholder="Numero carta" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <label class="sr-only" for="month">Mese</label>
                            <select class="form-control" id="month">
                            </select>
                        </div>
                        <div class="col-4">
                            <label class="sr-only" for="year">Anno</label>
                            <select class="form-control" id="year">
                            </select>
                        </div>
                        <div class="col-4">
                            <label class="sr-only" for="txbCvv">Cvv</label>
                            <input type="number" class="form-control" id="txbCvv" name="txbCvv" placeholder="CVV" required>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary btn-add_newcard">Aggiungi la tua carta</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </main>
    <?php echo_footer("Cards"); ?>
</body>
</html>
