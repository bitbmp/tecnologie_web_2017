<!DOCTYPE html>
<html lang="it">

<head>
    <?php
        require_once "php/page_parts.php";
        require_once "php/login_utils.php"; 
		require_once "php/orders_utils.php";
		
		if(!is_user_admin()) {
			header("Location: index.php");
        }
         
		echo echo_head("Gestione ordini");
	?>
	<script src="js/admin_orders.js"></script>
</head>
<body>
    <?php echo_menu("admin_orders.php"); ?>	
    <main>
        <div class="container">
            <?php echo_divider("Da confermare"); ?>
			<div id="orders-to-accept"><?php echo get_all_orders_divs(0); ?></div>
			
			<?php echo_divider("In preparazione"); ?>
			<div id="orders-to-prepare"><?php echo get_all_orders_divs(1); ?></div>
			
			<?php echo_divider("In consegna / Da ritirare"); ?>
			<div id="orders-to-deliver"><?php echo get_all_orders_divs(2); ?></div>
			
			<?php echo_divider("Consegnati / Ritirati"); ?>
			<div id="orders-delivered"><?php echo get_all_orders_divs(3); ?></div>
			
			<?php echo_divider("Annullati"); ?>
			<div id="orders-cancelled"><?php echo get_all_orders_divs(4); ?></div>
			
        </div>
    </main>
    <?php echo_footer("admin_orders.php"); ?>
</body>
</html>
