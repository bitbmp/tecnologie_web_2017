<!DOCTYPE html>
<html lang="it">

<head>
    <?php
        require_once "php/page_parts.php";
        require_once "php/login_utils.php"; 
		
		if(!is_user_logged_in()) {
			header("Location: index.php");
        }
         
		echo echo_head("I miei ordini");
	?>
        <script src="js/orders.js"></script>
</head>
<body>
    <?php echo_menu("orders.php"); ?>	
    <main>
        <div class="container">
            <?php echo_divider("I miei ordini"); ?>
            <div id="orders-list-area">
            </div>
        </div>
    </main>
    <?php echo_footer("Orders"); ?>
</body>
</html>
