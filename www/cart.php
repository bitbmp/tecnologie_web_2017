<!DOCTYPE html>
<html lang="it">

<head>
	<?php 
		require_once "php/login_utils.php";
		if(!is_user_logged_in()) {
			header("Location: login.php");
		}
		
		require_once "php/page_parts.php"; 
		require_once "php/cart_utils.php";
		require_once "php/cards_utils.php";
		require_once "php/address_utils.php";
		
		echo_head("Carrello");
	if(get_cart_products_count() > 0) { ?>
	<script src="js/cart_utils.js"></script>
	<?php } ?>
</head>

<body>

	<?php echo_menu("cart.php"); ?>
	<main>
		<div class="container">
			<?php echo_divider("Carrello"); ?>
			
			
			
			<?php 
				if(get_cart_products_count() > 0) { ?>
				<div class="row">
					<div class="col-7 pr-0 ">
						<p class="font-weight-bold">Articolo</p>
					</div>
					<div class="col-2 pl-0 pr-0">
						<p class="font-weight-bold">Prezzo</p>
					</div>
					<div class="col-3 pl-2 text-center">
						<p class="font-weight-bold">Q.tà</p>
					</div>
					
				</div>
					<?php 
					echo_all_cart_prods_divs();
				} else { ?>
					<h6>Il tuo carrello è vuoto, aggiungi qualcosa per procedere all'ordine</h6>
					
					</div>
					</main>
					<?php echo_footer("cart.php"); ?>
					</body>
					</html>
				<?php exit(); }?>
				
			<div class="row mt-0">
				<div class="col-7 pr-0"><p class="font-italic">Totale provvisorio</p></div>
				<div class="col-3 pl-0 pr-0 font-weight-bold"><p class="mb-0 lblCartTotalProv"></p></div>
			</div>
			<?php echo_divider("Spedizione"); ?>
			
			<div class="row mt-0 mb-0">
				<div class="col-12 text-right">
					<small>
					<a href="addresses.php">Aggiungi indirizzo</a>
					</small>
				</div>
			</div>
			<div class="row mt-0">
				<div class="col-6 d-flex align-items-center">
					<input id="rdbSpedizione0" type="radio" name="rdbSpedizione" value="0" <?php if(get_user()->default_address != null) { ?> checked="checked" <?php } ?> <?php if(get_addresses_count() <= 0) {echo "disabled"; }?> > 
					<label class="mb-0 ml-1" for="rdbSpedizione0">Indirizzo</label>
				</div>
				<div class="col-6">
					<?php echo_cmb_user_addresses(); ?>
				</div>
			</div>
			<div class="row mt-1">
				<div class="col-6 d-flex align-items-center">	
					<input id="rdbSpedizione1" type="radio" name="rdbSpedizione" value="1" <?php if(get_user()->default_address == null) { ?> checked="checked" <?php } ?>>
					<label class="mb-0 ml-1" for="rdbSpedizione1">Punto di ritiro</label>
				</div>
				<div class="col-6">
					<?php echo_cmb_point_addresses(); ?>
				</div>
			</div>
			<!--<div class="row mt-2">
				<div class="col-6 d-flex align-items-center">
					<input id="rdbSpedizione2" type="radio" name="rdbSpedizione" disabled value="2">
					<label class="mb-0 ml-1" for="rdbSpedizione2">La mia posizione</label>
				</div>
				<div class="col-6">
					<small>Non disponibile</small>
				</div>
			</div>-->
			<div class="row mt-2 mb-4">
				<div class="col-6 d-flex align-items-center">
					<label class="mb-0 font-weight-bold" for="txbOrario">Orario desiderato</label>
				</div>
				<div class="col-6">
					<input class="form-control" type="datetime-local" id="txbOrario">
				</div>
			</div>
			
			<?php echo_divider("Pagamento"); ?>
			
			<div class="row mt-0 mb-0">
				<div class="col-12 text-right">
					<small>
					<a href="cards.php">Aggiungi carta</a>
					</small>
				</div>
			</div>
			<div class="row mt-0">
				<div class="col-6 d-flex align-items-center">
					<input id="rdbPagamento0" type="radio" name="rdbPagamento" value="0" <?php if(get_user()->default_card != null) { ?> checked="checked" <?php } ?> <?php if(get_cards_count() <= 0) {echo "disabled"; }?>>
					<label class="mb-0 ml-1" for="rdbPagamento0">Carta di credito</label>
				</div>
				<div class="col-6">
					<?php echo_cmb_cards(); ?>
				</div>
			</div>
			<div class="row mt-1">
				<div class="col-6 d-flex align-items-center">	
					<input id="rdbPagamento1" type="radio" name="rdbPagamento" value="1" <?php if(get_user()->default_card == null) { ?> checked="checked" <?php } ?> >
					<label class="mb-0 ml-1" for="rdbPagamento1">Alla consegna</label>
				</div>
			</div>
			
			<?php echo_divider("Riepilogo ordine"); ?>
			
			
			<div class="row">
				<div class="col-6"><small class="mb-0 font-italic">Spedizione</small></div>
				<div class="col-6 text-right"><small class="lblAddress mb-0"></small></div>
			</div>
			<div class="row">
				<div class="col-6"><small class="mb-0 font-italic">Pagamento</small></div>
				<div class="col-6 text-right"><small class="lblPay mb-0"></small></div>
			</div>
			<div class="row">
				<div class="col-6"><small class="mb-0 font-italic">Orario desiderato</small></div>
				<div class="col-6 text-right"><small id="lblDateTime" class="mb-0 font-weight-bold"></small></div>
			</div>
			
			<div class="product-separator mt-0"></div>
			
			<div class="row">
				<div class="col-6"><small class="mb-0 font-italic">Totale ordinato</small></div>
				<div class="col-6 text-right"><small class="lblCartTotalProv mb-0"></small></div>
			</div>
			<div class="row">
				<div class="col-6"><small class="mb-0 font-italic">Spedizione</small></div>
				<div class="col-6 text-right"><small id="lblDeliverPrice" class="mb-0"></small></div>
			</div>
			
			<div class="product-separator mt-0 mb-0"></div>
			<div class="row mb-4 mt-0">
				<div class="col-6 "><p class="mb-0 font-italic font-weight-bold">Totale</p></div>
				<div class="col-6 text-right font-weight-bold"><p id="lblCartTotal" class="mb-0"></p></div>
			</div>
			<div class="row mb-0">
				<div class="col-12"><small id="lblError" class="text-danger mb-0"></small></div>
			</div>
			<div class="form-group">
				<input type="button" onclick="checkout()" class="btn btn-primary btn-lg btn-block" value="Conferma">
			</div>
		</div>
	</main>
	<?php echo_footer("cart.php"); ?>


</body>

</html>