<?php
	if(isset($_POST["prod_instance_id"])) {
		
		require_once "../mysql_conn.php";
		require_once "../login_utils.php";
		$conn = connect_db();
		$stmt = $conn->prepare("DELETE FROM prod_instance WHERE id = ? AND product_list = ?;");
		$stmt->bind_param("ii", $_POST["prod_instance_id"], get_user()->cart_id);  
		$stmt->execute();
		$stmt->close();
		$conn->close();
		
		echo true;
	} else {
		echo false;
	}
?>