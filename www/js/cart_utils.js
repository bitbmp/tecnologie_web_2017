$(document).ready(function() {
	hide_last_separator();
	update_lbl_cart_count_ajax();
	
	update_lbl_cart_total_ajax();
	

	$("#txbOrario").val(roundMinutes(new Date()).toJSON().slice(0,19));
	
	$("input[type=radio]").click( function(){
		update_order_recap();
	});
	$("select").change( function(){
		update_order_recap();
	});
	$("#txbOrario").change( function(){
		update_order_recap();
	});
	update_order_recap();
});


function roundMinutes(date) {
    date.setHours(date.getHours() + Math.round(date.getMinutes()/60));
    date.setMinutes(0);
	date.setSeconds(0);

    return date;
}


function update_order_recap() {
	update_lbl_cart_total(last_price);
	deliver_type = $('input[name=rdbSpedizione]:checked').val();
	if(deliver_type == 0) {
		$(".lblAddress").text($('#cmbAddress').find(":selected").val());
	}
	if(deliver_type == 1) {
		$(".lblAddress").text($('#cmbPoint').find(":selected").val());
	}
	
	pay_type = $('input[name=rdbPagamento]:checked').val();
	if(pay_type == 0) {
		$(".lblPay").text($('#cmbCard').find(":selected").val());
	} else {
		$(".lblPay").text("Alla consegna");
	}
	
	$("#lblDateTime").text($('#txbOrario').val().replace("T"," "));
}

function checkout() {
	
	deliver_type = $('input[name=rdbSpedizione]:checked').val();
	pay_type = $('input[name=rdbPagamento]:checked').val();
	date = $("#txbOrario").val();
	
	if(date == "") {
		$("#lblError").text("Inserisci un orario desiderato valido");
		return;
	}
	
	if(deliver_type == 0) {
		address = $('#cmbAddress').find(":selected").attr("id");
	}
	if(deliver_type == 1) {
		address = $('#cmbPoint').find(":selected").attr("id");
	}
	card = "-1";
	if(pay_type == 0) {
		card = $('#cmbCard').find(":selected").attr("id");
	}
	
	card = card.replace('card_','');
	address = address.replace('address_','');
	/*
	alert("deliver type: " + deliver_type+ "\n" + 
			"pay type: " + pay_type+ "\n" + 
			"card: " + card+ "\n" +
			"address: " + address+ "\n" + 
			"date: " + date);*/
			
	 $.ajax({
        type: "POST",
        url: "php/web_methods/checkout.php",
		data: 'deliver_type=' + deliver_type + '&pay_type=' + pay_type + '&card=' + card + '&address=' + address+ '&date=' + date,
        success: function (r) {
			if($.isNumeric(r)) {
				window.location.replace("order_details.php?id="+r);
				console.log("checkout success, order: "+r);
			}
			else {
				$("#lblError").text(r);
			}
        },
        failure: function (r) {
            console.log("checkout failed");
        }
    });
}

function hide_last_separator() {
	//$("section.product_in_cart div.product-separator").last().hide();
	$("section.product_in_cart div.product-separator").last().css('opacity', '0.65').css('margin-bottom','0').css('background-color', 'black'/*'#007BFF'*/);
	
	$("section.product_in_cart div.product-separator").last().removeClass("mb-2").removeClass("mt-2");
	$("section.product_in_cart div.product-separator").last().addClass("mb-0").addClass("mt-2");
}


function get_deliver_price() {
	val = $('input[name=rdbSpedizione]:checked').val();	
	if(val === '0') return 2.00; //address
	if(val === '1') return 0.00; //point
	if(val === '2') return 2.00; //my position
	return 0.00;
}
last_price = 0;
function update_lbl_cart_total(price){
	last_price = price;
	$("#lblCartTotal").text(( Number(price) + Number(get_deliver_price())).toFixed(2) + " €");
	$(".lblCartTotalProv").text(price + " €");
	$("#lblDeliverPrice").text(get_deliver_price().toFixed(2) + " €");
}

function get_cart_count() {
	value = 0;
	$.ajax({
        type: "POST",
		async: false,
        url: "php/web_methods/get_cart_products_count.php",
        success: function (r) {
            value = r;
			console.log("get_cart_count success");
        },
        failure: function (r) {
            console.log("get_cart_count failed");
        }
    });
	return value;
}


function update_lbl_cart_total_ajax(){
	 $.ajax({
        type: "POST",
        url: "php/web_methods/get_cart_total_price.php",
        success: function (r) {
            update_lbl_cart_total(r);
			console.log("update_lbl_cart_total_ajax success");
        },
        failure: function (r) {
            console.log("update_lbl_cart_total_ajax failed");
        }
    });
}

function remove_from_cart(prod_instance_id) {
	$.ajax({
        type: "POST",
        url: "php/web_methods/remove_from_cart.php",
        data: 'prod_instance_id=' + prod_instance_id + '',
        success: function (r) {
            $("#product_in_cart_"+prod_instance_id).fadeOut("fast", function(){ 
				$(this).remove(); 
				hide_last_separator();
				console.log("#product_in_cart_"+prod_instance_id+" removed");
			});
			
			$.ajax({
				type: "POST",
				url: "php/web_methods/get_cart_products_count.php",
				success: function (r) {
					update_lbl_cart_count(r);
					if(r == 0) {
						window.location.replace("cart.php");
					}
					console.log("get_cart_count success");
				},
				failure: function (r) {
					console.log("get_cart_count failed");
				}
			});
			update_lbl_cart_total_ajax();
			
			console.log("remove_from_cart success");
        },
        failure: function (r) {
            console.log("remove_from_cart failed");
        }
    });
}
function change_quantity_cart(prod_instance_id) {
	if($("#cart_quantity_"+prod_instance_id).val() <= 0) {
		$("#cart_quantity_"+prod_instance_id).val($("#tmp_cart_quantity_"+prod_instance_id).val())
		return;
	}
	$.ajax({
        type: "POST",
        url: "php/web_methods/change_quantity_cart.php",
        data: 'prod_instance_id=' + prod_instance_id + '&quantity='+($("#cart_quantity_"+prod_instance_id).val()),
        success: function (r) {
			update_lbl_cart_count_ajax();
			update_lbl_cart_total_ajax();
			console.log("change_quantity_cart success");
			$("#tmp_cart_quantity_"+prod_instance_id).val($("#cart_quantity_"+prod_instance_id).val())
        },
        failure: function (r) {
            console.log("change_quantity_cart failed");
        }
    });
}