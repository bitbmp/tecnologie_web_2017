<?php
require_once "../cards_utils.php";

//now is post call and can add only drinks.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        echo json_encode(get_cards_type());
    } else {
        echo json_encode("Devi accedere per effettuare acquisti.");
    }
} else {
    echo json_encode("Questa non è una richiesta post.");
}

?>