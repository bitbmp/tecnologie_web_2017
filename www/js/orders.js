
$(document).ready(function() {
    //Orders list section
    getAllOrders();    
});

function getAllOrders(request) {
    console.log("get all orders triggered");
    $('#orders-list-area').html("");
    $.ajax({
        type: "POST",
        url: "php/web_methods/get_all_orders.php",
        success: function(html){
            if(html != "Devi essere loggato per accedere." && html != "Questa non è una richiesta post.") {
                $('#orders-list-area').append(html);
                //Need to re-attach event to buttons             
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("get_all_orders failed");
            console.log(textStatus, errorThrown);
        },
    });
}