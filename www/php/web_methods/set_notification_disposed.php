<?php
	require_once "../mysql_conn.php";
	require_once "../login_utils.php";
	
	
	if(!is_user_logged_in()) {
		exit(1);
	}
	
	$conn = connect_db();
	$result = $conn->query("UPDATE notifications SET disposed = 1 WHERE user = ".get_user()->id." AND disposed = 0");
	$result->execute();
	$conn->close();
?>