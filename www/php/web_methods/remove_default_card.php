<?php
require_once "../cards_utils.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        if(isset($_POST["card_id"])) {
            $user_id = $_SESSION["user"]->id;            
            $card_id = $_POST["card_id"];
            if(remove_user_default_card($user_id, $card_id)) {
                reload_user();
                echo "Carta rimossa da default.";
            } else {
                echo "Impossibile rimuove la carta da default.";
            }     
        } else {
            echo "Parametri mancanti.";
        }
    } else {
        echo "Devi accedere per effettuare queste operazioni.";
    }
} else {
    echo "Questa non è una richiesta post.";
}

?>