<!DOCTYPE html>
<html lang="it">

<head>
    <?php 
		require_once "php/page_parts.php"; 
		require_once "php/login_utils.php"; 
		
		if(is_user_logged_in()) {
			header("Location: index.php");
		}
		
		echo_head("Registrati");
	?>
</head>
<body>

<?php echo_menu("register.php"); ?>	
<main>
	<div class="container">

		<?php echo_logo_image(); ?>
		
		<?php 
			$email = "";
			$password = "";
			$first_name = "";
			$last_name = "";
			$email_valid = TRUE;
			$password_valid = TRUE;
			$name_valid = TRUE;
			
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			
				if(isset($_POST["txbEmail"])) {
					$email = $_POST["txbEmail"];
					$email_valid = is_new_user_email_valid($email);
				} else {
					$email_valid = FALSE;
				}
				
				if(isset($_POST["txbPassword"])) {
					$password = $_POST["txbPassword"];
					$password_valid = strlen($password) >= 8;
				} else {
					$password_valid = FALSE;
				}
				
				if(isset($_POST["txbFirstName"]) && isset($_POST["txbLastName"])) {
					$first_name = $_POST["txbFirstName"];
					$last_name = $_POST["txbLastName"];
				} else {
					$name_valid = FALSE;
				}
				
				
				
				
				if($email_valid && $password_valid && $name_valid) {
					if(insert_new_user($email, $password, $first_name, $last_name)){
						?>
						<form id="successForm" action="login.php" method="post">
						<input type="hidden" name="txbEmail" value="<?php echo $email; ?>">
						<input type="hidden" name="fromRegister">
						</form>
						
						<script type="text/javascript">
							setTimeout(redirect, 4500);
							function redirect() {
								document.getElementById('successForm').submit();
							}
						</script>

						<div style="padding-top:10px">
							<hr class="hr-text" data-content="Registrazione effettuata con successo" >
							<p class="text-center">A breve sarai rediretto nella pagina di acesso</p>
						</div>
						
						</div>
						</body>
						</html>
						<?php
						exit();
					} else {
						echo "Error (insert_new_user failed)";
						exit(1);
					}
				}
			}
		?>
		<form class="form-group" method="post">
			
			<?php echo_divider("Registrazione"); ?>
			
			<!-- Email already taken error message -->
			<?php if(!$email_valid) { ?>
			<div>
				<small id="emailHelp" class="text-danger">
				<p class="mb-0 text-right">Email non valida o già usata. <a href="login.php" >Volevi accedere?</a></p>
				</small>
			</div>
			<?php } ?>
			
			
			<label class="sr-only" for="txbEmail">Email</label>
			<div class="input-group mb-2 mt-0">
				<div class="input-group-addon"><div class="fa fa-envelope fa-fw"></div></div>
				<input type="email" class="form-control <?php echo !$email_valid?"is-invalid":""; ?>" name="txbEmail" id="txbEmail" placeholder="Email" value="<?php echo $email; ?>" required>
			</div>
			
			<!-- Password already taken error message -->
			
			<div id="lblNoPassword" class="text-right mb-0" style="display: none;">
				<small  class="text-danger">La password deve avere almeno 8 caratteri</small>
			</div>
			
			
			<label class="sr-only" for="txbPasswordTmp">Passowrd</label>
			<div class="input-group mb-2">
				<div class="input-group-addon"><div class="fa fa-lock fa-fw"></div></div>
				<input type="password" class="form-control <?php echo !$password_valid?"is-invalid":""; ?>" name="txbPasswordTmp" id="txbPasswordTmp" placeholder="Password" required>
			</div>
			
			
			<!-- Password already taken error message -->
			<?php if(!$name_valid) { ?>
			<div>
				<small id="nameHelp" class="text-danger">
				<p class="mb-0 text-right">Inserisci nome e cognome</p>
				</small>
			</div>
			<?php } ?>
			
			<label class="sr-only" for="txbFirstName">Nome</label>
			<label class="sr-only" for="txbLastName">Cognome</label>
			<div class="input-group mb-4">
				<div class="input-group-addon"><div class="fa fa-user-circle fa-fw"></div></div>
				<input type="text" class="form-control <?php echo !$name_valid?"is-invalid":""; ?>" id="txbFirstName" name="txbFirstName" placeholder="Nome" value="<?php echo $first_name; ?>" required>
				<input type="text" class="ml-1 form-control <?php echo !$name_valid?"is-invalid":""; ?>" id="txbLastName" name="txbLastName" placeholder="Cognome" value="<?php echo $last_name; ?>" required>
			</div>
			
			<input type="button" onclick="formhash(this.form, this.form.txbPasswordTmp);" class="btn btn-primary btn-lg btn-block login-button" value="Registrati"> 

		</form>
		
		
		<div class="form-group" style="padding-top:10px">
			<?php echo_divider("Disponi già di un account?"); ?>
			<button type="button" class="btn btn-primary btn-lg btn-block login-button" onclick="window.location.href='login.php'">Accedi</button>
		</div>
	</div>
<main>
</body>
</html>
