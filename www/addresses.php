<!DOCTYPE html>
<html lang="it">

<head>
    <?php
        require_once "php/page_parts.php";
        require_once "php/login_utils.php"; 
		
		if(!is_user_logged_in()) {
			header("Location: index.php");
        }
         
		echo echo_head("I miei indirizzi");
	?>
    <script src="js/input.js"></script>
    <script src="js/addresses.js"></script>
</head>
<body>
    <?php echo_menu("addresses.php"); ?>	

    <main>
        <div class="container">
            <?php echo_divider("I miei indirizzi"); ?>
            <div id="addresses-list-area">
            </div>
            <section id="add_address" style="padding-top:10px">
                <h1 class="sr-only">Add address area</h1>
                <form method="post">
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary btn-toggle-newaddress">Aggiungi nuovo indirizzo&nbsp;&nbsp;&nbsp;&nbsp;<span class="far fa-address-card"></span></button>
                        </div>
                    </div>
                </form>
                <form method="post" id="add-address-form" style="display:none;">
                    <div class="row">
                        <div class="col-8 text-center">
                            <label class="sr-only" for="txbRoad">Via</label>
                            <input type="text" class="form-control mb-2 mt-2" id="txbRoad" name="txbRoad" placeholder="Via" autocomplete="on" required>
                        </div>
                        <div class="col-4 text-center" style="padding-left: 0 !important;">
                            <label class="sr-only" for="txbNumber">Numero</label>
                            <input type="number" class="form-control mb-2 mt-2" id="txbNumber" name="txbNumber" placeholder="N°"  autocomplete="on" required>
                        </div>
                        <div class="col-12 text-center">       
                            <label class="sr-only" for="txbCAP">CAP</label>
                            <input type="number" class="form-control" id="txbCAP" name="txbCAP" placeholder="CAP" autocomplete="on" required>
                        </div>
                        <div class="col-8 text-center">
                            <label class="sr-only" for="txbCity">Città</label>
                            <input type="text" class="form-control mb-2 mt-2" id="txbCity" name="txbCity" placeholder="Città" autocomplete="on" required>
                        </div>
                        <div class="col-4 text-center" style="padding-left: 0 !important;">
                            <label class="sr-only" for="txbProvince">Provincia</label>
                            <input type="text" class="form-control mb-2 mt-2" id="txbProvince" name="txbProvince" placeholder="Provincia" autocomplete="on" required>
                        </div>
                        <div class="col-12 text-center">       
                            <label class="sr-only" for="txbAlias">Alias</label>
                            <input type="text" class="form-control mb-2" id="txbAlias" name="txbAlias" placeholder="Etichetta" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary btn-add_newaddress">Aggiungi il tuo indirizzo</button>
                        </div>
                    </div>
                </form>
            </section>
        </div>
    </main>
    <?php echo_footer("Addresses"); ?>
</body>
</html>
