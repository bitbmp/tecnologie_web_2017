<?php
require_once "mysql_conn.php";
require_once "login_utils.php";
require_once "address_utils.php";
require_once "cards_utils.php";



function user_has_order($user_id, $order_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT count(*) as c FROM `order` WHERE id = ? and user = ?");
    $stmt->bind_param("ii", $order_id, $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $count = $row["c"];
    } 
    $conn->close();
    $stmt->close();
	return $count > 0;
}

function get_user_id_from_order($order_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT user FROM `order` WHERE id = ?");
    $stmt->bind_param("i", $order_id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $user = $row["user"];
    } else {
        $user = false;
    }
    $conn->close();
    $stmt->close();
	return $user;
}

function get_order_info($order_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT `order`.id as order_id, `order`.user as user, creation_date, desired_date, pay_state, address.id as address_id, address.address as address, address.type as address_type, alias, card.id as card_id, code, card_type.type as card_type, product_list, `order`.product_list as product_list, total_price, deliver_price FROM `order` INNER JOIN address ON `order`.address = address.id LEFT JOIN card ON `order`.card = card.id LEFT JOIN card_type ON card.card_type = card_type.id LEFT JOIN product_list ON product_list = product_list.id WHERE `order`.id = ?");
    $stmt->bind_param("i", $order_id);
    if($stmt->execute()) {
        $result = $stmt->get_result();        
    } else {
        $result = false;
    }
	$stmt->close();
	$conn->close();
	return $result;
}

function get_order_states($order_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT * FROM `order_state` WHERE `order` = ? ORDER BY date ASC");
    $stmt->bind_param("i", $order_id);
    if($stmt->execute()) {
        $result = $stmt->get_result();        
    } else {
        $result = false;
    }
	$stmt->close();
	$conn->close();
	return $result;
}

function get_order_last_state($order_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT state FROM `order_state` WHERE `order` = ? ORDER BY date DESC, id DESC LIMIT 1");
    $stmt->bind_param("i", $order_id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $state = $row["state"];
    } else {
        $state = false;
    }
	$stmt->close();
	$conn->close();
	return $state;
}

function is_order_annullable($order_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT * FROM `order_state` WHERE `order` = ? ORDER BY date DESC");
    $stmt->bind_param("i", $order_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $num_rows = 0;
    while($state_info = $result->fetch_assoc()) {
        $num_rows++;
    }
    if(($num_rows == 1) && (order_string($state_info["state"]) == "Ordinato")) {
        $is_annulable = true;
    } else {
        $is_annulable = false;
    }
	$stmt->close();
	$conn->close();
	return $is_annulable;
}

function cancel_user_order($user_id, $order_id) {
    if(user_has_order($user_id, $order_id) && is_order_annullable($order_id) && insert_new_order_state($order_id, 4 /* annullato */)) {
        $conn = connect_db();
        $stmt = $conn->prepare("SELECT * FROM `order_state` WHERE `order` = ? ORDER BY date DESC LIMIT 1");
        $stmt->bind_param("i", $order_id);
        if($stmt->execute()) {
            $result = $stmt->get_result();        
        } else {
            $result = false;
        }
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

//state da 0 a 4 Ordinato, In preparazione, In consegna, Consegnato, Annullato
function change_order_status($order_id, $order_status) {
    if(is_user_admin() && insert_new_order_state($order_id, $order_status)) {
        $conn = connect_db();
        $stmt = $conn->prepare("SELECT * FROM `order_state` WHERE `order` = ? ORDER BY date DESC LIMIT 1");
        $stmt->bind_param("i", $order_id);
        if($stmt->execute()) {
            $result = $stmt->get_result();        
        } else {
            $result = false;
        }
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

function echo_order_details_section($order_id) {
    if(($result = get_order_info($order_id)) && ($order_info = $result->fetch_assoc())) { ?>
        <div id="order-details">
            <div class="row">
                <div class="col-7 pr-0 font-italic">
                    <h4 class="mb-2">Codice ordine</h4>
                    <p class="mb-0">Ricezione ordine</p>
                    <p class="mb-0">Consegna concordata</p>
                </div>
				<div class="col-5 pl-0 text-right">
					<h4 class="mb-2"><?php echo $order_info["order_id"] ?></h4>
                    <p class="mb-0"><?php echo formatted_full_date($order_info["creation_date"]) ?></p>
                    <p class="mb-0"><?php echo formatted_full_date($order_info["desired_date"]) ?></p>
				</div>
            </div>
    </div>
    <?php 
	if(is_user_admin()) { 
		$user = get_user_by_id($order_info["user"]);
	?>
			<div class="product-separator mb-2 mt-2"></div>
			<div class="row">
                <div class="col-4 pr-0 font-italic">
                    <p class="mb-0">Nome</p>
                    <p class="mb-0">Cognome</p>
                    <p class="mb-0">Email</p>
                </div>
				<div class="col-8 pl-0 text-right">
					<p class="mb-0"><?php echo $user->first_name; ?></p>
                    <p class="mb-0"><?php echo $user->last_name; ?></p>
                    <p class="mb-0"><?php echo $user->email; ?></p>
				</div>
            </div>
	<?php }
	
	} else {
        echo "Impossibile reperire le informazioni relative all'ordine richiesto.";
    }
}

function echo_order_destination_section($order_id) {
    if(($result = get_order_info($order_id)) && ($order_info = $result->fetch_assoc())) { ?>
        <div id="destination-details">
            <div class="row">
                <div class="col-6 pr-0 font-italic">
                    <p class="mb-2">Tipo</p>
                    <p class="mb-0">Indirizzo</p>
                </div>
				<div class="col-6 pl-0 text-right">
                    <p class="mb-2"><?php echo address_string($order_info["address_type"]) ?></p>
                    <p class="mb-0"><?php echo $order_info["alias"]  ?></p>
					<small class="mb-0" ><?php echo $order_info["address"]  ?></small>
                </div>
            </div>
    </div>
    <?php } else {
        echo "Impossibile reperire le informazioni relative alla destinazione dell'ordine richiesto.";
    }
}

function echo_order_payment_section($order_id) {
    if(($result = get_order_info($order_id)) && ($order_info = $result->fetch_assoc())) { ?>
        <div id="payment-details">
            <div class="row">
                <div  class="col-4 pr-0 font-italic">
                    <p class="mb-2"><?php echo $order_info["card_id"] == null ? "Modalità" : "Modalità"?> </p>
                    <?php if($order_info["card_id"] != null) { ?>
                        <p class="mb-0">Carta</p> <?php
                    } ?>
                    <p class="mb-0">Stato</p>
                </div>
				<div class="col-8 pl-0 text-right">
                    <p class="mb-2"><?php echo $order_info["card_id"] == null ? "Pagamento alla consegna" : "Carta di debito"?> </p>
                    <?php if($order_info["card_id"] != null) { ?>
                        <p class="mb-0"><?php echo_printable_card_string($order_info["card_type"], $order_info["code"]) ?></p> <?php
                    } ?>
                    <p class="mb-0"><?php echo pay_string($order_info["pay_state"]) ?></p>
                </div>
            </div>
        </div>
    <?php } else {
        echo "Impossibile reperire le informazioni relative al pagamento dell'ordine richiesto.";
    }
}

function echo_order_summary_section($order_id) {
    if(($result = get_order_info($order_id)) && ($order_info = $result->fetch_assoc())) {
		require_once "page_parts/cart.php";
		?>
		<div class="row">
				<div class="col-7">
					<p class="font-weight-bold">Articolo</p>
				</div>
				<div class="col-2 text-right">
					<p class="font-weight-bold">Q.tà</p>
				</div>
				<div class="col-3 text-right">
					<p class="font-weight-bold">Prezzo</p>
				</div>
				
		</div>
		
		<?php 
		echo get_all_cart_prods_divs($order_info["product_list"], true);
		 echo_divider("Totale");
		?>
		
		<div class="row mt-2">
			<div class="col-6"><small class="mb-0 font-italic">Totale ordinato</small></div>
			<div class="col-6 text-right"><small><p class="lblCartTotalProv mb-0"><?php echo number_format($order_info["total_price"] - $order_info["deliver_price"], 2, '.', ''); ?> €</p></small></div>
		</div>
		<div class="row">
			<div class="col-6"><small class="mb-0 font-italic">Spedizione</small></div>
			<div class="col-6 text-right"><small><p id="lblDeliverPrice" class="mb-0"><?php echo $order_info["deliver_price"]; ?> €</p></small></div>
		</div>
		
		<div class="product-separator mt-0 mb-0"></div>
		<div class="row mb-4 mt-0">
			<div class="col-6 "><p class="mb-0 font-italic font-weight-bold">Totale</p></div>
			<div class="col-6 text-right font-weight-bold"><p id="lblCartTotal" class="mb-0"><?php echo $order_info["total_price"]; ?> €</p></div>
		</div>
		
		<?php
    } else {
        echo "Impossibile reperire le informazioni relative al pagamento dell'ordine richiesto.";
    }
}

function echo_order_state_section($order_id) {
    if(($result = get_order_info($order_id)) && ($order_info = $result->fetch_assoc()) && ($states = get_order_states($order_info["order_id"]))) { ?>
        <div id="state-details">
            <form method="post">
                <input type="hidden" name="order_id"  class="input-hidden-order-id" value="<?php echo $order_info["order_id"]; ?>">
                <?php
                while ($states_info = $states->fetch_assoc()) {
                    echo_internal_order_state($order_id, $states_info);
                }
                ?>     
            </form>
            </div>
    <?php } else {
        echo "Impossibile reperire le informazioni relative agli stati dell'ordine richiesto.";
    }
}

function echo_internal_order_state($order_id, $states_info) {
    ?> <div class="row">
            <div class="col-5">
                <p class="mb-0"><?php echo formatted_full_date($states_info["date"]) ?></p>
            </div>
            <div class="col-5 pr-0">
                <p class="mb-0"><?php echo order_string($states_info["state"]) ?></p>
            </div>
            <?php if((order_string($states_info["state"]) == "Ordinato") && (is_order_annullable($order_id)) && (user_has_order(get_user()->id, $_GET["id"]))) { ?>
            <div class="col-2 pl-0">
                <button type="button" class="btn btn-secondary btn-cancel_order"><div class="far fa-trash-alt"></div>
            </div> <?php } ?>
        </div> <?php
}

function set_order_as_usual($user_id, $order_id) {
    if(user_has_order($user_id, $order_id) && ($result = get_order_info($order_id)) && ($order_info = $result->fetch_assoc())) {
        $conn = connect_db();
        $stmt = $conn->prepare("UPDATE user SET usual = ? WHERE id = ?");
        $stmt->bind_param("ii", $order_info["product_list"], $user_id);
        if($stmt->execute()) {
            $result = true;        
        } else {
            $result = false;
        }
        $stmt->close();
        $conn->close();
    } else {
        $result = false;
    }
    return $result;

}

//Functions used to populate page orders.php
function get_orders_sections($user_id) {
    $conn = connect_db();
    //LEFT OR INNER JOIN TO DECIDE
    $stmt = $conn->prepare("SELECT outOrder.id, state, creation_date, desired_date, total_price, date FROM `order` AS outOrder INNER JOIN order_state AS outOrder_state ON outOrder.id = outOrder_state.order where user = ? and date = ( SELECT MAX(date) FROM `order_state` AS inOrder WHERE inOrder.order = outOrder.id) ORDER BY creation_date DESC, outOrder_state.id DESC");
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$html = get_orders_sections_from_result($conn, $result);
	$conn->close();
	return $html;
}

function get_orders_sections_from_result($conn, $result) {
    if ($result->num_rows > 0) {
        ob_start();
        $i = 0;
		while ($myrow = $result->fetch_assoc()) {
            if($i != 0) {
				?><div class="product-separator"></div><?php
			}
            internal_echo_orders_section($conn, $myrow["id"], $myrow["state"], $myrow["creation_date"], $myrow["desired_date"], $myrow["total_price"]);
            $i++;
		}
		return ob_get_clean();
	} else {
		return "";
	}
}

function internal_echo_orders_section($conn, $order_id, $order_state, $creation_date, $desidered_date, $total_price) {
    ?>
    <section class="order-section">
        <form method="post">
            <input type="hidden" name="order_id"  class="input-hidden-order-id" value="<?php echo $order_id; ?>">
            <div class="row mb-0">
                <div class="col-12">
                    <h4><a class="mb-0 font-weight-bold" href="order_details.php?id=<?php echo $order_id?>"><?php echo "Order #" . $order_id ?></a></h4>
                </div>
                <div class="col-9">
                <?php
                    if($order_state == 3) { ?>
                            <p class="mb-0 font-weight-bold text-success"><?php echo "[" . order_string($order_state) . "] "?> <?php echo get_current_date($desidered_date) == date("Y-m-d") ? "Ordine delle " . hour_minute_from_date($desidered_date) : "Ordine del " . formatted_full_date($desidered_date);  ?></p>                        
                    <?php } else if ($order_state == 4){ ?>
                            <p class="mb-0 font-weight-bold text-danger"><?php echo "[" . order_string($order_state) . "] "?> <?php echo get_current_date($desidered_date) == date("Y-m-d") ? "Ordine delle " . hour_minute_from_date($desidered_date) : "Ordine del " . formatted_full_date($desidered_date);  ?></p>                    
                        <?php } else { ?>
                            <p class="mb-0 font-weight-bold"><?php echo "[" . order_string($order_state) . "] "?> <?php echo get_current_date($desidered_date) == date("Y-m-d") ? "Ordine delle " . hour_minute_from_date($desidered_date) : "Ordine del " . formatted_full_date($desidered_date);  ?></p>
                        <?php } ?>
                    <p class="mb-0"><?php echo "Ordinato il : " ?><span class="mb-0 order-date"><?php echo formatted_full_date($creation_date);?></span></p>
                </div>
                <div class="col-3 text-right">
                    <p class="mb-0 font-weight-bold"><?php echo $total_price . "€"; ?></p>
                </div>
            </div>
        </form>
    </section>
    <?php
}

//Function used to pupulate admin_orders
function get_all_orders($state_id) {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT order_state.order, order_state.state, desired_date, creation_date FROM 
							(SELECT `order`.id, max(date) as date FROM `order` 
							INNER JOIN order_state ON order_state.`order` = `order`.id
							GROUP BY `order`.id) as d 
							INNER JOIN order_state ON d.id = order_state.order 
							INNER JOIN `order` ON `order`.id = d.id
							AND d.date = order_state.date 
							AND creation_date >= CURRENT_DATE() AND creation_date < CURRENT_DATE() + INTERVAL 1 DAY
							WHERE state = ?
							ORDER BY desired_date DESC, creation_date DESC");
	$stmt->bind_param("i", $state_id);
	if($stmt->execute()) {
        $result = $stmt->get_result();        
    } else {
        $result = false;
    }
	$stmt->close();
	$conn->close();
	return $result;
}
function get_all_orders_divs($state_id) {
	?>
	<div class="row">
		<div class="col-2">
			<p class="font-weight-bold">#</p>
		</div>
		<div class="col-7">
			<p class="font-weight-bold">Data desiderata</p>
		</div>
		<div class="col-3 pl-0">
			<p class="font-weight-bold">Totale</p>
		</div>
	</div>
	<?php
	ob_start();
	$orders = get_all_orders($state_id);
	$i = 0;
	while($row = $orders->fetch_assoc()) {
		if($info = get_order_info($row["order"])->fetch_assoc()) {
			echo_order_admin_div_detail($info, $i);
			$i++;
		}
	}
	return ob_get_clean();
}
function echo_order_admin_div_detail($info, $i) {
	if($i != 0) { ?>
	<div class="product-separator mb-1 mt-1"></div>
	<?php } ?>
	
	<div class="row mb-0">
		<div class="col-2 pr-0">
			<a href="order_details.php?id=<?php echo $info["order_id"]; ?>">#<?php echo $info["order_id"]; ?></a>
		</div>
		<div class="col-7">
			<p class="mb-0"><?php echo $info["desired_date"]; ?></p>
		</div>
		<div class="col-3 pl-0">
			<p class="mb-0"><?php echo $info["total_price"]; ?> €</p>
		</div>
	</div>
	<!--
	<div class="row mt-0">
		<div class="col-2 pr-0">
			<button class="btn btn-danger">No</a>
		</div>
		<div class="col-2 pr-0">
			<button class="btn btn-success">Ok</a>
		</div>
		<div class="col-3 pr-0">
			<button class="btn btn-primary">Fatto</a>
		</div>
		<div class="col-4 pr-0">
			<button class="btn btn-primary">Consegnato</a>
		</div>
	</div>
	-->
	
	<?php
}

//Get order status string from code
function order_string($order_state) {
    switch ($order_state) {
        case 0:
            return "Ordinato";
            break;
        case 1:
            return "In preparazione";
            break;
        case 2:
            return "In consegna";
            break;
        case 3:
            return "Consegnato";
            break;
        case 4:
            return "Annullato";
            break;
        default:
            return "";
    }
}

function pay_string($pay_state) {
    switch ($pay_state) {        
        case 0:
            return "Non completato";
            break;
        case 1:
            return "Completato";
            break;
        default:
            return "";
    }
}

//Utility date manipulations functions
function get_current_date($date_str) {
    $date = strtotime($date_str);
    return date('Y-m-d', $date);
}

function hour_minute_from_date($order_date) {
    $date = strtotime($order_date);
    return date('H', $date) . ":" .date('m', $date);
}

function formatted_full_date($date_str) {
    $date = strtotime($date_str);
    return date("d/m/y H:i", $date);
}


//state da 0 a 4 Ordinato, In preparazione, In consegna, Consegnato, Annullato
function insert_new_order_state($order_id, $state) {
    $conn = connect_db();
    $stmt = $conn->prepare("INSERT INTO order_state (state, date, `order`) VALUES (?,now(),?)");
    $stmt->bind_param("ii", $state, $order_id);		
    if($stmt->execute()) {
        $result = true;			
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

?>