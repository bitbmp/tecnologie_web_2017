<?php

require_once "login_utils.php";
require_once "mysql_conn.php";
function get_cart_products_count() {
	if(!is_user_logged_in()) {
		return 0;
	}
	
	$conn = connect_db();
	$result = $conn->query("SELECT sum(quantity) as c FROM prod_instance WHERE product_list=".get_user()->cart_id);
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		$c = $row["c"];
	} else {
		$c = 0;
	}
	$conn->close();
	return $c;
}

function get_cart_total_price() {
	$conn = connect_db();
	$result = $conn->query("SELECT sum((price+IF(bread_price is NULL, 0, bread_price))*quantity) as total 
							FROM view_cart_product_list WHERE product_list =".get_user()->cart_id);

	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		$c = $row["total"];
	} else {
		$c = 0;
	}
	$conn->close();
	return $c;
}

function set_product_quantity($product_in_cart_id, $quantity) {
    $conn = connect_db();
	$stmt = $conn->prepare("UPDATE prod_instance SET quantity=? WHERE id=?");
	$stmt->bind_param("ii", $quantity, $product_in_cart_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
	$stmt->close();
	$conn->close();	
	return $result;
}

/* Functions for insert a product in cart. */

//Add a product to cart: if product isn't in cart already add a new prod_istance; otherwise increment his quantity.
function add_product_to_cart($product, $cart, $bread_type_id, $removed_ingredient_id_list, $quantity = 1) {
    if(checkInput($product, $bread_type_id, $removed_ingredient_id_list) == false) {
        return false;
    }
    $product_in_cart_id = product_in_cart($product, $cart, $bread_type_id, $removed_ingredient_id_list);
    if($product_in_cart_id == NULL) {
            $prod_instance_id = add_new_product_to_cart($product, $cart, $bread_type_id, $quantity);
            $result = $prod_instance_id ? true : false;
            if ($removed_ingredient_id_list != NULL) {
                foreach ($removed_ingredient_id_list as $id) {
                    add_removed_ingredient($id, $prod_instance_id);
                }
            }
    } else {
        $result = increment_product_quantity($product_in_cart_id, $quantity);
    }	
	return $result;
}

function remove_product_from_cart($product_in_cart_id) {
    $conn = connect_db();
	$stmt = $conn->prepare("DELETE FROM prod_instance WHERE id = ?");
	$stmt->bind_param("i", $product_in_cart_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
	$stmt->close();
	$conn->close();	
	return $result;
}

function product_in_cart($product, $cart, $bread_type_id, $removed_ingredient_id_list) {
    $conn = connect_db();
    if($bread_type_id == NULL) {
        $stmt = $conn->prepare("SELECT id FROM prod_instance where product = ? and product_list = ? and bread_type is null");
        $stmt->bind_param("ii", $product, $cart);
    } else {
        $stmt = $conn->prepare("SELECT id FROM prod_instance where product = ? and product_list = ? and bread_type = ?");
        $stmt->bind_param("iii", $product, $cart, $bread_type_id);
    }
    $stmt->execute();
    $result = $stmt->get_result();
    $ret = NULL;
	if(empty($removed_ingredient_id_list)) {
        while (($row = $result->fetch_assoc()) && $ret == NULL) {
            $ret = $row["id"];
            if(!empty(removed_ingredients_in_product($ret))) {
                $ret = NULL;
            }
        }
    } else { 
        while (($row = $result->fetch_assoc()) && $ret == NULL) {
            $ret = $row["id"];
            if($removed_ingredient_id_list != removed_ingredients_in_product($ret)) {
                $ret = NULL;
            }
        }
    }
    $stmt->close();
    $conn->close();
    return $ret;   
}

function removed_ingredients_in_product($product_instance_id) {
    $conn = connect_db();
	$stmt = $conn->prepare("SELECT ingredient FROM without_ing where prod_instance = ?");
	$stmt->bind_param("i", $product_instance_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $ingredients = array();
    while ($row = $result->fetch_assoc()) {
        array_push($ingredients, $row["ingredient"]);
    }
	$stmt->close();
	$conn->close();	
	return $ingredients;
}

//return product inserted id or false
function add_new_product_to_cart($product, $cart, $bread_type_id, $quantity) {
    $conn = connect_db();
    $stmt = $conn->prepare("INSERT INTO prod_instance (product, product_list, bread_type, quantity) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("iiii", $product, $cart, $bread_type_id, $quantity);
    if ($stmt->execute()) {
        $result = $stmt->insert_id;
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

function increment_product_quantity($product_in_cart_id, $quantity) {
    $conn = connect_db();
	$stmt = $conn->prepare("UPDATE prod_instance SET quantity=quantity + $quantity WHERE id=?");
	$stmt->bind_param("i", $product_in_cart_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
	$stmt->close();
	$conn->close();	
	return $result;
}

function add_removed_ingredient($ingredient_id, $prod_instance_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("INSERT INTO without_ing (prod_instance, ingredient) VALUES (?, ?)");
    $stmt->bind_param("ii", $prod_instance_id, $ingredient_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

/* Input Control functions BELOW HERE */
function checkInput($product, $bread_type_id, $removed_ingredient_id_list) {
    if(product_in_database($product)) {
        if(bread_type_in_product($bread_type_id, $product) && !array_diff($removed_ingredient_id_list, ingredients_in_product($product))) {            
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function product_in_database($product) {
    $conn = connect_db();
	$stmt = $conn->prepare("SELECT id FROM product where id = ?");
	$stmt->bind_param("i", $product);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $return = true;
    } else {
        $return = false;
    }
	$stmt->close();
	$conn->close();	
	return $return;
}

function ingredients_in_product($product) {
    $conn = connect_db();
	$stmt = $conn->prepare("SELECT ingredient FROM prod_ing where product = ?");
	$stmt->bind_param("i", $product);
    $stmt->execute();
    $result = $stmt->get_result();
    $ingredients = array();
    while ($row = $result->fetch_assoc()) {
        array_push($ingredients, $row["ingredient"]);
    }
	$stmt->close();
	$conn->close();	
	return $ingredients;
}

function bread_type_in_product($bread_type, $product) {
    if($bread_type == NULL) {
        return true;
    }
    $conn = connect_db();
	$stmt = $conn->prepare("SELECT product.id FROM product inner join product_type on product.product_type = product_type.id
                            inner join product_bread_type on product_type.id = product_bread_type.product_type
                            where product.id = ? and bread_type = ?");
	$stmt->bind_param("ii", $product, $bread_type);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $return = true;
    } else {
        $return = false;
    }
	$stmt->close();
	$conn->close();	
	return $return;
}

function get_cart_hash() {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT MD5(GROUP_CONCAT(MD5(concat(instance_id,quantity)) SEPARATOR ' ')) as cart_hash 
							FROM view_cart_product_list
							WHERE product_list = ?
							GROUP BY null");
	$stmt->bind_param("i", get_user()->cart_id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $return = $row["cart_hash"];
    } else {
        $return = "";
    }
	$stmt->close();
	$conn->close();	
	return $return;
}


?>