$(document).ready(function() {
    $('input.radio-bread-type').change(radioBreadTypeChanged);
});

function radioBreadTypeChanged(event) {
    var form = $(this).parents("form:first");
    setNewPrice(form);
}

function setNewPrice(form) {
    var priceSpan = form.find("span.product-price-value");
    var basePrice = parseFloat(priceSpan.attr("data-baseprice"));
    var additionalPrice = parseFloat(form.find('input[type=radio]:checked').attr("data-price"));
    var price = basePrice+additionalPrice;
    priceSpan.html(price.toFixed(2));
}

function setBasePrice(form) {
    var priceSpan = form.find("span.product-price-value");
    var basePrice = parseFloat(priceSpan.attr("data-baseprice"));
    priceSpan.html(basePrice.toFixed(2));
}