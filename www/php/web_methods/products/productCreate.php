<?php	
    require_once "../../product_edit.php";
    require_once "../../login_utils.php";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(is_user_logged_in() && is_user_admin())	{
            if(!empty($_POST["name"]) && !empty($_POST["price"]) && !empty($_POST["prep_time"]) && !empty($_POST["product_type"])) {
                echo json_encode(insert_product($_POST["name"], $_POST["price"], $_POST["prep_time"], get_user()->id, $_POST["product_type"])); 
            } else {
                echo json_encode(array('Result' => 'ERROR', 'Message' => 'Wrong parameters.')); 
            }
        } else {
            echo "Devi essere loggato per accedere.";
        }	
    } else {
        echo "Questa non è una richiesta post.";
    }
    

?>