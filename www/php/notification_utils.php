<?php
function add_notification($content, $user) {
	require_once "mysql_conn.php";
	$conn = connect_db();
	$stmt = $conn->prepare("INSERT INTO notifications (user, content) VALUES (?, ?)");
	$stmt->bind_param("is", $user, $content);
	$stmt->execute();
	$stmt->close();
	$conn->close();
}
function add_admin_notification($content) {
	require_once "mysql_conn.php";
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT * FROM user WHERE admin = 1");   
    $stmt->execute();
    $result = $stmt->get_result();
	while ($row = $result->fetch_assoc()) {
		add_notification($content, $row["id"]);
	}
	$stmt->close();
	$conn->close();	
}
?>