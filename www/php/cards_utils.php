<?php
require_once "mysql_conn.php";
require_once "login_utils.php";

function get_cards_count() {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT count(id) as c FROM view_valid_cards where user = ?");
    $stmt->bind_param("i", get_user()->id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $count = $row["c"];
    } 
	$conn->close();
	return $count;
}
function get_cards_type() {
    $conn = connect_db();
    $result = $conn->query("SELECT * FROM card_type");
    $values = array();
	while($row = $result->fetch_assoc()) {
        $values[] = $row;
	}
	$conn->close();
	return $values;
}

function add_card_to_user($user_id, $owner, $card_type, $card_number, $month, $year, $cvv) {
    $conn = connect_db();
    $expiration = new DateTime();
    $expiration->setDate($year, $month, 1);
    $stmt = $conn->prepare("INSERT INTO card (owner, expiration, code, cvc, card_type, user) VALUES (?, ?, ?, ?, ?, ?)");
    $stmt->bind_param("ssiiii", $owner, $expiration->format('Y-m-d'), $card_number, $cvv, $card_type, $user_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

function remove_card_from_user($user_id, $card_id) {
    //Remove user default card. Note this is ok because remove_user_default_card works only if the card to remove from default is exactly the default card.
    if(remove_user_default_card($user_id, $card_id)) {
        $conn = connect_db();
        $stmt = $conn->prepare("UPDATE card SET deleted = 1 WHERE user = ? AND id = ?");
        $stmt->bind_param("ii", $user_id, $card_id);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = false;
        }
        $stmt->close();
        $conn->close();
    } else {
        $result = false;
    }
    return $result;
}

function change_user_default_card($user_id, $card_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("UPDATE user SET default_card = ? WHERE id = ?");
    $stmt->bind_param("ii", $card_id, $user_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

function remove_user_default_card($user_id, $card_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("UPDATE user SET default_card = NULL WHERE id = ? AND default_card = ?");
    $stmt->bind_param("ii", $user_id, $card_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

function get_non_expired_users_cards($user_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT id FROM view_valid_cards WHERE user = ? AND expiration > CURDATE()");
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $values = array();
    while ($row = $result->fetch_assoc()) {
        array_push($values, $row["id"]);
    } 
	$stmt->close();
	$conn->close();
	return $values;
}

function get_cards_sections($user_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT view_valid_cards.id, owner, expiration, code, cvc, type, view_valid_cards.user, user.default_card FROM view_valid_cards INNER JOIN card_type ON view_valid_cards.card_type = card_type.id LEFT JOIN user ON view_valid_cards.user = user.id WHERE user = ?");
    $stmt->bind_param("i", $user_id);
    $stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$html = get_cards_sections_from_result($conn, $result);
	$conn->close();
	return $html;
}

function get_cards_sections_from_result($conn, $result) {
	if ($result->num_rows > 0) {
        ob_start();
		while ($myrow = $result->fetch_assoc()) {
			internal_echo_card_section($conn, $myrow["id"], $myrow["owner"], $myrow["expiration"], $myrow["code"], $myrow["cvc"], $myrow["type"], $myrow["id"] == $myrow["default_card"]);
            ?><div class="product-separator"></div><?php
		}
		return ob_get_clean();
	} else {
		return "";
	}
}

function internal_echo_card_section($conn, $card_id, $owner, $expiration, $card_number, $cvv, $card_type, $is_default) {
    ?>
    <section class="card-section">
        <form method="post">
            <input type="hidden" name="cart_id"  class="input-hidden-cart-id" value="<?php echo $card_id; ?>">
            <div class="row mb-0">
                <div class="col-9">
                    <p class="mb-0" style="font-weight: bold;"><?php echo $is_default ? "Carta di default" : ""; ?></p>
                    <h4 class="mb-0"><?php echo_printable_card_string($card_type, $card_number) ?></h4>
                    <p class="mb-0"> Intestatario: <?php echo $owner ?></p>
                    <?php
                        if(is_expired($expiration)) {
                            ?> <p class="mb-0" style="color:red;"> Scaduta: <?php echo_month_and_year($expiration) ?></p><?php                         
                        } else {
                            ?> <p class="mb-0"> Scadenza: <?php echo_month_and_year($expiration) ?></p><?php
                        }
                    ?>
                </div>
                <div class="col-3 text-right">
                    <button type="button" class="btn btn-danger btn-remove_card"><span class="far fa-trash-alt"></span>
                </div>
            </div>
            <?php if(!$is_default) {
                        if(!is_expired($expiration)) {?>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-link btn-set_default_card">Imposta a default&nbsp;&nbsp;&nbsp;<span class="fas fa-cogs"></span></div>
                        </div>
                        <?php } ?>
            </div> <?php } else {?>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-link btn-remove_default_card" style="color:red;">Rimuovi da default&nbsp;&nbsp;&nbsp;<span class="fas fa-cogs"></span></div>
                        </div> <?php
            }?>
        </form>
    </section>
    <?php
}

function echo_printable_card_string($card_type, $card_number) {
    echo "[" . $card_type . "] " . "termina con " . last_4_numbers($card_number);
}

function last_4_numbers($card_number) {
    return substr_replace($card_number,'', 0, 12);
}

function is_expired($expiration) {
    $now = new DateTime(date("Y-m-d"));
    $expire_dt = new DateTime($expiration);
    return $expire_dt < $now;
}

function echo_month_and_year($date) {
    list($year, $month, $day) = explode('-', $date);
    echo $month . "/" . $year;
}
    

?>