<?php
require_once "../cart_utils.php";

//now is post call and can add only drinks.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        if(isset($_POST["product_id"])) {
            $chart_id = $_SESSION["user"]->cart_id;            
            $product_id = $_POST["product_id"];
            if (isset($_POST["bread_type_id"])) {
                $bread_type_id = $_POST["bread_type_id"];
                $removed_ingredient_id_list = isset($_POST["removed_ingredient_list"]) ? $_POST["removed_ingredient_list"] : array();
                if(add_product_to_cart($product_id, $chart_id, $bread_type_id, $removed_ingredient_id_list, isset($_POST["quantity"]) && is_numeric($_POST["quantity"]) ? intval($_POST["quantity"]) : 1 )) {
                    echo "Prodotto aggiunto al carrello.";
                } else {
                    echo "Impossibile aggiungere il prodotto al carrello.";
                }
            } else {
                if(add_product_to_cart($product_id, $chart_id, NULL, array(), isset($_POST["quantity"]) && is_numeric($_POST["quantity"]) ? intval($_POST["quantity"]) : 1)) {
                    echo "Prodotto aggiunto al carrello.";
                } else {
                    echo "Impossibile aggiungere il prodotto al carrello.";
                }
            }     
        } else {
            echo "Nessun prodotto selezionato.";
        }
    } else {
        echo "Devi accedere per effettuare acquisti.";
    }
} else {
    echo "Questa non è una richiesta post.";
}

?>