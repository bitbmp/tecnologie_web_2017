<?php
require_once "../orders_utils.php";

$response = new stdClass();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        if(isset($_POST["order_id"])) {
            $user_id = $_SESSION["user"]->id;            
            $order_id = $_POST["order_id"];
            if(set_order_as_usual($user_id, $order_id)) {
                reload_user();
                $response->msg = "Ordine impostato come 'Il solito'.";
                echo json_encode($response);			
            } else {
                $response->msg = "Impossibile impostare l'ordine come 'Il solito'.";
                echo json_encode($response);
            }        
        } else {
            $response->msg = "Parametri mancanti.";
            echo json_encode($response);
        }
    } else {
        $response->msg = "Devi accedere per effettuare queste operazioni.";
        echo json_encode($response);
    }
} else {
    $response->msg = "Questa non è una richiesta post.";
    echo json_encode($response);
}