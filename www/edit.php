<!DOCTYPE html>
<html lang="it">

<head>
	<?php 
		require_once "php/page_parts.php";
		require_once "php/login_utils.php"; 
		echo_head("Gestione prodotti");
	?>
	
	<link href="js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />
	<script src="js/jquery-ui/jquery-ui.min.js"></script>
	
	<!-- Include one of jTable styles. -->
	<link href="js/jtable/themes/metro/lightgray/jtable.css" rel="stylesheet" type="text/css" />
 
 	<!-- Include jTable script file. -->
 	<script src="js/jtable/jquery.jtable.min.js"></script>
	<script src="js/jtable/localization/jquery.jtable.it.js"></script>
	<script src="js/product_edit.js"></script>
</head>

<body>

	<?php echo_menu("edit.php"); ?>
	<main>
		<div class="container">
			<?php if(is_user_logged_in() && is_user_admin())	{ ?>
			<?php echo_divider("Gestione prodotti"); ?>
			<div id="div-table-ing"></div>
			<?php echo_divider(); ?>
			<div id="div-table-bread"></div>
			<?php echo_divider(); ?>
			<div id="div-table-ptype"></div>
			<?php echo_divider(); ?>
			<div id="div-table-prod" style="overflow-x: auto;"></div>
			<?php 
			} else {
				?> <p class="text-center">Devi essere loggato come amministratore per accedere a questa pagina!<p> <?php	
			}
			?>
		</div>
	</main>

	<?php echo_footer("Index"); ?>

</body>

</html>