function InputInvalidator(component, condition) {
    condition ? invalidate_components({component}) : validate_components({component});
}

function invalidate_components(components) {
    $.each(components, function (key, value) {
        value.removeClass("is-valid");
        value.addClass("is-invalid");
    });
}
function validate_components(components) {
    $.each(components, function (key, value) {
        value.removeClass("is-invalid");
        value.addClass("is-valid");
    });
}
function clean_components_validation(components) {
    $.each(components, function (key, value) {
        value.removeClass("is-invalid");
        value.removeClass("is-valid");
    });
}

function FormToggler(form, toggle_button, default_button_html) {
    console.log("toggle form");    
    if(form.css('display') == 'none') {
        toggle_button.html("Annulla");
    } else {
        toggle_button.html(default_button_html);
    }
    form.slideToggle("slow");
}