var cards_request_number = 0;

$(document).ready(function(){
    //Cards list section
    getAllCards(cards_request_number);    
    
    //Remove card handler
    $(".btn-remove_card").click(removeCard);
    //Change default card handler
    $(".btn-set_default_card").click(changeDefaultCard);
    //Remove default card handler 
    $(".btn-remove_default_card").click(removeDefaultCard);

    //Add form helper handler
    $("#txbCartNumber").on("keyup", function() {
        InputInvalidator($(this), !checkCardNumber($(this).val()));
    });
    $("#txbCvv").on("keyup", function() {
        InputInvalidator($(this), !checkCvv($(this).val()));
    });
    $("#txbOwner").on("keyup", function() {
        InputInvalidator($(this), !checkOwner($(this).val()));
    });
    
    //Add-cards form handler
    $(".btn-toggle-newcard").click( function() {
        FormToggler($("#add-card-form"), $(this), "Aggiungi metodo di pagamento&nbsp;&nbsp;&nbsp;&nbsp;<span class=\"far fa-credit-card\"></span>");
    });
    $(".btn-add_newcard").click(addCard);
    addComboboxValues();    
});

function getAllCards(request) {
    cards_request_number++;    
    console.log("get all card triggered");
    $('#cards-list-area').html("");
    $.ajax({
        type: "POST",
        url: "php/web_methods/get_all_cards.php",
        success: function(html){
            if(html != "Devi essere loggato per accedere." && html != "Questa non è una richiesta post." && cards_request_number === request+1) {
                $('#cards-list-area').append(html);
                //Need to re-attach event to remove card buttons
                $(".btn-remove_card").click(removeCard);
                $(".btn-set_default_card").click(changeDefaultCard);
                $(".btn-remove_default_card").click(removeDefaultCard);                
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("get_all_cards failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function changeDefaultCard(event) {
    console.log("change default card triggered");
    var form = $(this).parents('form:first');
    var card_id = form.find('.input-hidden-cart-id');

    var obj_data = {card_id: card_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/change_default_card.php",
        data: obj_data,
        success: function(msg){
            console.log(msg);
            if (msg == "Carta promossa a default.") {
                getAllCards(cards_request_number);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("change_default_card failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function removeDefaultCard(event) {
    console.log("remove default card triggered");
    var form = $(this).parents('form:first');
    var card_id = form.find('.input-hidden-cart-id');

    var obj_data = {card_id: card_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/remove_default_card.php",
        data: obj_data,
        success: function(msg){
            console.log(msg);
            if (msg == "Carta rimossa da default.") {
                getAllCards(cards_request_number);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("remove_default_card failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function removeCard(event) {
    console.log("remove card triggered");
    var form = $(this).parents('form:first');
    var card_id = form.find('.input-hidden-cart-id');

    var obj_data = {card_id: card_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/remove_card.php",
        data: obj_data,
        success: function(msg){
            console.log(msg);
            if (msg == "Carta rimossa.") {
                getAllCards(cards_request_number);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("remove_card failed");
            console.log(textStatus, errorThrown);
        },
    });
} 

function addCard(event) {
    console.log("add card triggered");
    
    var form = $(this).parents('form:first');
    var owner = form.find('#txbOwner');
    var card_type = form.find('#card_type option:selected');
    var card_number = form.find('#txbCartNumber');
    var year = form.find('#year option:selected');
    var month = form.find('#month option:selected');
    var cvv = form.find('#txbCvv');

    if(checkInput(owner, card_number, cvv)) {
        var obj_data = {owner: owner.val(), card_type: card_type.val(), card_number: card_number.val(), month: month.val(), year: year.val(), cvv: cvv.val()};
        $.ajax({
            type: "POST",
            url: "php/web_methods/add_card.php",
            data: obj_data,
            success: function(msg){
                if (msg == "Carta aggiunta.") {
                    getAllCards(cards_request_number);                                        
                    $(".btn-toggle-newcard").delay(3000).trigger("click");   
                    form.trigger("reset");
                } else {
                    console.log(msg);
                }
                clean_components_validation({owner, card_number, cvv});                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("add card failed");
                console.log(textStatus, errorThrown);
            },
        });
    }
}

function addComboboxValues() {
    addMonthOptions();
    addYearOptions(20);
    addCardTypeOptions();
}
function addMonthOptions() {
    for (var i = 1; i <= 12; i++) {
        $("#month").append(new Option(i, i));        
    }
}

function addYearOptions(year_count) {
    var current_year = new Date().getFullYear();
    for (var i = current_year; i < current_year+year_count; i++) {
        $("#year").append(new Option(i, i));        
    }
}

function addCardTypeOptions() {
    $.ajax({
        type: "POST",
        url: "php/web_methods/get_cards_type.php",
        dataType: "json",
        success: function(cards){
            if(cards != "Devi accedere per effettuare acquisti." && cards != "Questa non è una richiesta post.") {
                for (var i = 0; i < cards.length; i++) {
                    $("#card_type").append(new Option(cards[i].type, cards[i].id));        
                }
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("add_to_cart failed");
            console.log(textStatus, errorThrown);
        },
     });
}

function checkCardNumber(card_number) {
    return (card_number != null && $.isNumeric(card_number) && card_number > 0 && card_number.toString().length == 16);
}

function checkCvv(cvv) {
    return (cvv != null && $.isNumeric(cvv) && cvv > 0 && cvv.toString().length == 3);
}

function checkOwner(owner) {
    return owner != null && jQuery.trim(owner).length > 0 && !/\d/.test(owner);
}

function checkInput(owner, card_number, cvv) {
    if(!checkOwner(owner.val())) {
        console.log("Owner not valid");
        invalidate_components({owner});
        return false;
    } else if (!checkCardNumber(card_number.val())) {
        console.log("Card number not valid");
        invalidate_components({card_number});
        validate_components({owner});
        return false;
    } else if (!checkCvv(cvv.val())) {
        console.log("Cvv not valid");
        invalidate_components({cvv});
        validate_components({owner, card_number});
        return false; 
    } else { 
        return true;
    }
}
