<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    require_once "../product_utils.php";
    if(!empty($_POST["filter_product_types"]) && !empty($_POST["page_number"]) && is_numeric($_POST["page_number"])) {
        echo get_products_sections_from_sql_procedure("pull_products_regex", $_POST["page_number"] != 1, "ssi", empty($_POST["search_keyword"]) ? ".*" : get_regex_from_search_string($_POST["search_keyword"]), $_POST["filter_product_types"], $_POST["page_number"]);
    } else if(!empty($_POST["search_keyword"]) && !empty($_POST["page_number"]) && is_numeric($_POST["page_number"])) {
        //echo get_products_sections_search_by_keyword($_POST["search_keyword"], $_POST["page_number"]);
        echo get_products_sections_from_sql_procedure("search_products_regex", $_POST["page_number"] != 1, "si", empty($_POST["search_keyword"]) ? ".*" : get_regex_from_search_string($_POST["search_keyword"]), $_POST["page_number"]);
    } else if (!empty($_POST["page_number"]) && is_numeric($_POST["page_number"])) {
        //echo get_products_sections("view_food_list", $_POST["page_number"]);
        echo get_products_sections_from_sql_procedure("get_most_bought", $_POST["page_number"] != 1, "i", $_POST["page_number"]);
    } else {
        echo "Invalid parameters";
    }

} else {
    echo "Questa non è una richiesta post.";
}

?>