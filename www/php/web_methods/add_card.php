<?php
require_once "../cards_utils.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        if(isset($_POST["owner"]) && isset($_POST["card_type"]) && isset($_POST["card_number"]) && isset($_POST["month"]) && isset($_POST["year"]) && isset($_POST["cvv"])) {
            $user_id = $_SESSION["user"]->id;            
            $owner = $_POST["owner"];
            $card_type = $_POST["card_type"];
            $card_number = $_POST["card_number"];
            $month = $_POST["month"];
            $year = $_POST["year"];
            $cvv = $_POST["cvv"];
            if(add_card_to_user($user_id, $owner, $card_type, $card_number, $month, $year, $cvv)) {
                echo "Carta aggiunta.";
            } else {
                echo "Impossibile aggiungere la carta.";
            }        
        } else {
            echo "Parametri mancanti.";
        }
    } else {
        echo "Devi accedere per effettuare queste operazioni.";
    }
} else {
    echo "Questa non è una richiesta post.";
}

?>