<?php	
    require_once "../../product_edit.php";
    require_once "../../login_utils.php";


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(is_user_logged_in() && is_user_admin())	{
            if(isset($_POST["id"]) && isset($_POST["name"]) && isset($_POST["price"]) && isset($_POST["prep_time"]) && isset($_POST["product_type"])) {
                echo json_encode(update_product($_POST["id"], $_POST["name"], $_POST["price"], $_POST["prep_time"], $_POST["product_type"])); 
            } else {
                echo json_encode(array('Result' => 'ERROR', 'Message' => 'Wrong parameters.')); 
            }
        } else {
            echo "Devi essere loggato per accedere.";
        }	
    } else {
        echo "Questa non è una richiesta post.";
    }
?>