<?php
	require_once "../mysql_conn.php";
	require_once "../login_utils.php";
	
	
	if(!is_user_logged_in()) {
		echo "-1";
		exit(1);
	}
	
	$conn = connect_db();
	$result = $conn->query("SELECT count(id) as c FROM notifications WHERE user = ".get_user()->id." AND disposed = 0");
	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		$c = $row["c"];
	} else {
		$c = 0;
	}
	$conn->close();
	echo $c;
?>