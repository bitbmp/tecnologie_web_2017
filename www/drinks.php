<!DOCTYPE html>
<html lang="it">

<head>
    <?php
		require_once "php/page_parts.php"; 
		echo echo_head("Bevande");
	?>
    <script src="js/personalize_ingredients.js"></script>
	<script src="js/add_to_cart.js"></script>
	<script src="js/pull_products.js"></script>
    <script src="js/update_price_bread_type.js"></script>
	<script>
		current_filter = "100";
		default_show_dynamic_area = true;
	</script>
</head>
<body>

    <?php echo_menu("drinks.php"); ?>	

    <main>
        <div class="container">

            <?php echo_search_bar("Cerca per nome"); ?>
            
            <div class="hide-during-search">
                <?php echo_divider("Bevande"); ?>
            </div>

            <div class="show-during-search" style="display:none;">
			    <?php echo_divider("Ricerca"); ?>
		    </div>
		    <div class="product-list-dynamic-area"></div>

		    <div id="loading" class="loader-ellips" style="display:none;">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
		    </div> 
        </div>
    </main>
    
    <?php echo_footer("Drinks"); ?>

</body>
</html>
