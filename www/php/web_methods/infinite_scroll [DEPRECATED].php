<?php
//now is post call and can add only drinks.
if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    require_once "../product_utils.php";
    if(isset($_POST["page_number"]) && is_numeric($_POST["page_number"])) {
        echo get_products_sections("view_food_list", $_POST["page_number"]);
    } else {
        echo "Invalid page number";
    }

} else {
    echo "Questa non è una richiesta post.";
}

?>