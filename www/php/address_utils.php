<?php
require_once "mysql_conn.php";
require_once "login_utils.php";

function get_addresses_count() {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT count(id) as c FROM view_valid_addresses where user = ?");
    $stmt->bind_param("i", get_user()->id);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $count = $row["c"];
    } 
	$conn->close();
	return $count;
}

function add_address_to_user($user_id, $road, $civic_number, $cap, $city, $province, $alias) {
    $address = $road . " " . $civic_number . ", " . $city . " " . $cap . " (" . strtoupper($province) .  "), IT";
    $default_type = 0;
    if (address_already_present($user_id, $address, $alias)) {
        $result = false;        
    } else {
        $conn = connect_db();
        $stmt = $conn->prepare("INSERT INTO address (address, type, alias, user) VALUES (?, ?, ?, ?)");
        $stmt->bind_param("sisi", $address, $default_type, $alias, $user_id);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = false;
        }
        $stmt->close();
        $conn->close();
    }
    return $result;
}

function remove_address_from_user($user_id, $address_id) {
     //Remove user default address. Note this is ok because remove_user_default_address works only if the address to remove from default is exactly the default address.
     if(remove_user_default_address($user_id, $address_id)) {
        $conn = connect_db();
        $stmt = $conn->prepare("UPDATE address SET deleted = 1 WHERE user = ? AND id = ?");
        $stmt->bind_param("ii", $user_id, $address_id);
        if ($stmt->execute()) {
            $result = true;
        } else {
            $result = false;
        }
        $stmt->close();
        $conn->close();
    } else {
        $result = false;
    }
    return $result;
}

function change_user_default_address($user_id, $address_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("UPDATE user SET default_address = ? WHERE id = ?");
    $stmt->bind_param("ii", $address_id, $user_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

function remove_user_default_address($user_id, $address_id) {
    $conn = connect_db();
    $stmt = $conn->prepare("UPDATE user SET default_address = NULL WHERE id = ? AND default_address = ?");
    $stmt->bind_param("ii", $user_id, $address_id);
    if ($stmt->execute()) {
        $result = true;
    } else {
        $result = false;
    }
    $stmt->close();
    $conn->close();
    return $result;
}

function address_already_present($user_id, $address, $alias) {
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT count(*) as c FROM view_valid_addresses WHERE user = ? AND (alias = ? OR address = ?)");
    $stmt->bind_param("iss", $user_id, $alias, $address);
    $stmt->execute();
    $result = $stmt->get_result();
    if ($row = $result->fetch_assoc()) {
        $count = $row["c"];
    }
    $stmt->close();
    $conn->close();
    return $count > 0;
}

function get_addresses_sections($user_id) {
    $default_type = 0;    
    $conn = connect_db();
    $stmt = $conn->prepare("SELECT view_valid_addresses.id, address, type, alias, view_valid_addresses.user, default_address FROM view_valid_addresses LEFT JOIN user ON view_valid_addresses.user = user.id WHERE user.id = ? AND type = ?");
    $stmt->bind_param("ii", $user_id, $default_type);
    $stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$html = get_addresses_sections_from_result($conn, $result);
	$conn->close();
	return $html;
}

function get_addresses_sections_from_result($conn, $result) {
    if ($result->num_rows > 0) {
		ob_start();
		while ($myrow = $result->fetch_assoc()) {
			internal_echo_address_section($conn, $myrow["id"], $myrow["address"], $myrow["type"], $myrow["alias"], $myrow["id"] == $myrow["default_address"]);
            ?><div class="product-separator"></div><?php
		}
		return ob_get_clean();
	} else {
		return "";
	}
}

function internal_echo_address_section($conn, $address_id, $address, $type, $alias, $is_default) {
    ?>
    <section class="address-section">
        <form method="post">
            <input type="hidden" name="address_id"  class="input-hidden-address-id" value="<?php echo $address_id; ?>">
            <div class="row mb-0">
                <div class="col-9">
                    <p class="mb-0" style="font-weight: bold;"><?php echo $is_default ? "Indirizzo di default" : ""; ?></p>
                    <h4 class="mb-0"><?php echo $alias; ?></h4>
                    <p class="mb-0"><?php echo $address; ?></p>
                </div>
                <div class="col-3 text-right">
                    <button type="button" class="btn btn-danger btn-remove_address"><span class="far fa-trash-alt"></span></button>
                </div>
            </div>
            <?php if(!$is_default) { ?>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-link btn-set_default_address">Imposta a default&nbsp;&nbsp;&nbsp;<span class="fas fa-cogs"></span></button>
                        </div>
                    </div>
             <?php } else {?>
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="btn btn-link btn-remove_default_address" style="color:red;">Rimuovi da default&nbsp;&nbsp;&nbsp;<span class="fas fa-cogs"></span></button>
                        </div> <?php
            }?>
        </form>
    </section>
    <?php
}

//Get address type string from code 
function address_string($address_code) {
    switch ($address_code) {
        case 0:
            return "Indirizzo";
            break;
        case 1:
            return "Punto di ritiro";
            break;
        case 2:
            return "La mia posizione";
            break;
        default:
            return "";
    }
}

?>