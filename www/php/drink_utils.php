<?php
require_once "php/mysql_conn.php";

function get_drink_type_id() {
	return 100;
}

function get_all_drink_divs() {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT product.id, product.name, price FROM product_type INNER JOIN product ON product_type.id = product.product_type WHERE product_type.id = ?");
    $stmt->bind_param("i", get_drink_type_id());    
    $stmt->execute();
    
    $result = $stmt->get_result();

	$stmt->close();
	$conn->close();	
	$html = "";
	ob_start();
	$i = 0;
	while ($myrow = $result->fetch_assoc()) {
		if($i != 0) {
			?><div class="product-separator"></div><?php
		}
		internal_echo_drink_div($myrow);
		$i++;
	}
	return ob_get_clean();
}
function get_drink_div($drink_id) {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT product.id, product.name, price FROM product_type INNER JOIN product ON product_type.id = product.product_type WHERE product_type.id = ? AND product.id = ?");
    $stmt->bind_param("ii", get_drink_type_id(), $drink_id);    
    $stmt->execute();
    
    $result = $stmt->get_result();

	$stmt->close();
	$conn->close();	
	$html = "";
	ob_start();
	if ($myrow = $result->fetch_assoc()) {
		internal_echo_drink_div($myrow);
	}
	return ob_get_clean();
}
function internal_echo_drink_div($myrow) {
	?>
	<section class="drink_section">
	<form method="post">
		<input type="hidden" name="product_id"  class="input-hidden-product-id" value="<?php echo $myrow["id"]; ?>">
		<input type="hidden" class="input-hidden-personalized" name="personalized" value="false">
			<div class="row">
				<div class="col-6">
					<p class="mb-0"><?php echo $myrow["name"]; ?></p>
					<small id="add_to_cart_message">
						<p class="mt-0 text-left" id="add_to_cart_text" style="display:none;"></p>
					</small>
				</div>
				<div class="col-3">
					<p><?php echo $myrow["price"]; ?>€</p>
				</div>
				<div class="col-3">
					<button type="button" class="btn btn-primary btn-add_to_cart"><div class="fa fa-cart-plus fa-lg"></div>
				</div>
			</div>
		</form>
	</section>	
	<?php
}
?>