<?php
require_once "../orders_utils.php";
    
if (isset($_POST["state_id"])) {
    if(is_user_admin())	{           
        echo get_all_orders_divs($_POST["state_id"]);
    } else {
        echo "Devi essere un amministratore";
    }	
} else {
    echo "Parametri non validi";
}

?>