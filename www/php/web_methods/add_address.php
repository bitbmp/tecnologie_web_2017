<?php
require_once "../address_utils.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(is_user_logged_in())	{
        if(isset($_POST["road"]) && isset($_POST["civic_number"]) && isset($_POST["cap"]) && isset($_POST["city"]) && isset($_POST["province"]) && isset($_POST["alias"])) {
            $user_id = $_SESSION["user"]->id;            
            $road = $_POST["road"];
            $civic_number = $_POST["civic_number"];
            $cap = $_POST["cap"];
            $city = $_POST["city"];
            $province = $_POST["province"];
            $alias = $_POST["alias"];
            if(add_address_to_user($user_id, $road, $civic_number, $cap, $city, $province, $alias)) {
                echo "Indirizzo aggiunto.";
            } else {
                echo "Impossibile aggiungere l'indirizzo.";
            }
        } else {
            echo "Parametri mancanti.";
        }
    } else {
        echo "Devi accedere per effettuare queste operazioni.";
    }
} else {
    echo "Questa non è una richiesta post.";
}

?>