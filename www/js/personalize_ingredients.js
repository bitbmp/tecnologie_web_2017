$(document).ready(function() {
    $(".btn-personalize_ing").click(personalizeButtonClick);
});

function personalizeButtonClick(event) {
    var form = $(this).parents("form:first");
    var is_personalized = form.find('input.input-hidden-personalized').prop("value");
    changePersonalizeAreaVisibility($(this), form, is_personalized === "false"); 
}

function changePersonalizeAreaVisibility(button, form, is_visible) {
    var personalize_area = form.find('.personalize-area');
    var personalized_input = form.find('input.input-hidden-personalized');
    var is_personalized = personalized_input.prop("value");
    var ingList = form.find('.ingredient-list');

    if ( is_visible ) {
        personalized_input.prop("value", "true");
        ingList.hide();
        personalize_area.slideDown();
        button.html("Annulla");
        setNewPrice(form);
    } else {
        personalized_input.prop("value", "false");
        personalize_area.slideUp( function() {
            ingList.show();
        });
        button.html("Personalizza");
        setBasePrice(form);
    }

}

/* OLD function
$(".btn-personalize_ing").click( function(){
    
    var form = $(this).parents("form:first");
    
    var checkboxes = form.find(':input[type="checkbox"]');
    var radios = form.find(':input[type="radio"]');
    var labels = form.find('.label-personalize');
    
    var personalize_area = form.find('.personalize-area');
    var personalized_input = form.find('input.input-hidden-personalized');
    var is_personalized = personalized_input.prop("value");
    var ingList = form.find('.ingredient-list');
    
    
    if ( is_personalized === "false") {
        personalized_input.prop("value", "true");
        checkboxes.prop("disabled", false);
        radios.prop("disabled", false);
        ingList.hide();
        checkboxes.show();
        radios.show();
        labels.show();
        $(this).html("Annulla");
    } else {
        personalized_input.prop("value", "false");
        checkboxes.prop("checked", true);
        checkboxes.prop("disabled", true);
        radios.prop("disabled", true);
        ingList.show();
        checkboxes.hide();
        radios.hide();
        labels.hide();
        $(this).html("Personalizza");
    }
    
});
*/