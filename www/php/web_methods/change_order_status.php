<?php
require_once "../orders_utils.php";

$response = new stdClass();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in() && is_user_admin())	{
        if(isset($_POST["order_id"]) && isset($_POST["order_status"])) {
            $order_id = $_POST["order_id"];
            $order_status = $_POST["order_status"];
            if($result = change_order_status($order_id, $order_status)) {
                $response->msg = "Stato cambiato.";
                $record = $result->fetch_assoc();
                ob_start();
                echo_internal_order_state($order_id, $record);
                $html = ob_get_clean();
                $response->html = $html;
                echo json_encode($response);
				add_order_notification($order_id, $order_status);
            } else {
                $response->msg = "Impossibile cambiare stato dell'ordine.";
                echo json_encode($response);
            }        
        } else {
            $response->msg = "Parametri mancanti.";
            echo json_encode($response);
        }
    } else {
        $response->msg = "Devi accedere per effettuare queste operazioni.";
        echo json_encode($response);
    }
} else {
    $response->msg = "Questa non è una richiesta post.";
    echo json_encode($response);
}

function add_order_notification($order_id, $new_state) {
	$content = null;
	if($new_state == 4) {
		ob_start();
		?>
		Il tuo <a href="order_details.php?id=<?php echo $order_id; ?>">ordine #<?php echo $order_id; ?></a> è stato cancellato dal venditore
		<?php
		$content = ob_get_clean();
		ob_start();
		?>
		Hai cancellato <a href="order_details.php?id=<?php echo $order_id; ?>">l'ordine #<?php echo $order_id; ?></a>
		<?php
		$content_admin = ob_get_clean();
	}
	if($new_state == 1) {
		ob_start();
		?>
		Il tuo <a href="order_details.php?id=<?php echo $order_id; ?>">ordine #<?php echo $order_id; ?></a> è in preparazione
		<?php
		$content = ob_get_clean();
		ob_start();
		?>
		Hai accettato <a href="order_details.php?id=<?php echo $order_id; ?>">l'ordine #<?php echo $order_id; ?></a>
		<?php
		$content_admin = ob_get_clean();
	}
	if($new_state == 2) {
		ob_start();
		?>
		Il tuo <a href="order_details.php?id=<?php echo $order_id; ?>">ordine #<?php echo $order_id; ?></a> è pronto
		<?php
		$content = ob_get_clean();
		ob_start();
		?>
		Hai messo in consegna <a href="order_details.php?id=<?php echo $order_id; ?>">l'ordine #<?php echo $order_id; ?></a>
		<?php
		$content_admin = ob_get_clean();
	}
	if($new_state == 3) {
		ob_start();
		?>
		Il tuo <a href="order_details.php?id=<?php echo $order_id; ?>">ordine #<?php echo $order_id; ?></a> è stato ritirato
		<?php
		$content = ob_get_clean();
		ob_start();
		?>
		Hai consegnato <a href="order_details.php?id=<?php echo $order_id; ?>">l'ordine #<?php echo $order_id; ?></a>
		<?php
		$content_admin = ob_get_clean();
	}
	
	
	require_once "../notification_utils.php";
	$user_id = get_user_id_from_order($order_id);
	if($user_id != null && $content != null) {
		add_notification($content, $user_id);
		add_admin_notification($content_admin);
	}
}
?>