<?php
require_once "mysql_conn.php";

function get_ingredients_array($sorting = "name ASC", $startIndex = 0, $pageSize = 1000000) {
	$sorting_params = explode( ' ', $sorting);
	if(!empty($sorting_params) && $sorting_params && count($sorting_params) === 2 && check_parameter($sorting_params[0], "id", "name", "price")
	&& check_parameter($sorting_params[1], "ASC", "DESC") && is_numeric($startIndex) && is_numeric($pageSize)) {
		$conn = connect_db();
		$stmt = $conn->prepare("SELECT id, name, price from ingredient ORDER BY ".$sorting." LIMIT ".$startIndex.", ".$pageSize);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$conn->close();
		$ings_ar = array();
		while ($myrow = $result->fetch_assoc()) {
			array_push($ings_ar, $myrow);
		}
		return $ings_ar;
	} else {
		return array();
	}
}

function get_ingredients_count() {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT Count(*) AS Count from ingredient");
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	if ($result->num_rows > 0) {
		$my_row = $result->fetch_assoc();
		return intval($my_row["Count"]);
	} else {
		return 0;
	}
}

function check_parameter($param, ...$values) {
	foreach ($values as $v) {
        if($param === $v) {
			return true;
		}
	}
	return false;
}

function list_ingredient($sorting, $startIndex, $pageSize) {
	return array('Result' => 'OK', "Records" => get_ingredients_array($sorting, $startIndex, $pageSize), 'TotalRecordCount' => get_ingredients_count());
}

function insert_ingredient($name, $price) {
	$conn = connect_db();
	$stmt = $conn->prepare("INSERT INTO ingredient(name, price) VALUES (?, ?)");
	$stmt->bind_param("sd", $name, $price);
	$result_to_ret = array();
	if($stmt->execute()) {
		$stmt->close();
		$stmt = $conn->prepare("SELECT id, name, price from ingredient WHERE id = LAST_INSERT_ID()");
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			$my_row = $result->fetch_assoc();
			$result_to_ret = array('Result' => 'OK', 'Record' => $my_row);
		} else {
			$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert completed but could not fetch the inserted record: '.$stmt->error);
		}
	} else {
		$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert failed: '.$stmt->error);	
	}
	$stmt->close();
	$conn->close();
	return $result_to_ret;
}

function delete_ingredient($id) {
	$conn = connect_db();
	$stmt = $conn->prepare("DELETE FROM ingredient WHERE id = ?");
	$stmt->bind_param("i", $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Delete failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}

function update_ingredient($id, $name, $price) {
	$conn = connect_db();
	$stmt = $conn->prepare("UPDATE ingredient SET name = ?, price = ? WHERE id = ?");
	$stmt->bind_param("sdi", $name, $price, $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Edit failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}

function get_breads_array($sorting = "name ASC", $startIndex = 0, $pageSize = 1000000) {
	$sorting_params = explode( ' ', $sorting);
	if(!empty($sorting_params) && $sorting_params && count($sorting_params) === 2 && check_parameter($sorting_params[0], "id", "name", "price")
	&& check_parameter($sorting_params[1], "ASC", "DESC") && is_numeric($startIndex) && is_numeric($pageSize)) {
		$conn = connect_db();
		$stmt = $conn->prepare("SELECT id, name, price from bread_type ORDER BY ".$sorting." LIMIT ".$startIndex.", ".$pageSize);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$conn->close();
		$ings_ar = array();
		while ($myrow = $result->fetch_assoc()) {
			array_push($ings_ar, $myrow);
		}
		return $ings_ar;
	} else {
		return array();
	}
}

function get_breads_count() {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT Count(*) AS Count from bread_type");
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	if ($result->num_rows > 0) {
		$my_row = $result->fetch_assoc();
		return intval($my_row["Count"]);
	} else {
		return 0;
	}
}

function list_bread($sorting, $startIndex, $pageSize) {
	return array('Result' => 'OK', "Records" => get_breads_array($sorting, $startIndex, $pageSize), 'TotalRecordCount' => get_breads_count());
}

function insert_bread($name, $price) {
	$conn = connect_db();
	$stmt = $conn->prepare("INSERT INTO bread_type(name, price) VALUES (?, ?)");
	$stmt->bind_param("sd", $name, $price);
	$result_to_ret = array();
	if($stmt->execute()) {
		$stmt->close();
		$stmt = $conn->prepare("SELECT id, name, price from bread_type WHERE id = LAST_INSERT_ID()");
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			$my_row = $result->fetch_assoc();
			$result_to_ret = array('Result' => 'OK', 'Record' => $my_row);
		} else {
			$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert completed but could not fetch the inserted record: '.$stmt->error);
		}
	} else {
		$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert failed: '.$stmt->error);	
	}
	$stmt->close();
	$conn->close();
	return $result_to_ret;
}

function delete_bread($id) {
	$conn = connect_db();
	$stmt = $conn->prepare("DELETE FROM bread_type WHERE id = ?");
	$stmt->bind_param("i", $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Delete failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}

function update_bread($id, $name, $price) {
	$conn = connect_db();
	$stmt = $conn->prepare("UPDATE bread_type SET name = ?, price = ? WHERE id = ?");
	$stmt->bind_param("sdi", $name, $price, $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Edit failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}

function get_ptypes_array($sorting = "name ASC", $startIndex = 0, $pageSize = 1000000) {
	$sorting_params = explode( ' ', $sorting);
	if(!empty($sorting_params) && $sorting_params && count($sorting_params) === 2 && check_parameter($sorting_params[0], "id", "name")
	&& check_parameter($sorting_params[1], "ASC", "DESC") && is_numeric($startIndex) && is_numeric($pageSize)) {
		$conn = connect_db();
		$stmt = $conn->prepare("SELECT id, name from product_type ORDER BY ".$sorting." LIMIT ".$startIndex.", ".$pageSize);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$conn->close();
		$ings_ar = array();
		while ($myrow = $result->fetch_assoc()) {
			array_push($ings_ar, $myrow);
		}
		return $ings_ar;
	} else {
		return array();
	}
}

function get_ptypes_count() {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT Count(*) AS Count from product_type");
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	if ($result->num_rows > 0) {
		$my_row = $result->fetch_assoc();
		return intval($my_row["Count"]);
	} else {
		return 0;
	}
}

function list_ptype($sorting, $startIndex, $pageSize) {
	return array('Result' => 'OK', "Records" => get_ptypes_array($sorting, $startIndex, $pageSize), 'TotalRecordCount' => get_ptypes_count());
}

function insert_ptype($id, $name) {
	$conn = connect_db();
	$stmt = $conn->prepare("INSERT INTO product_type(id, name) VALUES (?,?)");
	$stmt->bind_param("is", $id, $name);
	$result_to_ret = array();
	if($stmt->execute()) {
		$stmt->close();
		$stmt = $conn->prepare("SELECT id, name from product_type WHERE id = ?");
		$stmt->bind_param("i", $id);
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			$my_row = $result->fetch_assoc();
			$result_to_ret = array('Result' => 'OK', 'Record' => $my_row);
		} else {
			$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert completed but could not fetch the inserted record: '.$stmt->error);
		}
	} else {
		$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert failed: '.$stmt->error);	
	}
	$stmt->close();
	$conn->close();
	return $result_to_ret;
}

function delete_ptype($id) {
	$conn = connect_db();
	$stmt = $conn->prepare("DELETE FROM product_type WHERE id = ?");
	$stmt->bind_param("i", $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Delete failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}

function update_ptype($id, $name) {
	$conn = connect_db();
	$stmt = $conn->prepare("UPDATE product_type SET name = ? WHERE id = ?");
	$stmt->bind_param("si", $name, $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Edit failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}


function get_breads_of_ptype_array($sorting = "name ASC", $startIndex = 0, $pageSize = 1000000, $ptypeId = 1) {
	$sorting_params = explode( ' ', $sorting);
	if(!empty($sorting_params) && $sorting_params && count($sorting_params) === 2 && check_parameter($sorting_params[0], "id", "name")
	&& check_parameter($sorting_params[1], "ASC", "DESC") && is_numeric($startIndex) && is_numeric($pageSize)) {
		$conn = connect_db();
		$stmt = $conn->prepare("SELECT bread_type.id, bread_type.name from product_type INNER JOIN product_bread_type ON product_type.id = product_bread_type.product_type 
		INNER JOIN bread_type ON bread_type.id = product_bread_type.bread_type WHERE product_type = ? ORDER BY ".$sorting." LIMIT ".$startIndex.", ".$pageSize);
		$stmt->bind_param("i", $ptypeId);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$conn->close();
		$result_to_ret = array();
		while ($myrow = $result->fetch_assoc()) {
			array_push($result_to_ret, $myrow);
		}
		return $result_to_ret;
	} else {
		return array();
	}
}

function get_breads_of_ptype_count($ptypeId = 1) {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT Count(*) AS Count from product_type INNER JOIN product_bread_type ON product_type.id = product_bread_type.product_type 
	INNER JOIN bread_type ON bread_type.id = product_bread_type.bread_type WHERE product_type = ?");
	$stmt->bind_param("i", $ptypeId);
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	if ($result->num_rows > 0) {
		$my_row = $result->fetch_assoc();
		return intval($my_row["Count"]);
	} else {
		return 0;
	}
}

function list_breads_of_ptype($sorting, $startIndex, $pageSize, $ptypeId) {
	return array('Result' => 'OK', "Records" => get_breads_of_ptype_array($sorting, $startIndex, $pageSize, $ptypeId), 'TotalRecordCount' => get_breads_of_ptype_count($ptypeId));
}


function insert_breads_of_ptype($ptypeId, $breadId) {
	$conn = connect_db();
	$stmt = $conn->prepare("INSERT INTO product_bread_type(product_type, bread_type) VALUES (?,?)");
	$stmt->bind_param("ii", $ptypeId, $breadId);
	$result_to_ret = array();
	if($stmt->execute()) {
		$stmt->close();
		$stmt = $conn->prepare("SELECT bread_type.id, bread_type.name from bread_type WHERE id = ?");
		$stmt->bind_param("i", $breadId);
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			$my_row = $result->fetch_assoc();
			$result_to_ret = array('Result' => 'OK', 'Record' => $my_row);
		} else {
			$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert completed but could not fetch the inserted record: '.$stmt->error);
		}
	} else {
		$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert failed: '.$stmt->error);	
	}
	$stmt->close();
	$conn->close();
	return $result_to_ret;
}


function delete_breads_of_ptype($ptypeId, $breadId) {
	$conn = connect_db();
	$stmt = $conn->prepare("DELETE FROM product_bread_type WHERE product_type = ? AND bread_type = ?");
	$stmt->bind_param("ii", $ptypeId, $breadId);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Delete failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}


function get_available_breads($ptypeId) {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT bread_type.id, bread_type.name FROM bread_type WHERE bread_type.id NOT IN (
	SELECT product_bread_type.bread_type FROM product_bread_type WHERE product_bread_type.product_type = ?)");
	$stmt->bind_param("i", $ptypeId);
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	$formatted_data = array();
	while ($myrow = $result->fetch_assoc()) {
		array_push($formatted_data, (object)array('DisplayText' => $myrow['name'], 'Value' => $myrow['id']));
	}
	return array('Result' => 'OK', 'Options' => $formatted_data);
}


function get_products_array($sorting = "name ASC", $startIndex = 0, $pageSize = 1000000) {
	$sorting_params = explode( ' ', $sorting);
	if(!empty($sorting_params) && $sorting_params && count($sorting_params) === 2 && check_parameter($sorting_params[0], "id", "name", "price", "prep_time", "buy_count", "created_by", "product_type")
	&& check_parameter($sorting_params[1], "ASC", "DESC") && is_numeric($startIndex) && is_numeric($pageSize)) {
		$conn = connect_db();
		$stmt = $conn->prepare("SELECT product.id, product.name, product.price, product.prep_time, product.buy_count, CONCAT( user.first_name,\" \", user.last_name) AS created_by, product.product_type 
		FROM product INNER JOIN user ON user.id = product.created_by
		ORDER BY ".$sorting." LIMIT ".$startIndex.", ".$pageSize);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$conn->close();
		$ings_ar = array();
		while ($myrow = $result->fetch_assoc()) {
			array_push($ings_ar, $myrow);
		}
		return $ings_ar;
	} else {
		return array();
	}
}

function get_products_count() {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT Count(*) AS Count from product");
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	if ($result->num_rows > 0) {
		$my_row = $result->fetch_assoc();
		return intval($my_row["Count"]);
	} else {
		return 0;
	}
}

function list_product($sorting, $startIndex, $pageSize) {
	return array('Result' => 'OK', "Records" => get_products_array($sorting, $startIndex, $pageSize), 'TotalRecordCount' => get_products_count());
}

function get_available_ptypes() {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT product_type.id, product_type.name FROM product_type");
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	$formatted_data = array();
	while ($myrow = $result->fetch_assoc()) {
		array_push($formatted_data, (object)array('DisplayText' => $myrow['name'], 'Value' => $myrow['id']));
	}
	return array('Result' => 'OK', 'Options' => $formatted_data);
}


function insert_product($name, $price, $prep_time, $created_by, $product_type) {
	$conn = connect_db();
	$stmt = $conn->prepare("INSERT INTO product(name, price, prep_time, created_by, product_type) VALUES (?,?,?,?,?)");
	$stmt->bind_param("sdiii", $name, $price, $prep_time, $created_by, $product_type);
	$result_to_ret = array();
	if($stmt->execute()) {
		$stmt->close();
		$stmt = $conn->prepare("SELECT product.id, product.name, product.price, product.prep_time, product.buy_count, CONCAT( user.first_name,\" \", user.last_name) AS created_by, product.product_type 
		FROM product INNER JOIN user ON user.id = product.created_by WHERE product.id = LAST_INSERT_ID()");
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			$my_row = $result->fetch_assoc();
			$result_to_ret = array('Result' => 'OK', 'Record' => $my_row);
		} else {
			$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert completed but could not fetch the inserted record: '.$stmt->error);
		}
	} else {
		$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert failed: '.$stmt->error);	
	}
	$stmt->close();
	$conn->close();
	return $result_to_ret;
}


function delete_product($id) {
	$conn = connect_db();
	$stmt = $conn->prepare("DELETE FROM product WHERE id = ?");
	$stmt->bind_param("i", $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Delete failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}


function update_product($id, $name, $price, $prep_time, $product_type) {
	$conn = connect_db();
	$stmt = $conn->prepare("UPDATE product SET name = ?, price = ?, prep_time = ?, product_type = ? WHERE id = ?");
	$stmt->bind_param("sdiii", $name, $price, $prep_time, $product_type, $id);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Edit failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}


function get_ings_of_prod_array($sorting = "name ASC", $startIndex = 0, $pageSize = 1000000, $prodId = 1) {
	$sorting_params = explode( ' ', $sorting);
	if(!empty($sorting_params) && $sorting_params && count($sorting_params) === 2 && check_parameter($sorting_params[0], "id", "name")
	&& check_parameter($sorting_params[1], "ASC", "DESC") && is_numeric($startIndex) && is_numeric($pageSize)) {
		$conn = connect_db();
		$stmt = $conn->prepare("SELECT ingredient.id, ingredient.name from ingredient INNER JOIN prod_ing ON ingredient.id = prod_ing.ingredient
		INNER JOIN product ON product.id = prod_ing.product WHERE product.id = ? ORDER BY ".$sorting." LIMIT ".$startIndex.", ".$pageSize);
		$stmt->bind_param("i", $prodId);
		$stmt->execute();
		$result = $stmt->get_result();
		$stmt->close();
		$conn->close();
		$result_to_ret = array();
		while ($myrow = $result->fetch_assoc()) {
			array_push($result_to_ret, $myrow);
		}
		return $result_to_ret;
	} else {
		return array();
	}
}

function get_ings_of_prod_count($prodId = 1) {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT Count(*) AS Count from product INNER JOIN prod_ing ON product.id = prod_ing.product WHERE product.id = ?");
	$stmt->bind_param("i", $prodId);
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	if ($result->num_rows > 0) {
		$my_row = $result->fetch_assoc();
		return intval($my_row["Count"]);
	} else {
		return 0;
	}
}

function list_ings_of_prod($sorting, $startIndex, $pageSize, $prodId) {
	return array('Result' => 'OK', "Records" => get_ings_of_prod_array($sorting, $startIndex, $pageSize, $prodId), 'TotalRecordCount' => get_ings_of_prod_count($prodId));
}


function insert_ings_of_prod($productId, $ingId) {
	$conn = connect_db();
	$stmt = $conn->prepare("INSERT INTO prod_ing(product, ingredient) VALUES (?,?)");
	$stmt->bind_param("ii", $productId, $ingId);
	$result_to_ret = array();
	if($stmt->execute()) {
		$stmt->close();
		$stmt = $conn->prepare("SELECT ingredient.id, ingredient.name from ingredient WHERE id = ?");
		$stmt->bind_param("i", $ingId);
		$stmt->execute();
		$result = $stmt->get_result();
		if ($result->num_rows > 0) {
			$my_row = $result->fetch_assoc();
			$result_to_ret = array('Result' => 'OK', 'Record' => $my_row);
		} else {
			$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert completed but could not fetch the inserted record: '.$stmt->error);
		}
	} else {
		$result_to_ret = array('Result' => 'ERROR', 'Message' => 'Insert failed: '.$stmt->error);	
	}
	$stmt->close();
	$conn->close();
	return $result_to_ret;
}



function delete_ings_of_prod($productId, $ingId) {
	$conn = connect_db();
	$stmt = $conn->prepare("DELETE FROM prod_ing WHERE product = ? AND ingredient = ?");
	$stmt->bind_param("ii", $productId, $ingId);
	if($stmt->execute()) {
		$result = array('Result' => 'OK');
	} else {
		$result = array('Result' => 'ERROR', 'Message' => 'Delete failed: '.$stmt->error);
	}
	$stmt->close();
	$conn->close();
	return $result;
}



function get_available_ings($productId) {
	$conn = connect_db();
	$stmt = $conn->prepare("SELECT ingredient.id, ingredient.name FROM ingredient WHERE ingredient.id NOT IN (
	SELECT prod_ing.ingredient FROM prod_ing WHERE prod_ing.product = ?)");
	$stmt->bind_param("i", $productId);
	$stmt->execute();
	$result = $stmt->get_result();
	$stmt->close();
	$conn->close();
	$formatted_data = array();
	while ($myrow = $result->fetch_assoc()) {
		array_push($formatted_data, (object)array('DisplayText' => $myrow['name'], 'Value' => $myrow['id']));
	}
	return array('Result' => 'OK', 'Options' => $formatted_data);
}

