<?php
require_once "../address_utils.php";
    
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(is_user_logged_in())	{
        $user_id = $_SESSION["user"]->id;             
        echo get_addresses_sections($user_id);
    } else {
        echo "Devi essere loggato per accedere.";
    }	
} else {
    echo "Questa non è una richiesta post.";
}

?>