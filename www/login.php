<!DOCTYPE html>
<html lang="it">

<head>
    <?php 
		require_once "php/page_parts.php"; 
		require_once "php/login_utils.php"; 
		
		if(is_user_logged_in()) {
			header("Location: index.php");
		}
		
		echo_head("Login");
	?>
</head>
<body> <!--style="min-height: 35rem; padding-top: 4.5rem;"-->

<?php echo_menu("login.php"); ?>	
<main>
	<div class="container">

		<?php echo_logo_image(); ?>
		
		<?php
			$email = "";
			$password = "";
			$email_valid = TRUE;
			$password_valid = TRUE;
			$attempts_valid = TRUE;
			
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				
				if(isset($_POST["fromRegister"])) {
					if(isset($_POST["txbEmail"])) {
						$email = $_POST["txbEmail"];
					}
				}
				else 
				{
					if(isset($_POST["txbEmail"])) {
						$email = $_POST["txbEmail"];
					} else {
						$email_valid = FALSE;
					}
					
					if(isset($_POST["txbPassword"])) {
						$password = $_POST["txbPassword"];
					} else {
						$password_valid = FALSE;
					}	
					
					if($email_valid && $password_valid) {
						if(is_attempts_ok()) {
							$user = check_login($email, $password);
							if($user != NULL) {
								user_login($user);
								header("Location: index.php");
							} else {
								$email_valid = FALSE;
								$password_valid = FALSE;
								if(!is_attempts_ok()) {
									$attempts_valid= FALSE;
								}
							}
						}
						else {
							$attempts_valid= FALSE;
						}
					}
				}
			}
		?>
		
		<form class="form-group" method="post">
			<?php echo_divider("Accesso"); ?>
			
			
			<label class="sr-only" for="txbEmail">Email</label>
			<div class="input-group mb-2 mt-2">
				<div class="input-group-addon"><div class="fa fa-envelope fa-fw"></div></div>
				<input type="email" class="form-control <?php echo !$email_valid?"is-invalid":""; ?>" id="txbEmail" name="txbEmail" placeholder="Email" value="<?php echo $email; ?>" required>
			</div>
			
			
			<label class="sr-only" for="txbPasswordTmp">Passowrd</label>
			<div class="input-group">
				<div class="input-group-addon"><div class="fa fa-lock fa-fw"></div></div>
				<input type="password" class="form-control <?php echo !$password_valid?"is-invalid":""; ?>" id="txbPasswordTmp" name="txbPasswordTmp" placeholder="Password" required>
			</div>
			
			<!-- Login failed -->
			<?php if(!$attempts_valid) { ?>
			<div class="float-right mb-4">
				<small id="loginHelp" class="text-danger">
				Hai fatto troppi tentativi, aspetta qualche minuto e riprova
				</small>
			</div>
			<?php } else if(!$email_valid || !$password_valid) { ?>
			<div class="float-right mb-4">
				<small id="loginHelp" class="text-danger">
				Email e/o password non corretti.<!-- <a href="forgot_password.php">Hai dimenticato la password?</a>-->
				</small>
			</div>
			<?php } else { ?>
				<div class="mb-4"><!--<small><a href="forgot_password.php">Password dimenticata</a></small>--></div>
			<?php } ?>
			
			<div class="form-group">
				<input type="button" onclick="formhash(this.form, this.form.txbPasswordTmp);" class="btn btn-primary btn-lg btn-block" value="Accedi">
			</div>

		</form>
		
		<div class="form-group" style="padding-top:10px">
			<?php echo_divider("Sei nuovo?"); ?>
			<button type="button" class="btn btn-primary btn-lg btn-block login-button" onclick="window.location.href='register.php'">Registrati</button>
		</div>

	</div>
</main>


</body>
</html>
