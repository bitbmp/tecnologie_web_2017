<?php
	require_once "../login_utils.php";
	if(!is_user_logged_in()) {
		echo "Forbidden";
		exit (2);
	}
	require_once "../cart_utils.php";
	require_once "../mysql_conn.php";
	require_once "../orders_utils.php";

	
	$date = get_required_date();
	if($date == NULL) {
		echo $_POST["errorMessage"];
		exit(1);
	}
	$card = get_required_card();
	if(!is_required_card_valid()) {
		echo $_POST["errorMessage"];
		exit(1);
	}
	$address = get_required_address();
	if($address == NULL) {
		echo $_POST["errorMessage"];
		exit(1);
	}
	
	if(is_cart_empty()) {
		echo $_POST["errorMessage"];
		exit(1);
	}
	
	echo place_new_order($date, $address, $card);
	
	
	function is_cart_empty() { 
		require_once "../cart_utils.php";
		if(get_cart_products_count() <= 0) {
			$_POST["errorMessage"] = "Il carrello è vuoto";
			return true;
		}
		return false;
	}
	function get_required_date() { 
		$_POST["errorMessage"] = "La data inserita non è valida";
		$date_s = str_replace("T"," ",$_POST["date"]);
		
		$date = date_create_from_format('Y-m-d H:i', $date_s);
		$current_date = new DateTime();
		$max_date = (new DateTime())->add(new DateInterval('PT120M'));
		$min_date = (new DateTime())->add(new DateInterval('PT5M'));
		if($date < $min_date) {
			$_POST["errorMessage"] = "Non puoi effettuare un oridine con meno di 5 minuti di margine, aumenta un pochino l'orario desiderato";
			return null;
		}
		if($date > $max_date) {
			$_POST["errorMessage"] = "Non puoi effettuare un oridine che arriverà tra 2 ore o più, diminuisci un pochino l'orario desiderato";
			return null;
		}
		
		return $date_s;
	}
	function get_required_card() {
		return $_POST["pay_type"] == 1 ? null : $_POST["card"];
	}
	function is_required_card_valid() {
		$_POST["errorMessage"] = "Metodo di pagamento non valido";
		if($_POST["pay_type"] == 1) {
			return true;
		} else {
			if(!is_card_of_owner($_POST["card"])) {
				$_POST["errorMessage"] = "Non è la tua carta...";
				return false;
			}
			if(is_card_expired($_POST["card"])) {
				$_POST["errorMessage"] = "Questa carta è scaduta";
				return false;
			}
		}
		return true;
	}
	function is_card_of_owner($card_id) {
		//TODO
		return true;
	}
	function is_card_expired($card_id) {
		//TODO
		return false;
	}
	
	function get_required_address() {
		if(!is_address_of_owner()){
			$_POST["errorMessage"] = "Indirizzo non valido";
			return null;
		}
		return $_POST["address"];
	}
	function is_address_of_owner() {
		//TODO
		return true;
	}
	
	function move_cart_content() {
		$conn = connect_db();
		$stmt = $conn->prepare("INSERT INTO product_list () VALUES ()");
		$stmt->execute();
		$result = $stmt->insert_id;
		$stmt->close();
		
		$stmt = $conn->prepare("UPDATE prod_instance SET product_list = ? WHERE product_list = ?");
		$stmt->bind_param("ii", $result, get_user()->cart_id);
		$stmt->execute();
		$stmt->close();
		
		
		$conn->close();
		return $result;
	}
	function get_deliver_price($address) {
		return $_POST["deliver_type"] == "1"?0:2;
	}
	function place_new_order($desired_date, $address, $card) {
		
		$deliver_price = get_deliver_price($address);
		$total_price = $deliver_price + get_cart_total_price();
		$product_list = move_cart_content();
		
		
		$conn = connect_db();
		$stmt = $conn->prepare("INSERT INTO `order` (creation_date, desired_date, pay_state, total_price, deliver_price, user, address, card, product_list) 
								VALUES (now(),?,0,?,?,?,?,?,?)");
		$stmt->bind_param("sddiiii", $desired_date, $total_price, $deliver_price, get_user()->id, $address, $card, $product_list);
		if($stmt->execute()) {
			$result = $stmt->insert_id;
			if(!insert_new_order_state($result, 0)) {
				$_POST["errorMessage"] = "Errore nella conferma dell'ordine";
				$result = -1;
			}
		} else {
			$_POST["errorMessage"] = "Errore nella conferma dell'ordine";
			$result = -1;
		}
		$result = $stmt->insert_id;
		$stmt->close();
		$conn->close();
		add_order_notification($result);
		return $result;
	}
	function add_order_notification($order_id) {
		ob_start();
		?>
		Il tuo <a href="order_details.php?id=<?php echo $order_id; ?>">ordine #<?php echo $order_id; ?></a> è stato ricevuto
		<?php
		$content = ob_get_clean();
		ob_start();
		?>
		Un nuovo <a href="order_details.php?id=<?php echo $order_id; ?>">ordine #<?php echo $order_id; ?></a> è stato ricevuto
		<?php
		$content_admin = ob_get_clean();
		
		require_once "../notification_utils.php";
		add_notification($content, get_user()->id);
		add_admin_notification($content_admin);
	}
	
	
?>