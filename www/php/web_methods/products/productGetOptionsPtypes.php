<?php	
    require_once "../../product_edit.php";
    require_once "../../login_utils.php";


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(is_user_logged_in() && is_user_admin())	{
            echo json_encode(get_available_ptypes()); 
        } else {
            echo "Devi essere loggato per accedere.";
        }	
    } else {
        echo "Questa non è una richiesta post.";
    }
?>