$(document).ready(function() {
    $(".btn-add_to_cart").click(addToCartButtonClick);
    $(".btn-add_usual_to_cart").click(addUsualToCartButtonClick);
    $("#buy-again").click(buyOrderAgainClick);
});

function addToCartButtonClick(event) {
    var form = $(this).parents("form:first");
    var product_id = form.find('.input-hidden-product-id').prop("value");
    var is_personalized = (form.find('.input-hidden-personalized').prop("value") === "true");
    var removed_ingredient_list = [];
    var bread_type_id = form.find('input[type=radio]').length > 0 ? 1 : null;
    if (is_personalized) {
        removed_ingredient_list = form.find('input[type=checkbox]:not(:checked)').map(function() { return this.name; }).get();
        bread_type_id = form.find('input[type=radio]:checked').prop("value");
    }

    console.log("Requesting add operation with values: ");
    console.log("product_id: " + product_id);
    console.log("is_personalized: " + is_personalized);

    addToCart(form, product_id, bread_type_id, removed_ingredient_list, 1, true);
    

}

function addToCart(form, product_id, bread_type_id, removed_ingredient_list, quantity, showNotificationOnSuccess) {
        /*var obj_data = {product_id: product_id, bread_type_id: bread_type_id, removed_ingredient_list: removed_ingredient_list};*/
        var obj_data = {product_id: product_id};
        if(quantity) {
            obj_data["quantity"] = quantity;
        }
        
        if (bread_type_id) {
            obj_data["bread_type_id"] = bread_type_id;
            console.log("bread_type_id: " + bread_type_id);
        }
    
        if (removed_ingredient_list && removed_ingredient_list.length > 0) {
            obj_data["removed_ingredient_list"] = removed_ingredient_list;
            console.log("removed_ingredient_list: " +  removed_ingredient_list);
        }
        
        $.ajax({
            type: "POST",
            //dataType: "json",
            //contentType: "application/json; charset=utf-8",
            url: "php/web_methods/add_to_cart.php",
            data: obj_data,
            success: function(msg){
                console.log("add_to_cart success");
                if(showNotificationOnSuccess) {
                    printNotificationAddToCart(form, msg);
                }
                update_lbl_cart_count_ajax();                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log("add_to_cart failed");
                console.log(textStatus, errorThrown);
            },
         });
}

function printNotificationAddToCart (form, msg) {
    add_to_cart_message = form.find('.add_to_cart_message');
    if(msg == "Prodotto aggiunto al carrello." || msg == "Prodotti aggiunti al carrello.") {
        add_to_cart_message.removeClass().addClass("add_to_cart_message text-success");
    } else {
        add_to_cart_message.removeClass().addClass("add_to_cart_message text-danger");                    
    }
    var result_text = form.find('.add_to_cart_text');
    result_text.html(msg);
    console.log("ciao");
    add_to_cart_message.slideDown("slow").delay(3000).slideUp("slow");
}

function getAllproductsAndAddToCart(form) {
    var products = form.find('.product_in_cart').each(function (index, element) {
        var product_id = $(element).attr("data-product-id");
        var bread_type_id = $(element).attr("data-bread-type-id");
        var removed_ingredient_list = JSON.parse($(element).attr("data-removed-ingredients"));
        var quantity = $(element).attr("data-quantity");

        console.log(product_id);
        console.log(bread_type_id);
        console.log(removed_ingredient_list);
        console.log(quantity);

        addToCart(form, product_id, bread_type_id, removed_ingredient_list, quantity, false);
    });
}

function addUsualToCartButtonClick(event) {
    var form = $(this).parents("form:first");
    getAllproductsAndAddToCart(form);
    printNotificationAddToCart(form, "Prodotti aggiunti al carrello.");
}

function buyOrderAgainClick(event) {
    var form = $(this).parents("body:first");
    getAllproductsAndAddToCart(form);
    printNotificationAddToCart(form, "Prodotti aggiunti al carrello.");
}
