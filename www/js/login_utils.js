function formhash(form, password) {
	if(password.value.length < 8) {
		$('#txbPasswordTmp').addClass("is-invalid");
		$("#lblNoPassword").slideDown("slow").delay(3000).slideUp("slow");   		
		return;
	}
	
   var p = document.createElement("input");
   form.appendChild(p);
   p.name = "txbPassword";
   p.type = "hidden"
   p.value = hash_password(password.value);
   // Assicurati che la password non venga inviata in chiaro.
   password.value = "";
   // Come ultimo passaggio, esegui il 'submit' del form.
   form.submit();
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}
function send_pin_to() {
	$("#lblEmailNonValida").finish();
	$("#lblEmailInviata").finish();
	if(!validateEmail($("#txbEmail").val())) {
		$("#lblEmailNonValida").slideDown("slow").delay(3000).slideUp("slow");   
		return;
	}
	$.ajax({
        type: "POST",
        url: "php/web_methods/send_pin.php",
        data: 'email=' + $("#txbEmail").val() + '',
        success: function (r) {
			console.log("send_pin_to success " + r);
			$("#lblEmailInviata").slideDown("slow").delay(3000).slideUp("slow");   
        },
        failure: function (r) {
            console.log("send_pin_to failed");
        }
    });
}

function change_password() {
	$('#txbPasswordTmp').removeClass("is-invalid");
	if($('#txbPasswordTmp').val().length < 8) {
		$('#txbPasswordTmp').addClass("is-invalid");
		$("#lblNoPassword").slideDown("slow").delay(3000).slideUp("slow");   		
		return;
	}
	new_password = hash_password($("#txbPasswordTmp").val());
	$.ajax({
        type: "POST",
        url: "php/web_methods/change_password.php",
        data: 'new_password=' + new_password + '&pin=' + $("#txbPin").val(),
        success: function (r) {
			console.log("change_password success");
			if(r === "ok") {
				$("#lblResultOk").slideDown("slow").delay(3000).slideUp("slow");   
			} else {
				$("#lblResultNo").slideDown("slow").delay(3000).slideUp("slow");   
			}
        },
        failure: function (r) {
            console.log("change_password failed");
        }
    });
}

function hash_password(password) {
   salt = "sadifhjgsadioufgsdaf098sda6ft32ogfysa97dfgsd80f";
   return sha512(password + salt);
}

