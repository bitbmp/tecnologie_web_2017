<?php	
    require_once "../../product_edit.php";
    require_once "../../login_utils.php";


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(is_user_logged_in() && is_user_admin())	{
            if(!empty($_POST["id"]) && !empty($_POST["name"])) {
                echo json_encode(update_ptype($_POST["id"], $_POST["name"])); 
            } else {
                echo json_encode(array('Result' => 'ERROR', 'Message' => 'Wrong parameters.')); 
            }
        } else {
            echo "Devi essere loggato per accedere.";
        }	
    } else {
        echo "Questa non è una richiesta post.";
    }
?>