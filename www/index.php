<!DOCTYPE html>
<html lang="it">

<head>
	<?php 
		require "php/page_parts.php"; 
		echo_head("Home");
	?>
	<script src="js/personalize_ingredients.js"></script>
	<script src="js/add_to_cart.js"></script>
	<script src="js/pull_products.js"></script>
	<script src="js/update_price_bread_type.js"></script>
</head>

<body>

	<?php echo_menu("index.php"); ?>
	<main>
		<div class="container">

			<?php echo_search_bar(); ?>

			<div class="product-list-static-area hide-during-search">
			
			<?php
			if(is_user_logged_in() && get_user()->usual != null) {
					echo_divider("Il Solito");
					echo_usual();
			} ?>
			
			<?php echo_divider("Preferito dai clienti"); ?>

			<?php echo_most_bought_products(); ?> 

			</div>
			
			<div class="show-during-search" style="display:none;">
				<?php echo_divider("Ricerca"); ?>
			</div>
			<div class="product-list-dynamic-area"></div>

			<div id="loading" class="loader-ellips" style="display:none;">
				<span class="loader-ellips__dot"></span>
				<span class="loader-ellips__dot"></span>
				<span class="loader-ellips__dot"></span>
				<span class="loader-ellips__dot"></span>
			</div>
		</div>
	</main>

	<?php echo_footer("Index"); ?>

</body>

</html>