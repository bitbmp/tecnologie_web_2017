<?php	
    require_once "../../product_edit.php";
    require_once "../../login_utils.php";


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if(is_user_logged_in() && is_user_admin())	{
            if(!empty($_REQUEST["productId"])) {
                echo json_encode(get_available_ings($_REQUEST["productId"])); 
            } else {
                echo json_encode(array('Result' => 'ERROR', 'Message' => 'Wrong parameters.')); 
            }
        } else {
            echo "Devi essere loggato per accedere.";
        }	
    } else {
        echo "Questa non è una richiesta post.";
    }
?>