<?php
require_once "php/mysql_conn.php";
require_once "php/login_utils.php";

function get_all_cart_prods_divs($product_list, $view_only) {
	
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT * FROM view_cart_product_list WHERE product_list = ?;");
    $stmt->bind_param("i", $product_list);  
    $stmt->execute();
    
    $result = $stmt->get_result();

	$stmt->close();
	$i = 0;
	ob_start();
	while ($myrow = $result->fetch_assoc()) {
		$conn1 = connect_db();
		$result1 = $conn1->query("CALL ingredients(".$myrow["instance_id"].");");

		$without = "";
		$without_ids = array();
		$count = 0;

		while ($myrow1 = $result1->fetch_array()) {
			if($myrow1["exist"] == 0) {
				$without .= $myrow1["name"].", ";
				array_push($without_ids, $myrow1["id"]);
				$count++;
			}
		}
		$result1->close();
		$conn1->close();
		
		if($count <= 0) {
			$without = "";
		} else {
			//removes last ,
			$without = substr($without, 0, -2);
		}
	
		internal_echo_cart_prods_div($myrow, $without, $without_ids, $i, $view_only);
		$i++;
		
	}
	$conn->close();	
	return ob_get_clean();
}

function internal_echo_cart_prods_div($myrow, $without, $without_ids, $i, $view_only) {
	?>
	<div id="product_in_cart_<?php echo $myrow["instance_id"]; ?>" class="product_in_cart" data-product-id="<?php echo $myrow["product_id"]; ?>" 
	data-bread-type-id="<?php echo $myrow["bread_type_id"]; ?>" data-removed-ingredients='<?php echo json_encode($without_ids); ?>' data-quantity="<?php echo $myrow["quantity"]; ?>">
		
		<?php if($i != 0 && $view_only) { ?>
		<div class="product-separator mb-2 mt-2"></div>	
		<?php } ?>
		
		<div class="row" style="padding-top:0px;">
			
			
			
			
			
			<?php if(!$view_only) { ?>
					<div class="col-7 d-flex align-items-centerpr-0">
						<div>
							<small class="font-italic"><?php echo $myrow["product_type"]; ?></small>
							<p class="mb-0"><strong> <?php echo $myrow["name"]; ?></strong></p>
						</div>
					</div>
					<div class="col-2 d-flex align-items-center pl-0 pr-0">
						<p class="mb-0"><?php echo $myrow["price"]; ?>€</p>
					</div>
					<div class="col-3 d-flex align-items-center pl-2">
						<label class="sr-only" for="cart_quantity_<?php echo $myrow["instance_id"]; ?>">Seleziona quantità</label>
						<input type="number" style="max-width:55px; min-width:55px;" name="quantity" min="1" max="100" class="form-control" id="cart_quantity_<?php echo $myrow["instance_id"]; ?>" value="<?php echo $myrow["quantity"]; ?>" onchange="change_quantity_cart(<?php echo $myrow["instance_id"]; ?>)">
						<input type="hidden" id="tmp_cart_quantity_<?php echo $myrow["instance_id"]; ?>" value="<?php echo $myrow["quantity"]; ?>">
					</div>
			
			<?php } else { ?>
					<div class="col-7 d-flex align-items-centerpr-0">
						<div>
							<small class="font-italic"><?php echo $myrow["product_type"]; ?></small>
							<p class="mb-0"><strong> <?php echo $myrow["name"]; ?></strong></p>
						</div>
					</div>
					<div class="col-2 d-flex align-items-center justify-content-end">
						<p class="mb-0"><?php echo $myrow["quantity"]; ?></p>
					</div>
					<div class="col-3 d-flex align-items-center justify-content-end">
						<p class="mb-0"><?php echo $myrow["price"]; ?>€</p>
					</div>
			<?php } ?>
			
		</div>
		
		<div class="row">
			<div class="col-7 pr-0">
				<?php if($myrow["bread_price"]!=0) { ?><div><small><?php if($myrow["bread_type"]!= null) { echo "Tipo: ".$myrow["bread_type"]; }?></small></div> <?php } ?>
				<div><small><?php echo strlen($without)>0?"Senza: ".$without:""; ?></small></div>
			</div>
			<div class="col-2 pl-0 pr-0">
				<?php if($myrow["bread_price"]!=0) { ?><small class="mb-0">+<?php echo $myrow["bread_price"]; ?> €</small> <?php } ?>	
			</div>
			<?php if(!$view_only) { ?>
				<div class="col-3 pl-2">
				<button type="button" class="btn btn-link pl-0" onclick="remove_from_cart(<?php echo $myrow["instance_id"]; ?>)"><small>Rimuovi</small></button>
				</div>
			<?php } ?>
		</div>
		<?php if(!$view_only) { ?>
		<div class="product-separator mb-2 mt-2"></div>	
		<?php } ?>
	</div>
	<?php
}

function get_cmb_user_addresses() {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT * FROM view_valid_addresses WHERE type = 0 AND user = ?;");
    $stmt->bind_param("i", get_user()->id);  
    $stmt->execute();
    $result = $stmt->get_result();
	$stmt->close();
	
	ob_start();
	?>
	<label class="sr-only" for="cmbAddress">Indirizzo</label>
	<select class="form-control" id="cmbAddress">
	<?php
	
	while ($row = $result->fetch_assoc()) { ?>
		<option <?php if(get_user()->default_address == $row["id"]) { echo "selected"; } ?> id="address_<?php echo $row["id"]; ?>"><?php echo $row["alias"]; ?></option>
	<?php }
	
	?></select><?php
	$conn->close();	
	return ob_get_clean();
}
function get_cmb_point_addresses() {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT * FROM view_valid_addresses WHERE type = 1");
    $stmt->execute();
    $result = $stmt->get_result();
	$stmt->close();
	
	ob_start();
	?>
	<label class="sr-only" for="cmbPoint">Punto di ritiro</label>
	<select class="form-control" id="cmbPoint">
	<?php
	
	while ($row = $result->fetch_assoc()) { ?>
		<option  id="address_<?php echo $row["id"]; ?>"><?php echo $row["alias"]; ?></option>
	<?php }
	
	?></select><?php
	$conn->close();	
	return ob_get_clean();
}

function get_cmb_cards() {
	$conn = connect_db();
    $stmt = $conn->prepare("SELECT * FROM view_valid_cards WHERE user = ?;");
    $stmt->bind_param("i", get_user()->cart_id);  
    $stmt->execute();
    $result = $stmt->get_result();
	$stmt->close();
	
	ob_start();
	?>
	<label class="sr-only" for="cmbCard">Carta di credito</label>
	<select class="form-control" id="cmbCard">
	<?php
	
	while ($row = $result->fetch_assoc()) { ?>
		<option <?php if(get_user()->default_card == $row["id"]) { echo "selected"; } ?> id="card_<?php echo $row["id"]; ?>"><?php echo substr_replace($row["code"],'**** **** **** **** ', 0, 12); ?></option>
	<?php }
	
	?></select><?php
	$conn->close();	
	return ob_get_clean();
}
?>









