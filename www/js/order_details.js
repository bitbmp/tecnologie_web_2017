$(document).ready(function() {
    $(".btn-cancel_order").click(CancelOrder);
    $("#usual-order").click(SetOrderAsUsual);
    attachAdminButtonEvents();
});

function CancelOrder(event) {
    console.log("cancel order triggered");

    var form = $(this).parents('form:first');
    var order_id = form.find('.input-hidden-order-id');

    var obj_data = {order_id: order_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/cancel_order.php",
        data: obj_data,
        success: function(data){
            var obj = JSON.parse(data);
            if(obj.msg == "Ordine annullato.") {
                $("#state-details").append(obj.html);
                $(".btn-cancel_order").remove();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("cancel order failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function SetOrderAsUsual(event) {
    console.log("usual order triggered");

    var order_id = $('.input-hidden-order-id');

    var obj_data = {order_id: order_id.val()};
    $.ajax({
        type: "POST",
        url: "php/web_methods/set_order_as_usual.php",
        data: obj_data,
        success: function(data){
            var obj = JSON.parse(data);
            console.log(obj.msg);
            var output = $("#message-output");
            var output_cont = $("#message-container");
            if(obj.msg == "Ordine impostato come 'Il solito'.") {
                output_cont.removeClass().addClass("text-success");
            } else {
                output_cont.removeClass().addClass("text-danger");                    
            }
            output.html(obj.msg);
            output_cont.slideDown("slow").delay(3000).slideUp("slow");
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("usual order failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function ChangeOrderStatus(event) {
    console.log("change order triggered");

    var order_id = $('.input-hidden-order-id');
    var id = event.currentTarget.id;
    var order_status;

    switch(id) {
        case "btn-accept":
            order_status = 1;
            break;
        case "btn-deliver":
            order_status = 2;
            break;
        case "btn-finish":
            order_status = 3;
            break;
        case "btn-reject":
            order_status = 4;
            break
    }

    var obj_data = {order_id: order_id.val(), order_status : order_status};
    $.ajax({
        type: "POST",
        url: "php/web_methods/change_order_status.php",
        data: obj_data,
        success: function(data){
            var obj = JSON.parse(data);
            if(obj.msg == "Stato cambiato.") {
                $("#state-details").append(obj.html);
                $(".btn-cancel_order").remove();
                if(id == "btn-accept") {
                    $("#button-col").empty().append($('<button type="button" id="btn-deliver" class="btn btn-success">Pronto&nbsp;&nbsp;<div class="far fa-check-circle"></div></button>'));
                } else if(id == "btn-deliver") {
                    $("#button-col").empty().append($('<button type="button" id="btn-finish" class="btn btn-success">Consegnato&nbsp;&nbsp;<div class="far fa-check-circle"></div></button>'));
                } else if(id == "btn-reject" || id == "btn-finish") {
                    $("#button-row").prev().remove();
                    $("#button-row").remove();
                }
            }
            attachAdminButtonEvents();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log("change order failed");
            console.log(textStatus, errorThrown);
        },
    });
}

function attachAdminButtonEvents() {
    $("#btn-reject").off().click(ChangeOrderStatus);
    $("#btn-accept").off().click(ChangeOrderStatus);
    $("#btn-deliver").off().click(ChangeOrderStatus);
    $("#btn-finish").off().click(ChangeOrderStatus);
}