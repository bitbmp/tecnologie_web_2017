<!DOCTYPE html>
<html lang="it">

<head>
    <?php
        require_once "php/page_parts.php";
        require_once "php/login_utils.php"; 
		
		if(!is_user_logged_in()) {
			header("Location: index.php");
        }
         
		echo echo_head("Notifiche");
	?>
    <script src="js/input.js"></script>
    <script src="js/cards.js"></script>
</head>
<body>
    <?php echo_menu("notifies.php"); ?>	
	<main>
		<div class="container">
			<?php 
			echo_divider("Tutte le notifiche"); 
			require_once "php/mysql_conn.php";
			
			$conn = connect_db();
			$result = $conn->query("SELECT * FROM notifications WHERE user = ".get_user()->id." ORDER BY Date DESC");
			$i = 0;
			while ($row = $result->fetch_assoc()) {
				$i++;
				?>
				<div class="notification_container">
				<p class="mb-0 mt-0 font-italic"><small><?php echo $row["date"]; ?></small></p>
				<p class="mb-0 mt-0 font-weight-bold"><?php echo $row["content"]; ?></p>
				</div>
				<?php if($i < $result->num_rows) { ?>
				<div class="product-separator mb-1 mt-1"></div>
				
				<?php }
				
			} 
			$conn->close();
			?>
			
		</div>
	</main>
    <?php echo_footer("notifies.php"); ?>
</body>
</html>
