<!DOCTYPE html>
<html lang="it">

<head>
    <?php 
		require_once "php/page_parts.php"; 
		require_once "php/login_utils.php"; 
		
		if(is_user_logged_in()) {
			header("Location: index.php");
		}
		
		echo_head("Recupero password");
	?>
</head>
<body> <!--style="min-height: 35rem; padding-top: 4.5rem;"-->

<?php echo_menu("login.php"); ?>	
<main>
	<div class="container">

		<?php echo_logo_image(); ?>
		
		<?php
			$email = "";
			$password = "";
			$email_valid = TRUE;
			$password_valid = TRUE;
			$attempts_valid = TRUE;
			
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				
				if(isset($_POST["fromRegister"])) {
					if(isset($_POST["txbEmail"])) {
						$email = $_POST["txbEmail"];
					}
				}
				else 
				{
					if(isset($_POST["txbEmail"])) {
						$email = $_POST["txbEmail"];
					} else {
						$email_valid = FALSE;
					}
					
					if(isset($_POST["txbPassword"])) {
						$password = $_POST["txbPassword"];
					} else {
						$password_valid = FALSE;
					}	
					
					if($email_valid && $password_valid) {
						if(is_attempts_ok()) {
							$user = check_login($email, $password);
							if($user != NULL) {
								user_login($user);
								header("Location: index.php");
							} else {
								$email_valid = FALSE;
								$password_valid = FALSE;
								if(!is_attempts_ok()) {
									$attempts_valid= FALSE;
								}
							}
						}
						else {
							$attempts_valid= FALSE;
						}
					}
				}
			}
		?>
		
		<form class="form-group" method="post">
			<?php echo_divider("Recupero password"); ?>
			
			
			<label class="sr-only" for="txbEmail">Email</label>
			
			<div id="lblEmailInviata" class="text-right" style="display: none;">
				<small  class="text-success">PIN inviato</small>
			</div>
			<div id="lblEmailNonValida" class="text-right" style="display: none;">
				<small  class="text-danger">Email non valida</small>
			</div>
			<div class="input-group mt-0 mb-2">
				<div class="input-group-addon"><div class="fa fa-envelope fa-fw"></div></div>
				<input type="email" class="form-control" id="txbEmail" name="txbEmail" placeholder="Email" required>
			</div>
			

			<div class="form-group mb-4">
				<input type="button" onclick="send_pin_to();" class="btn btn-primary btn-lg btn-block" value="Invia PIN">
			</div>
			
			
			<div id="lblResultOk" class="text-right" style="display: none;">
				<small  class="text-success">Password cambiata!</small>
			</div>
			<div id="lblResultNo" class="text-right" style="display: none;">
				<small  class="text-danger">Pin non valido</small>
			</div>
			<div id="lblNoPassword" class="text-right" style="display: none;">
				<small  class="text-danger">La password deve avere almeno 8 caratteri</small>
			</div>
			
			<div class="input-group mb-2 mt-0">
				<div class="input-group-addon"><div class="fa fa-key fa-fw"></div></div>
				<input type="number" class="form-control" id="txbPin" name="txbPin" placeholder="PIN" required>
			</div>
			<label class="sr-only" for="txbPasswordTmp">Passowrd</label>
			<div class="input-group mb-2">
				<div class="input-group-addon"><div class="fa fa-lock fa-fw"></div></div>
				<input type="password" class="form-control <?php echo !$password_valid?"is-invalid":""; ?>" name="txbPasswordTmp" id="txbPasswordTmp" placeholder="Nuova password" required>
			</div>
			
			<div class="form-group">
				<input type="button" onclick="change_password();" class="btn btn-primary btn-lg btn-block" value="Convalida PIN">
			</div>

		</form>
	</div>
</main>


</body>
</html>
