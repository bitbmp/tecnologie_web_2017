<?php
require_once "../orders_utils.php";

$response = new stdClass();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {	
    if(is_user_logged_in())	{
        if(isset($_POST["order_id"])) {
            $user_id = $_SESSION["user"]->id;            
            $order_id = $_POST["order_id"];
            if($result = cancel_user_order($user_id, $order_id)) {
                $response->msg = "Ordine annullato.";
                $record = $result->fetch_assoc();
                ob_start();
                echo_internal_order_state($order_id, $record);
                $html = ob_get_clean();
                $response->html = $html;
                echo json_encode($response);
				
				add_order_notification($_POST["order_id"]);
            } else {
                $response->msg = "Impossibile annullare l'ordine.";
                echo json_encode($response);
            }        
        } else {
            $response->msg = "Parametri mancanti.";
            echo json_encode($response);
        }
    } else {
        $response->msg = "Devi accedere per effettuare queste operazioni.";
        echo json_encode($response);
    }
} else {
    $response->msg = "Questa non è una richiesta post.";
    echo json_encode($response);
}

function add_order_notification($order_id) {
	ob_start();
	?>
	Il tuo <a href="order_details.php?id=<?php echo $order_id; ?>">ordine #<?php echo $order_id; ?></a> è stato annullato come richiesto
	<?php
	$content = ob_get_clean();
	ob_start();
	?>
	<a href="order_details.php?id=<?php echo $order_id; ?>">L'ordine #<?php echo $order_id; ?></a> è stato annullato dal cliente
	<?php
	$content_admin = ob_get_clean();
	
	require_once "../notification_utils.php";
	add_notification($content, get_user()->id);
	add_admin_notification($content_admin);
}
?>