<?php
require_once "mysql_conn.php";

function get_products_sections($view_name = "view_product_list", $page = null) {
	$conn = connect_db();
    $page_size = 5;
	/*Possible security problem cause $view_name is concatenated in query, defining it as a param seems not working*/
	if (isset($page) && intval($page) > 0 ) {
		$stmt = $conn->prepare("SELECT * FROM ".$view_name." ORDER BY name ASC LIMIT ?,?");
		$stmt->bind_param("ii", $offset, $page_size);
		$offset = $page_size * ($page-1);

	} else {
		$stmt = $conn->prepare("SELECT * FROM ".$view_name." ORDER BY name ASC");
	}

    $stmt->execute();
	
	$result = $stmt->get_result();

	$stmt->close();
	
	$html = get_products_sections_from_result($conn, $result, ($page != 1 && $page != null));

	$conn->close();

	return $html;
}

function get_products_sections_search_by_keyword($keyword="%", $page = 1) {
	$conn = connect_db();
	$stmt = $conn->prepare("CALL search_products(?, ?)");
	$stmt->bind_param("si", $keyword, $page);
	$stmt->execute();
	
	$result = $stmt->get_result();

	$stmt->close();
	
	$html = get_products_sections_from_result($conn, $result, $page != 1);

	$conn->close();

	return $html;

}

function get_products_sections_from_sql_procedure($procedure_name, $insert_first_separator = true, $params_types, ...$params) {
	$conn = connect_db();

	$num_add_params = func_num_args()-3;
	$bind_params = func_get_args();
	array_shift($bind_params);
	array_shift($bind_params);
	//$bind_params = array_values($bind_params);

	
	$query = "CALL ".$procedure_name."(";
	
	for ($i = 0; $i < $num_add_params; $i++) {
		if ($i !== 0) {
			$query=$query.", ";
		}
		$query=$query."?";
	}
	$query=$query.")";
	//echo $query;
	//print_r($bind_params);
	$stmt = $conn->prepare($query);
	
	$stmt->bind_param(...$bind_params);
	
	$stmt->execute();
	
	$result = $stmt->get_result();

	$stmt->close();
	
	$html = get_products_sections_from_result($conn, $result, $insert_first_separator);

	$conn->close();

	return $html;
}

function get_regex_from_search_string($search_string) {
	$tmp = array();
	$result = "";
	preg_match_all('/[a-zA-Z]+/', $search_string, $tmp);
	$search_keys = $tmp[0];
	for ($i = 0; $i < count($search_keys); $i++) {
		$result=$result."(?=.*".($search_keys[$i]).")";
	}
	$result = $result.'.+';
	return $result;
}

function get_products_sections_from_result($conn, $result, $insert_first_separator = true) {
	if ($result->num_rows > 0) {
		ob_start();
		$i = 0;
		while ($myrow = $result->fetch_assoc()) {
			if($i != 0 || $insert_first_separator) {
				?><div class="product-separator"></div><?php
			}
			internal_echo_product_section($conn, $myrow["id"], $myrow["name"], $myrow["price"], $myrow["product_type"], $myrow["product_type_id"], $myrow["prep_time"]);
			$i++;
		}
		return ob_get_clean();
	} else {
		return "";
	}
}

function get_bread_types($conn = null, $product_type = null, $group_name) {
	if (isset($product_type) && isset($conn)) {
		$stmt = $conn->prepare("SELECT * FROM view_product_type_bread_types WHERE product_type = ?");
		$stmt->bind_param("i", $product_type); 
	
		$stmt->execute();
		
		$result = $stmt->get_result();
	
		$stmt->close();
		if ($result->num_rows > 0) {
			ob_start();
			?>
			<div class="custom-controls-stacked">
			<?php
			
			while ($myrow = $result->fetch_assoc()) {
				internal_echo_bread_radio($group_name, $myrow["bread_type"], ucfirst($myrow["name"]), $myrow["price"]);
			}
			?>
			</div>
			<?php
			return ob_get_clean();
		} 
	} 
	return "";
}

function get_product_ings_array($conn = null, $food_id = null) {
	if (isset($food_id) && isset($conn)) {
		$stmt = $conn->prepare("SELECT * FROM view_product_ingredients WHERE pid = ?");
		$stmt->bind_param("i", $food_id); 
	
		$stmt->execute();
		
		$result = $stmt->get_result();
	
		$stmt->close();	

		if ($result->num_rows > 0) {
			ob_start();
			$ing_names = array();
			?>
			<div class="custom-controls-stacked">
			<?php
			while ($myrow = $result->fetch_assoc()) {
				internal_echo_ing_checkbox($myrow["id"], ucfirst($myrow["name"]));
				array_push($ing_names, $myrow["name"]);
			}
			?>
			</div>
			<?php
			$checkboxes = ob_get_clean();
			ob_start();
			internal_echo_ing_list($ing_names);
			$list = ob_get_clean();
			return array($list, $checkboxes);
		}
	}
	return array("", "");
}

function internal_echo_ing_checkbox($id, $name) {
	?>
		<label class="label-personalize custom-control custom-checkbox">
			<?php echo $name; ?>
			<input type="checkbox" class="custom-control-input" name="<?php echo $id; ?>" checked>
			<span class="custom-control-indicator"></span>
		</label>
	<?php
}

function internal_echo_bread_radio($radio_group, $ing_id, $ing_name, $ing_price) {
	?>
		<label class="label-personalize custom-control custom-radio">
			<?php echo $ing_name; if (floatval($ing_price) > 0) {?> [+<?php echo $ing_price; ?> €]<?php }?>
			<input type="radio" class="custom-control-input radio-bread-type" name="<?php echo $radio_group; ?>" value="<?php echo $ing_id; ?>"
			data-price="<?php echo $ing_price; ?>" <?php /*default bread type*/ echo $ing_id == 1 ? "checked" : ""; ?>>
			<span class="custom-control-indicator"></span>
		</label>
	<?php
}

function internal_echo_ing_list($ing_names) {
	?>
		<small class="ingredient-list">
			(<?php
				for ($i = 0; $i < count($ing_names); $i++) {
					echo ($i != 0 ? ", " : "").$ing_names[$i];
				}
			?>)
		</small>
	<?php
}

function internal_echo_product_section($conn, $product_id, $product_name, $product_price, $product_type, $product_type_id, $product_prep_time) {
	?>
	<section class="product-section">
			<form>
				<input type="hidden" class="input-hidden-product-id" name="product-id" value="<?php echo $product_id; ?>">
				<input type="hidden" class="input-hidden-personalized" name="personalized" value="false">
				<div class="row">
					<div class="col-12">
						<small class="product-type-text"><?php echo ucfirst($product_type); ?></small>
					</div>
				</div>
				<div class="row">
					<div class="col-6">
						<h4><?php echo ucfirst($product_name); ?></h4><?php if($product_prep_time != 0) {?> - <?php echo intval(((int)$product_prep_time)/60);?> min<?php }?>
						<p class="add_to_cart_message" style="display:none;">
							<small class="mt-0 text-left add_to_cart_text"></small>
						</p>
					</div>

					<div class="col-3 text-right">
						<span class="product-price"><span class="product-price-value" data-baseprice="<?php echo $product_price; ?>"><?php echo $product_price; ?></span> €</span>
					</div>

					<div class="col-3 text-right">
						<button type="button" class="btn btn-primary btn-add_to_cart">
							<span class="fa fa-cart-plus fa-lg"></span>
						</button>
					</div>
				</div>
				<?php
					$ar_ings = get_product_ings_array($conn, $product_id);
					$ing_small_list = $ar_ings[0];
					$ing_checkboxes = $ar_ings[1];
					$bread_radios = get_bread_types($conn, $product_type_id, $product_id);

					if (!empty($ing_small_list) || !empty($bread_radios))
					{
				?>
				<div class="row">
					<div class="col-6">
						<?php 
						/*Ingredient list under product name*/
						echo $ing_small_list;
						?>
						<div class="personalize-area" style="display: none;">
							<?php
								if (!empty($ing_checkboxes)) {
							?>
							<fieldset>
								<legend>Ingredienti:</legend>
								<?php
								/*Checkboxes of ingredients*/
								echo $ing_checkboxes;
								?>
							</fieldset>
							<?php
								}

								if (!empty($bread_radios)) {
							?>
							<fieldset>
								<legend>Tipo impasto:</legend>
									<?php
									echo $bread_radios;
									?>
							</fieldset>
							<?php
								}
							?>
						</div>
					</div>
					<div class="col-6 text-right">
						<button type="button" class="btn btn-secondary btn-personalize_ing mt-2">Personalizza</button>
					</div>
				</div>
				<?php
					}
				?>
			</form>
		</section>	
	<?php
}

function get_search_bar($text = "") { ?>
	<form class="form-group" method="post" style="padding-top:10px" onkeypress="return event.keyCode != 13;">

	<label class="sr-only" for="txbCerca">Cerca</label>
	<div class="input-group mb-2 mt-2">
		<div class="input-group-addon">
			<div class="fa fa-search fa-fw"></div>
		</div>
		<input type="text" class="form-control" id="txbCerca" placeholder="<?php echo $text; ?>">
	</div>
	</form>
<?php
}
?>