<?php
	require_once "../mysql_conn.php";
	require_once "../login_utils.php";
	
	
	if(!is_user_logged_in()) {
		echo "Non hai effettuato l'accesso";
		exit(1);
	}
	
	$conn = connect_db();
	$result = $conn->query("SELECT * FROM notifications WHERE user = ".get_user()->id." AND disposed = 0 ORDER BY Date DESC LIMIT 5");
	$i = 0;
	while ($row = $result->fetch_assoc()) {
		$i++;
		?>
		<div class="notification_container">
		<p class="mb-0 mt-0 font-italic"><small><?php echo $row["date"]; ?></small></p>
		<p class="mb-0 mt-0 font-weight-bold"><?php echo $row["content"]; ?></p>
		
		<?php if(/*$i < $result->num_rows*/true) { ?>
		</div>
		<div class="product-separator mb-1 mt-1"></div>
		
		<?php }
		
	} 
	$conn->close();
	
	?> 

	<div class="notification_container">
	<a href="notifies.php">Guarda tutte le notifiche</a>
	</div>