<?php 
require_once "php/login_utils.php";
require_once "php/cart_utils.php"; 
?>

<nav class="navbar navbar-expand navbar-dark bg-dark">
  
<!--<a class="navbar-brand" href="index.php">Fast 'N Full</a>-->
<div class="container">
<ul class="navbar-nav row">
		
	<li class="nav-item <?php echo $menu_focus=="index.php"?"active":""; ?>">
		<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
	</li>
	<li class="nav-item <?php echo $menu_focus=="food.php"?"active":""; ?>">
		<a class="nav-link" href="food.php">Cibo</a>
	</li>
	<li class="nav-item <?php echo $menu_focus=="drinks.php"?"active":""; ?>">
		<a class="nav-link" href="drinks.php">Bevande</a>
	</li>

	<li class="nav-item dropdown <?php echo $menu_focus=="login.php"||$menu_focus=="register.php"||$menu_focus=="cards.php"||$menu_focus=="addresses.php"||$menu_focus=="orders.php"?"active":""; ?>">
		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<?php echo is_user_logged_in()?$_SESSION["user"]->first_name." ".$_SESSION["user"]->last_name:"Account"; ?>
		</a>
		<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
			<?php 
			if(is_user_logged_in()) { ?>
				<a class="dropdown-item" href="orders.php">I miei ordini</a>
				<a class="dropdown-item" href="addresses.php">I miei indirizzi</a>
				<a class="dropdown-item" href="cards.php">Le mie carte</a>
				<?php
				if(is_user_admin()) {
				?>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="edit.php">Gestione prodotti</a>
				<a class="dropdown-item" href="admin_orders.php">Gestione ordini</a>
				<?php
				}
				?>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="php/logout.php">Logout <span class="fas fa-sign-out-alt"></span></a>
				<?php
				} else {
				?>
				<a class="dropdown-item" href="login.php">Login <span class="fas fa-sign-in-alt"></span></a>
				<a class="dropdown-item" href="register.php">Register</a>
			<?php } ?>
		</div>

	</li>
		<?php 
			if(is_user_logged_in()) {?>
				<li class="nav-item">
				<button class="ml-2 mr-2 btn btn-light" onclick="window.location.href='cart.php'">Carrello <span class="fa fa-shopping-cart fa-fw"></span><span id="lbl_cart_count" class="badge ml-0 pl-0"></span></button>
				</li>
				<li class="nav-item <?php echo $menu_focus=="notifies.php"?"active":""; ?>">
					<a class="nav-link" id="popover_notification" href="#" data-placement="bottom" data-toggle="popover" title="Notifiche" data-content="">
						<span class="fa fa-bell fa-fw"></span><span class="badge ml-0 pl-0 text-danger" id="lbl_notify_count"></span>
					</a>
				</li>
		<?php 
			}
		?>
		<?php
		if(is_user_admin()) {
		?>
		<li class="nav-item <?php echo $menu_focus=="admin_orders.php"?"active":""; ?>">
			<a class="nav-link" href="admin_orders.php">Ordini</a>
		</li>
		<?php } ?>
    </ul>
 </div>
</nav>