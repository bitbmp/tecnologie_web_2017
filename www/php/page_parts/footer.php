<footer id="footer-area">
    <div class="container">
        <div class="row text-left">
            <div class="col-6">
                <div class="footer-content" >
                    <h1>Fast 'N Full</h1>
                    <p>
                        <a href="index.php">Home</a>
                        ♦
                        <a href="#">Informativa privacy</a>
                    </p>
                </div>
            </div>

            <div class="col-6 text-right">
                <div class="footer-content">
                    <a href="#" id="fb"><span class="fab fa-facebook-f" aria-hidden="true"></span></a>
                    <a href="#" id="gplus"><span class="fab fa-google-plus-g" aria-hidden="true"></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="product-separator"></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="bottom-info">
                    Copyright © 2017 BMP
                                    | P.Iva 123123123123
                                    | Via Prova 123, 47522 Cesena (FC)
                </p>
            </div>
        </div>
    </div>
</footer>